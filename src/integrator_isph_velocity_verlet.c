#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "sph.h"
#include "integrator.h"
#include "forces.h"
#include "isph.h"
#include "run.h"
#include "boundary.h"
#include "kernel.h"

static void predictor(particleData *pdata, double dt, int tag);
static void corrector(particleData *pdata, double dt);
static void save_prev(particleData *pdata);
static void update_ghost_displacement(double dt);
static void sum_divergence(particleData *pdata);
static void reset_acceleration(particleData * pdata,int tag);

/* Velocity-Verlet time integration.
 * Algorithm is based on Tartakovsky and Meakin.
 * \f{eqnarray*}{
 * r_i ( t + \Delta t) &=& r_i(t) + \Delta t v_i (t) + 0.5 \Delta t^2 a_i (t) \\
 * v_i(t+\Delta t) &=&  v_i(t) + 0.5 \Delta t ( a_i (t) + a_i (t+ \Delta t)) \f}
 */
void integrate_isph_velocity_verlet(particleData *pdata, double dt)
{
  save_prev(pdata); 

  if(rigid_body_sim)
    cg_rigid(pdata);

  interact_type itype[] = { count_neighbors, wsum, visc, pair_potential };
  interact(pdata,itype, sizeof itype / sizeof * itype ,dt); /*viscous force and potential forces*/ 

  /*
     if(surf_tens){
     interact(pdata,surfNorm,dt);
     int i,c;
     for(i=0;i<N;i++){
     pdata[i].modC = magnitude(&pdata[i].n);
     if(pdata[i].modC > DELTA){
     normalize(&pdata[i].n);
     }else{
     pdata[i].modC = 0.0;
     for(c=0;c<dim;c++)
     (&pdata[i].n.x)[c] = 0.0;
     }
     }
     printf("Normals computed \n");
     interact(pdata,surfKappa, dt);
     pad_surf_kappa(pdata);
     }*/
  /* for MLS surface tension
     interact(pdata,interfaceNeighbors,dt);
     if(surf_tens){
     interface_detect(pdata);
     if(dim == 2){
     interface_arrange(pdata);
     calculate_lagrange_kappa(pdata);
     }else if(dim ==3 ){
  //calculate_padded_kappa(pdata);
  calculate_MLS_kappa(pdata);
  surface_tension(pdata);
  }
  }
  */
  /* for the surf Kappa thing to add error term to pressure gradient 
     interact(pdata,surfKappa,dt);
     */

  body_forces(pdata); /*body force*/

  predictor(pdata, dt, 2); /* update velocity to u* */
  set_interior(pdata); 
  predictor(pdata, dt, 4); 
//  reset_acceleration(pdata,1);

  
  if(GHOSTBC){
    update_ghost_velocity(pdata,1);
    update_ghost_displacement(dt);
  }
  interact_type itype_b[] = { divV };
  if(defgrad){
    //   interact(pdata,defGrad,dt);
    //   compute_determinant_F(pdata, dt);
  }else{
    interact(pdata,itype_b,1,dt); /* divergence of u* */
  }
  /*if(divergence==4){
    update_divergence(pdata);
    }*/
int i;
  solve_pressure(pdata,dt); 
  interact_type itype_c[] = {pressForce};
  interact(pdata, itype_c, 1,dt);/*pressure force*/
//  correct_pressure_force(pdata);
  //reset_acceleration(pdata,2);
  predictor(pdata, dt, 3); 
  if(rigid_body_sim)
    rigid_body(pdata,dt);
 // printf("\n kappa value %lf \n", kappa);

  //for evaporation case 

/*
  if (fabs(sim_time) >=evaporation_time && fabs(sim_time)>1.1 ){
       
  interface_detect(pdata);
  printf("\n Detected interface for evaporation \n");

       evaporation_time += 0.1;
     }   
  */
  
  
   corrector(pdata,dt); 
  //int i;
  for(i=0; i<dim; i++)
    if(periodic[i] == 1) periodic_bc(pdata,i);
}

static void save_prev(particleData *pdata)
{
  int i,c,d;
  nnz = 0;
  for(c = 0; c < 3; c++){
    int body_id;
    for(body_id=0;body_id<10;body_id++){
      (&V_r_n[body_id].x)[c] = (&V_r[body_id].x)[c];
      (&omega_n[body_id].x)[c] = (&omega[body_id].x)[c];
      (&c_g[body_id].x)[c] = 0.0;
    }
  }
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i,c,d) 
#endif
  for(i=0;i<N;i++){
    for(c = 0; c < dim; c++){
      (&pdata[i].posn.x)[c] = (&pdata[i].pos.x)[c];
      (&pdata[i].veln.x)[c] = (&pdata[i].vel.x)[c];  
      (&pdata[i].accn.x)[c] =(&pdata[i].acc_p.x)[c] + (&pdata[i].acc_mu.x)[c] ;  
      (&pdata[i].acc.x)[c] = 0.0;
      (&pdata[i].acc_p.x)[c] = 0.0;
      (&pdata[i].acc_mu.x)[c] = 0.0;
//      (&pdata[i].n.x)[c] = 0.0;  
//      (&pdata[i].n_0.x)[c] = 0.0;  
      (&pdata[i].nearest.x)[c] = 0.0;  
      (&pdata[i].gradW.x)[c] = 0.0;  
      (&pdata[i].uxx.x)[c] = 0.0;  
      (&pdata[i].vel_xsph.x)[c] = 0.0;  
    }   
    for(c=0;c<dim;c++)
      for(d=0;d<dim;d++)
        (&(&pdata[i].def_grad_n.xx)[c].x)[d] = 0.0;

    pdata[i].num_neighbors = 0;
    pdata[i].interface_tag = 0;
    pdata[i].wsum = 0.0;
    pdata[i].s_kappa = 0.0;
    pdata[i].u = 0.0;
    pdata[i].div_pos = 0.0;
    pdata[i].kab = 0.0;
    pdata[i].pressuren = pdata[i].pressure;
  }
}


static void predictor(particleData *pdata, double dt, int tag)
{
  int i,c;
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i,c) 
#endif
  for(i=0;i<N;i++){
    if(tag==2){
      for(c=0;c<dim;c++)
        (&pdata[i].acc_mu.x)[c] = (&pdata[i].acc.x)[c];
    }
    if(tag==3){
      for(c=0;c<dim;c++)
        (&pdata[i].acc_p.x)[c] = (&pdata[i].acc.x)[c];
    }

    if(pdata[i].phase >=0){
    for(c = 0; c < dim; c++){
      if(tag==1 ){
        (&pdata[i].pos.x)[c]   = (&pdata[i].posn.x)[c]  + dt*((&pdata[i].veln.x)[c]) ; /*updating position based on old vel*/
      }
    }
    }
    if(pdata[i].phase >=0){
      for(c = 0; c < dim; c++){
        if(tag==2){
          (&pdata[i].vel.x)[c]   = (&pdata[i].veln.x)[c] + dt*((&pdata[i].acc.x)[c]) ;/*velocity based on visc and body forces*/
          (&pdata[i].acc.x)[c]   = 0.0;
        }else if(tag==3){
//          (&pdata[i].vel.x)[c]   = (&pdata[i].vel.x)[c] + dt*((&pdata[i].acc.x)[c]) ;
          (&pdata[i].vel.x)[c]   = (&pdata[i].veln.x)[c] + 0.5*dt*((&pdata[i].accn.x)[c]+ (&pdata[i].acc_mu.x)[c] + (&pdata[i].acc_p.x)[c]) ;
        }else if(tag==4){
          (&pdata[i].pos_s.x)[c] =  dt*((&pdata[i].vel.x)[c]) ;
        }
      }
    }
  } 
}
 
static void corrector(particleData *pdata,double dt)
{
  int i,c ;
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i,c) 
#endif
  for(i=0;i<N;i++){
    if (pdata[i].phase>=0 ) { 
      for(c = 0; c < dim; c++)
        //p-c sph  //      (&pdata[i].pos.x)[c] = (&pdata[i].posn.x)[c] + dt*0.5*((&pdata[i].vel.x)[c] + (&pdata[i].veln.x)[c]) ;
        //        (&pdata[i].pos.x)[c] = (&pdata[i].posn.x)[c] + dt*((&pdata[i].vel.x)[c] ) ;
        (&pdata[i].pos.x)[c] = (&pdata[i].posn.x)[c] + dt*((&pdata[i].vel.x)[c] ) +0.5*dt*dt*((&pdata[i].acc_mu.x)[c] + (&pdata[i].acc_p.x)[c]) ;

    } 
    if (pdata[i].phase < 0 ) { 
      for(c = 0; c < dim; c++)
        //p-c sph  //      (&pdata[i].pos.x)[c] = (&pdata[i].posn.x)[c] + dt*0.5*((&pdata[i].vel.x)[c] + (&pdata[i].veln.x)[c]) ;
        //        (&pdata[i].pos.x)[c] = (&pdata[i].posn.x)[c] + dt*((&pdata[i].vel.x)[c] ) ;
        (&pdata[i].pos.x)[c] = (&pdata[i].posn.x)[c] + dt*((&pdata[i].vel.x)[c] ) ;

    }
    /* for evaporation model 
    if(pdata[i].phase >=0 && pdata[i].interface_tag != 0 ){
      for(c = 0; c < dim; c++)
        (&pdata[i].pos.x)[c] = (&pdata[i].pos.x)[c] + ((&xhigh->x)[c] - (&xlow->x)[c]);

    }
    */
  }
}

/*For identify particles near a free surface *
* inorder to ramp pressure to ensure positive*
* values
static void interface_id(particleData *pdata)
{
  int i ;
#ifdef _OPENMP
#pragma omp parallel for default(shared) private(i,c) 
#endif
  for(i=0;i<N;i++){
    if (fabs(pdata[i].div_pos) <= 1.40 ) { 
      pdata[i].interface_tag = 1;
    }else {
      pdata[i].interface_tag = 0;
    } 
  }
}*/
/*
static void calculate_area_ab(particleData *pdata)                             
{                                                                              

  int i;                                                                       
  double x_max=0.0, y_max=0.0;                                                 
  double x_min=0.0, y_min=0.0;                                                 
  for(i=0;i<N;i++){                                                            

    if(pdata[i].pos.x>x_max)                                                   
      x_max = pdata[i].pos.x;                                                  
    if(pdata[i].pos.y>y_max)                                                   
      y_max = pdata[i].pos.y;                                                  
    if(pdata[i].pos.x<x_min)                                                   
      x_min = pdata[i].pos.x;                                                  
    if(pdata[i].pos.y<y_min)                                                   
      y_min = pdata[i].pos.y;                                                  

  }                                                                            
  major_axis = (x_max-x_min)/2.0;                                   

}*/
/*
static void update_divergence(particleData *pdata)
{
int i;
	for(i=0;i<N;i++){
		pdata[i].u = pdata[i].uxx.x/pdata[i].gradW.x + pdata[i].uxx.y/pdata[i].gradW.y;

	}
	return ;
}
*/
static void sum_divergence(particleData *pdata)
{
  int i;
  double sum = 0.0;
  for(i=0;i<N;i++){
    sum += pdata[i].u;
  }
   printf("\n Sum of divergence = %lf \n" , sum); 
}

static void update_ghost_displacement(double dt){
  int i,c;
  for(i=0;i<g_N;i++){
    for(c=0;c<dim;c++)
      (&g_pdata[i].pos_s.x)[c] =  dt*((&g_pdata[i].vel.x)[c]) ;
  }

}
static void reset_acceleration(particleData * pdata, int tag)
{
  int i, c;
  for(i=0;i<N;i++){    
    for(c=0;c<dim;c++)
      (&pdata[i].acc.x)[c] = 0.0;
  }
  return;
}
