#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

#include "particles.h"
#include "cell.h"
#include "kernel.h"

void allocate_cells()
{
/* Allocates the cells */
  cells.cellofpart = malloc(N* sizeof(int));
  cells.partincell = malloc(cells.dim.x * cells.dim.y * 
      cells.dim.z * sizeof(vector));
  cells.pos = malloc(cells.dim.x * cells.dim.y * 
      cells.dim.z * sizeof(vector));
  cells.coord = malloc(cells.dim.x * cells.dim.y * 
      cells.dim.z *sizeof(intvector));
}

intvector id_to_coord(int cellid)
{
  intvector coord;
  int resz = (cellid)%(cells.dim.x * cells.dim.y);
  coord.z = (int)((cellid)/(cells.dim.x* cells.dim.y)) ;
  coord.y = (int)((resz)/cells.dim.x)  ;
  coord.x = (resz)%cells.dim.x ;  /* cells start with index 0 */
  return coord;
}

int coord_to_id(intvector coord)
{
  if(coord.x<0 || coord.y<0 || coord.z<0 || coord.x>=cells.dim.x ||
      coord.y>=cells.dim.y || coord.z>=cells.dim.z)
    return -1; /* Cell outside domain */
  else{
    int id = cells.dim.x * cells.dim.y * coord.z  + 
      cells.dim.x * coord.y + coord.x;
    return id;
  }
}


void particle_in_cell(particleData * pdata)
{
  int i;
  int c;
  intvector celldir;
  double box_length =  r_cutoff*h_global*particle_dx;

  for(i=0;i<cells.dim.x*cells.dim.y*cells.dim.z; i++)
    cells.partincell[i] = -1;

//#ifdef _OPENMP
//#pragma omp parallel for default(shared) private(i,c,celldir) 
//#endif
  for(i=0;i<N;i++)
  {
    for(c=0;c<dim;c++){
      (&celldir.x)[c] = (int)floor(((&pdata[i].pos.x)[c] - 
            (&xlow->x)[c])/box_length);
  //      if((&xhigh->x)[c] - (&pdata[i].pos.x)[c] < box_length)
    //      (&celldir.x)[c] -= 1; 
      if( (&celldir.x)[c] == (&cells.dim.x)[c] ||  (&celldir.x)[c] == (&cells.dim.x)[c]+1 )
        (&celldir.x)[c] = (&cells.dim.x)[c] -1 ;      
    }
    if(dim==2) celldir.z =0;
    pdata[i].cellid = coord_to_id(celldir);
    cells.cellofpart[i] = cells.partincell[(pdata[i].cellid)];   
    cells.partincell[(pdata[i].cellid)] = i;
  }
  printf("cells %d %d %d ", cells.dim.x, cells.dim.y, cells.dim.z);
  return;
}
