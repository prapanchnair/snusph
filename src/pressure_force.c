#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "forces.h"
#include "isph.h"

void pressure_forces( particleData * p_a, particleData * p_b, interactParams params,boolean ghost)
{
  int i;

  if(p_a->rho <= 0. || p_b->rho <= 0.){
    printf("Error Negative densities\n");
    exit(1);
  }
  if(isnan(p_a->pressure)!= 0 ){
    printf("nan in Pressure at %lf %lf \n",p_a->pos.x,p_a->pos.y);
    exit(1);
  }
  if(isnan(p_b->pressure)!= 0 ){
    printf("nan in Pressure at %lf %lf \n",p_b->pos.x,p_b->pos.y);
    exit(1);
  }

  double press = 0.;

  if(press_gradient==0){  /*Monaghan*/
    press = (p_a->pressure/(p_a->rho*p_a->rho) + p_b->pressure/(p_b->rho*p_b->rho));

    for(i = 0; i < dim; i++){
      (&p_a->acc.x)[i] += -p_b->mass*press*(&params.dwdx.x)[i];
//      (&p_b->acc.x)[i] += p_a->mass*press*(&params.dwdx.x)[i];
    }

  }else if(press_gradient == 1){
    press = (p_a->pressure - p_b->pressure)/(p_a->rho*p_b->rho);
    for(i = 0; i < dim; i++){
      (&p_a->acc.x)[i] += p_b->mass*press*(&params.dwdx.x)[i];
  //    (&p_b->acc.x)[i] += p_a->mass*press*(&params.dwdx.x)[i];
    }
  }
  else if(press_gradient == 2){
    //    press = (-p_a->pressure + p_b->pressure);
    if(ghost ==  false){
      press = (p_a->pressure + p_b->pressure);
      for(i = 0; i < dim; i++){
        (&p_a->acc.x)[i] += -p_b->mass*press*(&params.dwdx.x)[i]/(p_a->rho *p_a->rho);
    //    (&p_b->acc.x)[i] += p_a->mass*press*(&params.dwdx.x)[i]/(p_b->rho *p_b->rho);
      }
    }else {
      press = (p_a->pressure + p_b->pressure);
      for(i = 0; i < dim; i++){
        (&p_a->acc.x)[i] += -p_b->mass*press*(&params.dwdx.x)[i]/(p_a->rho *p_a->rho);
      }
    }

  }else{
    printf("\n Pressure Gradient approximation not defined \n");
    exit(1);
  }
}
void correct_pressure_force(particleData * pdata)
{
  int i,c;
  for(i = 0; i<N; i++){
    for(c=0;c<dim;c++)
      (&pdata[i].acc.x)[c] -= (pdata[i].pressure/(pdata[i].rho *pdata[i].rho))*(0.0 - (&pdata[i].gradW.x)[c])* pdata[i].mass;

  }
  return;
}
