#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "sph.h"
#include "particles.h"
#include "write.h"
#include "forces.h"
#include "isph.h"
#include "run.h"
#include "kernel.h"
#include "cell.h"

static void write_particles(particleData *pdata, simoutput * output);
static void write_grid_VTK(particleData *pdata, simoutput * output);
static void write_grid_TECPLOT(particleData *pdata, simoutput * output);
static void write_particles_VTK(particleData *pdata, simoutput * output, int step );
 
void write(particleData *pdata, int step)
{
  //double compare_time=0;
  //double compare_time_vtkp=0;
  //double compare_time_vtkg=0;
  //double compare_time_tp =0;
  //double compare_time_restart=0;
  /* write particles is not optional for now. Writing out anyways */
  if (fabs(sim_time) >=timefile->c_time){
    write_particles(pdata,timefile);
    timefile->c_time += timefile->dt;
  }   

  if(VTKgrid_is_on == true){
    if (fabs(sim_time) >=VTKgrid->c_time){
      write_grid_VTK(pdata,VTKgrid);
      VTKgrid->c_time += VTKgrid->dt;
    }   
  }
  if(VTKparticles_is_on == true){
    if (fabs(sim_time) >=VTKparticles->c_time){
      write_particles_VTK(pdata,VTKparticles, step);
      VTKparticles->c_time += VTKparticles->dt;
    }   
  }
  if(Tecplot_is_on == true){
    if (fabs(sim_time) >=Tecplot->c_time){
      write_grid_TECPLOT(pdata,Tecplot);
      Tecplot->c_time += Tecplot->dt;
    }   
  }


}

void write_grid_VTK(particleData *pdata, simoutput * output)
{
  char filename[30],  filenameline[30];                                                           
  sprintf(filename, "%stime%2.4lf.vtk",output->file_prefix, sim_time);         
  sprintf(filenameline, "lines_time%2.4lf.vtk", sim_time);         
  FILE *fp = fopen(filename, "w");
  FILE *fpline = fopen(filenameline, "w");  
  int Nx = (int)((xhigh->x - xlow->x)/particle_dx) +1;
  int Ny = (int)((xhigh->y - xlow->y)/particle_dx) +1;
  int Nz = (int)((xhigh->z - xlow->z)/particle_dx) +1;
  double dx = particle_dx; 
  double dy = particle_dx;
  double dz = particle_dx;
  if(dim==2){
    Nz =1;
    dz = 0.0;
  }
  vector x;                                                                    
  vector xr, periodic_corr;                                                    
  double rdotr ; /*rdotrp; *rdotrp for periodic boxes*/                       
  int i,id,c,j,k,l;
  for(c=0;c<dim;c++){  
    (&periodic_corr.x)[c] = (&xhigh->x)[c] - (&xlow->x)[c];     
  }
  fprintf(fp,"# vtk DataFile Version 3.0\n");                                  
  fprintf(fp,"particle point data\n");                                         
  fprintf(fp,"ASCII\n");                                                       
  fprintf(fp,"DATASET STRUCTURED_GRID\n");                                     
  fprintf(fp,"DIMENSIONS %d %d %d\n",Nx,Ny,Nz);                                
  fprintf(fp,"POINTS %d double\n",Nx*Ny*Nz);         
  double *** rho   = (double ***)malloc(Nx*sizeof(double**));
  double *** press = (double ***)malloc(Nx*sizeof(double**));
  double *** phase = (double ***)malloc(Nx*sizeof(double**));
  vector *** vel   = (vector ***)malloc(Nx*sizeof(vector**));
  vector *** pos   = (vector ***)malloc(Nx*sizeof(vector**));
  for (i = 0; i< Nx; i++) {
    rho[i]   = (double **) malloc(Ny*sizeof(double *));
    press[i] = (double **) malloc(Ny*sizeof(double *));
    phase[i] = (double **) malloc(Ny*sizeof(double *));
    vel[i]   = (vector **) malloc(Ny*sizeof(vector *));
    pos[i]   = (vector **) malloc(Ny*sizeof(vector *));
    for (j = 0; j < Ny; j++) {
      rho[i][j]   = (double *) malloc(Nz*sizeof(double ));
      press[i][j] = (double *) malloc(Nz*sizeof(double ));
      phase[i][j] = (double *) malloc(Nz*sizeof(double ));
      vel[i][j]   = (vector *) malloc(Nz*sizeof(vector ));
      pos[i][j]   = (vector *) malloc(Nz*sizeof(vector ));
    }
  }
  /*
   *Method 1 for 3d arrays*
   const int dim1, dim2, dim3;  
#define ARR(i,j,k) (array[dim2*dim3*i + dim3*j + k])
double * array = (double *)malloc(dim1*dim2*dim3*sizeof(double));
   * Method 2
   int dim1, dim2, dim3;
   int i,j,k;
   double *** array = (double ***)malloc(dim1*sizeof(double**));
   for (i = 0; i< dim1; i++) {
   array[i] = (double **) malloc(dim2*sizeof(double *));
   for (j = 0; j < dim2; j++) {
   array[i][j] = (double *)malloc(dim3*sizeof(double));
   }
   }
   */
  intvector celldir;
  int cellid;
  int p,q,r;
  intvector coord_neighbor;
  int neighb_id,id_b;
  double neighborhood = pow(r_cutoff * pdata[0].h,2.0);
  double box_length = pdata[0].h * r_cutoff;
  for(l = 0; l<Nz; l++){
    for(k = 0; k < Ny; k++){
      for(j = 0; j < Nx; j++){
        
        x.x = xlow->x + j*dx ; x.y =  xlow->y + k*dy; x.z =  xlow->z + l*dz;

        vel[j][k][l].x = 0;                                                    
        vel[j][k][l].y = 0.;                                                   
        vel[j][k][l].z = 0.;                                                   
        rho[j][k][l] = 0.;                                                     
        press[j][k][l] = 0.;                                                   
        phase[j][k][l] = 0.;                                                   

        for(c=0;c<dim;c++){
          (&celldir.x)[c] = (int)floor(((&x.x)[c] -                      
                (&xlow->x)[c])/box_length);
          if( (&celldir.x)[c] == (&cells.dim.x)[c] ||  (&celldir.x)[c] == (&cells.dim.x)[c]+1 )
            (&celldir.x)[c] = (&cells.dim.x)[c] -1 ;
        }
        if(dim==2) celldir.z =0;
        cellid =  coord_to_id(celldir);
//        celldir = id_to_coord(i);

        for(p=celldir.x-1;p<=celldir.x+1;p++){                                      
          for(q=celldir.y-1;q<=celldir.y+1;q++){                                    
            for(r=celldir.z-1;r<=celldir.z+1;r++){ 
              coord_neighbor.x = p;
              coord_neighbor.y = q;                                               
              coord_neighbor.z = r;         

              for(c=0;c<dim;c++)                                                  
              {                                                                   
                if(periodic[c] ==1){                                              
                  if( (&coord_neighbor.x)[c] == (&cells.dim.x)[c] ){              
                    (&coord_neighbor.x)[c] = 0;                                   
                  }else if((&coord_neighbor.x)[c] == -1){                         
                    (&coord_neighbor.x)[c] = (&cells.dim.x)[c] - 1;               
                  }                                                               
                }                                                                 
              }              
              neighb_id = coord_to_id(coord_neighbor);                            
              /* Asymmetric interactions, so neighbors in all directions are considered */
              if(neighb_id >= 0 )
              {
                id_b = cells.partincell[neighb_id];                               
                while(id_b >=0 ){                 
                  interactParams params;
                  params.rdotr = 0.0;
                  params.h = (pdata[id_b].h);
                  for(c = 0; c<dim;c++){                                          
                    (&params.xr.x)[c] = (&x.x)[c]-(&pdata[id_b].pos.x)[c];
                    if(periodic[c] && fabs((&params.xr.x)[c])>0.5*fabs((&periodic_corr.x)[c])){
                      if((&params.xr.x)[c] > 0.)                                  
                        (&params.xr.x)[c] += -(&periodic_corr.x)[c];              
                      else                                                        
                        (&params.xr.x)[c] += (&periodic_corr.x)[c];               
                    }                                                             
                    params.rdotr += (&params.xr.x)[c]*(&params.xr.x)[c];          
                  }            
                  if(params.rdotr <= neighborhood){ 
                    double h = h_global * particle_dx * r_cutoff;
                    double q = sqrt(rdotr)/h;                                            
                    double w = kernel(q,h);      
                    rho[j][k][l] += w*pdata[id_b].mass;                                          
                    press[j][k][l] += pdata[id_b].pressure*w*pdata[id_b].mass/pdata[id_b].rho; 
                    phase[j][k][l] += ((double)pdata[id_b].phase+2.0)*w*pdata[id_b].mass/pdata[id_b].rho;
                    vel[j][k][l].x += pdata[id_b].vel.x*w*pdata[id_b].mass/pdata[id_b].rho;  
                    vel[j][k][l].y += pdata[id_b].vel.y*w*pdata[id_b].mass/pdata[id_b].rho;                 
                    vel[j][k][l].z += pdata[id_b].vel.z*w*pdata[id_b].mass/pdata[id_b].rho;                  
                  }
                  id_b = cells.cellofpart[id_b];
                }
              }

            }
          }
        }
        fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", x.x, x.y, x.z);  

      
      }
    }
  }
  fprintf(fp,"POINT_DATA %d\n",Nx*Ny*Nz);                                      

  fprintf(fp,"\nSCALARS rho double \n");                                       
  fprintf(fp,"\nLOOKUP_TABLE default \n");                                     
  for(l = 0;l<Nz; l++){                                                        
    for(j = 0; j < Ny; j++){                                                   
      for(k = 0; k < Nx; k++){                                                 
        fprintf(fp,"%5.8lf\n",rho[k][j][l]);                                   
      }                                                                        
    }                                                                          
  }                                                                            
  fprintf(fp,"\nSCALARS pressure double \n");                                  
  fprintf(fp,"\nLOOKUP_TABLE default \n");                                     
  for(l = 0;l<Nz; l++){                                                        
    for(j = 0; j < Ny; j++){                                                   
      for(k = 0; k < Nx; k++){                                                 
        fprintf(fp,"%5.8lf\n",press[k][j][l]);                                 
      }                                                                        
    }                                                                          
  }                            
  fprintf(fp,"\nSCALARS phase double \n");                                     
  fprintf(fp,"\nLOOKUP_TABLE default \n");                                     
  for(l = 0;l<Nz; l++){                                                        
    for(j = 0; j < Ny; j++){                                                   
      for(k = 0; k < Nx; k++){                                                 
        fprintf(fp,"%5.8lf\n",phase[k][j][l]);                                 
      }                                                                        
    }                                                                          
  }                                                                            
  fprintf(fp,"\nVECTORS vel double\n");                                        
  for(l = 0;l<Nz; l++){                                                        
    for(j = 0; j < Ny; j++){                                                   
      for(k = 0; k < Nx; k++){                                                 
        fprintf(fp,"%3.8lf %3.8lf %3.8lf\n",vel[k][j][l].x,vel[k][j][l].y,vel[k][j][l].z);
      }                                                                        
    }                                                                          
  }                   
 /* double yy = xlow->y;  
  double xx = xlow->x;
 for(k = 0; k < Ny; k++){                                                 
    fprintf(fpline,"%3.8lf %3.8lf \n",yy, vel[(Nx-1)/2][k][(Nz-1)/2].x);//,xx,vel[k][(Nx-1)/2][(Nz-1)/2].y);
    yy +=dy;
    xx +=dx;
  }*/
  int kp;
  for(kp=0;kp<N;kp++){
      if(pdata[kp].pos.x <= 3.0*particle_dx + xlow->x)
    fprintf(fpline,"%3.8lf %3.8lf \n", pdata[kp].pos.y, pdata[kp].vel.x);//,xx,vel[k][(Nx-1)/2][(Nz-1)/2].y);

  }
  printf("writing file: %s with %d %d %d grid\n",filename, Nx, Ny, Nz);        
  fclose(fp);                   
  fclose(fpline);
}

void write_grid_TECPLOT(particleData *pdata,simoutput * output)
{

}

static void write_particles(particleData *pdata, simoutput * output)
{
  /*This file is the restart file as well*/
  int i, c;
  char filename[30];
  sprintf(filename, "%s%2.5lf",output->file_prefix, sim_time);
  FILE *fp = fopen(filename, "w");
 
  int write_version = 6; 
  //int count = 0;
  fprintf(fp,"%d\n",write_version);                                              
  /*time info */                                                                
  fprintf(fp,"%d %lf %lf %lf %d\n",step, sim_time, end_time,dt,set_dt);                        

  /*Domain information */                                                       
  fprintf(fp,"%d\n",dim);                                                       
  for(c=0;c<dim;c++) fprintf(fp,"%lf\n",(&xlow->x)[c]);                         
  for(c=0;c<dim;c++) fprintf(fp,"%lf\n",(&xhigh->x)[c]);                        
  for(c=0;c<3;c++) fprintf(fp,"%d\n",periodic[c]);                              

  /*Boundary */                                                                 
  fprintf(fp,"%d\n",GHOSTBC);                                                   
  for(c=0;c<2*dim;c++) fprintf(fp,"%d\n",domain_bc[c]);                         
  fprintf(fp,"%lf %lf %lf\n",wall_velocity.x, wall_velocity.y, wall_velocity.z);                                            
  fprintf(fp,"%lf\n",kappa);                                                    

  /*Pressure Solver info */                                                     
  fprintf(fp,"%d %d %d %le \n",L_solver, laplacian_type, max_iter, bicg_eps);                                                    

  /*Difference approximations*/                                                 
  fprintf(fp,"%d %d %d %d\n",press_gradient, divergence, viscous, viscous_model);

  /*kernel and related*/                                                        
  fprintf(fp,"%d %lf %lf %lf\n",kernel_model, r_cutoff, h_global, particle_dx);                

  /*Constant parameters*/                                                       
  for(c=0;c<dim;c++) fprintf(fp,"%lf ",(&bodyforce.x)[c]);                       
  fprintf(fp,"\n");                                                                 
  fprintf(fp,"%lf %lf %lf %lf %lf\n",viscosity[0], viscosity[1],               
      viscosity[2], viscosity[3], viscosity[4]);                             
  fprintf(fp,"%d\n",rigid_body_sim);                                            
  fprintf(fp,"%d\n",integrator_model);     
  /*Outputs*/                                                                   
  fprintf(fp,"%s\n",timefile->file_prefix);                                     
  fprintf(fp,"%lf\n",timefile->dt);                                             
  fprintf(fp,"%d\n",timefile->datatype);                                        

  fprintf(fp,"%d\n",VTKparticles_is_on);                                        
  if(VTKparticles_is_on == 1){                                                  
    fprintf(fp,"%s\n",VTKparticles->file_prefix);                               
    fprintf(fp,"%lf\n",VTKparticles->dt);                                       
    fprintf(fp,"%d\n",VTKparticles->datatype);                                  
    fprintf(fp,"%d\n",VTKparticles->gridx);                                     
    fprintf(fp,"%d\n",VTKparticles->gridy);                                     
    fprintf(fp,"%d\n",VTKparticles->gridz);                                     
  }                                                                             
  fprintf(fp,"%d\n",VTKgrid_is_on);                                             
  if(VTKgrid_is_on == 1){                                                       
    fprintf(fp,"%s \n",VTKgrid->file_prefix);                                   
    fprintf(fp,"%lf\n",VTKgrid->dt);                                            
    fprintf(fp,"%d \n",VTKgrid->datatype);                                      
    fprintf(fp,"%d \n",VTKgrid->gridx);                                         
    fprintf(fp,"%d \n",VTKgrid->gridy);                                         
    fprintf(fp,"%d \n",VTKgrid->gridz);                                         
  }                                                                             

  /*read_particles*/                                                            
  fprintf(fp,"%d \n",N);                                                        
  /* pos vel phase pressure rho mass h */                                       
  for(i=0;i<N;i++){                                                             
    fprintf(fp, "%4.10f %4.10f %4.10f %4.10f %4.10f %4.10f %d %4.16f %lf %lf %4.10f %d\n",pdata[i].pos.x,pdata[i].pos.y,pdata[i].pos.z,pdata[i].vel.x,pdata[i].vel.y,pdata[i].vel.z, pdata[i].phase, pdata[i].pressure, pdata[i].rho, pdata[i].mass, pdata[i].h, pdata[i].freesurface);
  }         
  
  printf("writing file: %s with %d particles %lf\n",filename, N ,output->dt);
  fclose(fp);
}

void write_particles_VTK(particleData *pdata, simoutput * output, int step)
{
  char filename[30];
  sprintf(filename, "%stime_%d.vtk",output->file_prefix, step);
  FILE *fp = fopen(filename, "w");
  //ng_N=0;
  //vector n_VTK[N-1];
  fprintf(fp,"# vtk DataFile Version 3.0\n");
  fprintf(fp,"#SPH Data\n");
  fprintf(fp,"ASCII\n");
  fprintf(fp,"DATASET UNSTRUCTURED_GRID\n");
  fprintf(fp,"POINTS %d double\n",N + g_N);

  int i;
  i=0;

  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", pdata[i].pos.x, pdata[i].pos.y, pdata[i].pos.z);
  }
  if(GHOSTBC){
    for(i=0;i<g_N;i++){
      fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", g_pdata[i].pos.x, g_pdata[i].pos.y, g_pdata[i].pos.z);
    }
  }

  fprintf(fp,"POINT_DATA %d\n",N+g_N);
  fprintf(fp,"SCALARS density double\n");
  fprintf(fp,"LOOKUP_TABLE default\n");

  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf \n",  pdata[i].rho );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf \n",  pdata[g_pdata[i].real_id].rho );
  }
  fprintf(fp,"SCALARS neighbors int\n");
  fprintf(fp,"LOOKUP_TABLE default\n");

  for(i=0;i<N;i++){
    fprintf(fp,"%d \n",  pdata[i].num_neighbors );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%d \n",  g_pdata[i].num_neighbors );
  }
  fprintf(fp,"SCALARS cell int\n");
  fprintf(fp,"LOOKUP_TABLE default\n");

  for(i=0;i<N;i++){
    fprintf(fp,"%d \n",  pdata[i].cellid );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%d \n",  g_pdata[i].cellid );
  }
  fprintf(fp,"SCALARS id int\n");
  fprintf(fp,"LOOKUP_TABLE default\n");

  for(i=0;i<N;i++){
    fprintf(fp,"%d \n",  i );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%d \n",  i);
  }
  fprintf(fp,"SCALARS interface int\n");
  fprintf(fp,"LOOKUP_TABLE default\n");

  for(i=0;i<N;i++){
    fprintf(fp,"%d \n",  pdata[i].interface_tag );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%d \n",  g_pdata[i].interface_tag );
  }
  fprintf(fp,"SCALARS phase int\n");
  fprintf(fp,"LOOKUP_TABLE default\n");

  for(i=0;i<N;i++){
    fprintf(fp,"%d \n",  pdata[i].phase );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%d \n",  g_pdata[i].phase );
  }
  fprintf(fp,"SCALARS wsum double\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf \n", pdata[i].wsum);
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf \n", pdata[g_pdata[i].real_id].wsum);
  }
  fprintf(fp,"SCALARS surfacekappa double\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf \n", pdata[i].s_kappa);
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf \n", pdata[g_pdata[i].real_id].s_kappa);
  }
  fprintf(fp,"SCALARS ghost int\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%d \n", i);
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%d \n", g_pdata[i].real_id);
  }
  fprintf(fp,"SCALARS velMag double\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf \n", sqrt(pdata[i].vel.x * pdata[i].vel.x +pdata[i].vel.y * pdata[i].vel.y + pdata[i].vel.z * pdata[i].vel.z )  );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf \n", sqrt(g_pdata[i].vel.x * g_pdata[i].vel.x +g_pdata[i].vel.y * g_pdata[i].vel.y + g_pdata[i].vel.z * g_pdata[i].vel.z )  );
  }
  fprintf(fp,"SCALARS pressure double\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf \n", pdata[i].pressure);
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf \n", pdata[g_pdata[i].real_id].pressure);
  }
  fprintf(fp,"SCALARS divV double\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf \n", pdata[i].u);
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf \n", g_pdata[i].u);
  }
  fprintf(fp,"SCALARS modC double\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf \n", pdata[i].modC);
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf \n", g_pdata[i].modC);
  }
  /*  fprintf(fp,"SCALARS divP double\n");
      fprintf(fp,"LOOKUP_TABLE default\n");
      for(i=0;i<N;i++){
      fprintf(fp,"%2.8lf \n", pdata[i].div_pos);
      }*/
  fprintf(fp,"SCALARS incompressible int\n");
  fprintf(fp,"LOOKUP_TABLE default\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%d \n", pdata[i].incompressible);
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%d \n", g_pdata[i].incompressible);
  }
  /*
     fprintf(fp,"SCALARS id int\n");
     fprintf(fp,"LOOKUP_TABLE default\n");
     for(i=0;i<N;i++){
     fprintf(fp,"%d \n", i);
     }
     */
  fprintf(fp,"VECTORS gradW double\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", pdata[i].gradW.x, pdata[i].gradW.y, pdata[i].gradW.z );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", g_pdata[i].gradW.x, g_pdata[i].gradW.y, g_pdata[i].gradW.z );
  }
  fprintf(fp,"VECTORS velocity double\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", pdata[i].vel.x, pdata[i].vel.y, pdata[i].vel.z );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", g_pdata[i].vel.x, g_pdata[i].vel.y, g_pdata[i].vel.z );
  }
  fprintf(fp,"VECTORS interfacenormal double\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", pdata[i].n.x, pdata[i].n.y, pdata[i].n.z );
  }
  for(i=0;i<g_N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", g_pdata[i].vel.x, g_pdata[i].vel.y, g_pdata[i].vel.z );
  }

  fprintf(fp,"VECTORS accelation_mu double\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", pdata[i].acc_mu.x, pdata[i].acc_mu.y, pdata[i].acc_mu.z );
  }
  /*  fprintf(fp,"VECTORS cellid int\n");
      for(i=0;i<N;i++){
      fprintf(fp,"%d %d %d\n", pdata[i].cell.x, pdata[i].cell.y, pdata[i].cell.z );
      }
      */
  fprintf(fp,"VECTORS accelation_total double\n");
  for(i=0;i<N;i++){
    fprintf(fp,"%2.8lf %2.8lf %2.8lf\n", pdata[i].accn.x, pdata[i].accn.y, pdata[i].accn.z );
  }
  printf("writing file: %s with %d particles\n",filename, N+g_N);

  fclose(fp);
}



void write_time_data(particleData * pdata)
{
/*
  char filename[30];
  sprintf(filename, "time_data.dat");
  double press[5];
  double vel;
  
  int i; 
  for (i=0;i<5;i++)
    press[i]= 0.0;
  if(sim_time==0.0){
    printf("Beginning to write time data \n");
    FILE *fp = fopen(filename, "w");
    fprintf(fp, " %2.8lf %2.8lf %2.8lf  %2.8lf %2.8lf  %2.8lf %2.8lf %2.8lf %2.8lf %2.8lf\n", sim_time, V_r.y, press[0], press[1], press[2],press[3],press[4], force_r.x, force_r.y, c_g.y);
    fclose(fp);
  }else{
    FILE *fp = fopen(filename, "a");

    while(plist){
      particle * p = plist->p;
      plist = plist->next;

      switch(p->id)
      {
        case 20008:
          press[0] = p->pressure;
        case 20028:
          press[1] = p->pressure;
        case 20064:
          press[2] = p->pressure;
        case 20142:
          press[3] = p->pressure;
        case 20294:
          press[4] = p->pressure;
      }
    }
    fprintf(fp, "%2.8lf %2.8lf %2.8lf %2.8lf %2.8lf %2.8lf %2.8lf %2.8lf %2.8lf %2.8lf\n", sim_time, V_r.y, press[0], press[1], press[2],press[3],press[4], force_r.x, force_r.y, c_g.y);
fclose(fp);
  }
  */
}

