#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>

#include "sph.h"
#include "particles.h"
#include "isph.h"
#include "integrator.h"
#include "forces.h"
#include "kernel.h"
#include "write.h"
#include "run.h"
#include "read_input.h"
#include "cell.h"

static void interpret();
static void parse(char *, char **, int);

particleData * read_input(FILE *fptr)
{
   /*Read a line*/
  while(!feof(fptr)){
    int m,n;
    m = 0;
    while(1) {
      if(fgets(&line[m],MAXLINE-m,fptr) ==NULL) break;    
      else n = strlen(line) + 1;
      
      m = n-2;
      while(m >=0 && isspace(line[m])) m--;
      if(m < 0 || line[m] != '&') break;
    }
    
    if(m!=0)
      interpret();
  }
  return pd;
}

static void interpret()
{
  strcpy(copy,line);

  char *ptr = copy;
  while (*ptr) {
    if (*ptr == '#') {
      *ptr = '\0';
      break;
    }
    ptr++;
  }
 
  command = strtok(copy," \t\n\r\f");
  if (command == NULL) return;

  int maxarg;
  char **arg;
  arg = malloc(10*sizeof(char *));

  if(strcmp(command,"dimension")==0){       /*Dimension*/
    maxarg = 1;   
    parse(ptr, arg, maxarg);
    dim = atoi(arg[0]);
    printf("%s : %d\n",command,dim);

    if(dim < 1 || dim > 3) {
      printf("Error: # of dimensions not correct should be 1, 2 or 3\n");
      exit(1);
    }
  }

  else if(strcmp(command,"periodic")==0){  /*Periodicity*/
    if(dim < 1 || dim > 3) {
      printf("Dimension of the problem not defined \n");
      exit(1);
    } 
    maxarg = dim;
    parse(ptr, arg, maxarg);
    int i;
    for (i = 0; i < maxarg; i++)      
      periodic[i] = atoi(arg[i]);
    printf("%s : %d ",command,periodic[0]);
    for(i=1;i<dim;i++)
      printf("%d ", periodic[i]);
    printf("\n");
  }
  else if(strcmp(command,"domain")==0){       /*Domain*/
    if(dim < 1 || dim > 3) {
      printf("Dimension of the problem not defined \n");
      exit(1);
    } 
    maxarg = 2*dim;
    parse(ptr,arg,maxarg);
    int i;
    xlow = (vector *) malloc (sizeof(vector));
    xhigh = (vector *) malloc (sizeof(vector));
   
    for(i = 0; i < dim; i++)
      (&xlow->x)[i] = atof(arg[i]);
    
    for(i = 0; i < dim; i++)
      (&xhigh->x)[i] = atof(arg[i+dim]);
      
    printf("xlow: %lf ",xlow->x);
    for(i=1;i<dim;i++)
      printf("%lf ", (&xlow->x)[i]);
    printf("\n");
    printf("xhigh: %lf ",xhigh->x);
    for(i=1;i<dim;i++)
      printf("%lf ", (&xhigh->x)[i]);
    printf("\n");
  }
  else if (strcmp(command,"bc_type")==0){ /*Ghost Wall BC */
    if(dim < 1 || dim > 3) {
      printf("Dimension of the problem not defined \n");
      exit(1);
    } 
    maxarg = 2*dim;
    GHOSTBC = true;
    parse(ptr, arg, maxarg);
    int i;
    printf("Domain BC: ");
    for(i=0;i<2*dim;i++){
      domain_bc[i] = atoi(arg[i]);
      printf("%d ",domain_bc[i]);
    }
    // 1 - for symmetry, 2 - for no slip wall, 0 - default
      printf(" \n");
  }

   /* added prapanj */ 

  else if (strcmp(command,"SPH")==0){ /*SPH CSPH and XSPH*/ 
    maxarg = 3;
    parse(ptr, arg, maxarg);
    
    SPH = atoi(arg[0]);
    CSPH = atoi(arg[1]);
    XSPH = atoi(arg[2]);
    printf("SPH: %d %d %d\n",SPH, CSPH, XSPH);

  }
  else if (strcmp(command,"ISPH")==0){ /*ISPH solver */
    maxarg = 5;
    ISPH = true;
    parse(ptr, arg, maxarg);
    L_solver = atoi(arg[0]);
    laplacian_type = atoi(arg[1]);
    max_iter = atoi(arg[2]);
    bicg_eps = atof(arg[3]);
    div_corr  = atoi(arg[4]);
    printf("ISPH: %d %d %d %2.8lf %d\n",L_solver,laplacian_type, max_iter, bicg_eps, div_corr);
  }
  else if (strcmp(command,"def_grad")==0){ /*ISPH solver */                     
    maxarg = 0;                                                                 
    defgrad = true;                                                             
    printf("Deformation Gradient Based Formulation \n");                        
  } 
  else if (strcmp(command,"press_gradient")==0){ /*ISPH solver */
    maxarg = 1;
    parse(ptr, arg, maxarg);
    press_gradient = atoi(arg[0]);
    printf("Pressure Gradient approximation: %d \n",press_gradient);
  }
  else if (strcmp(command,"divergence")==0){ /*ISPH solver */
    maxarg = 1;
    parse(ptr, arg, maxarg);
    divergence = atoi(arg[0]);
    printf("Divergence of velocity approximation: %d \n",divergence);
  }
  else if(strcmp(command,"kernel")==0){       /*Kernel*/
    maxarg = 2;   
    parse(ptr, arg, maxarg);
    kernel_model = atoi(arg[0]);
    r_cutoff = atof(arg[1]);
    double a = kernel(1,1);/* Required to initiate the coefficients*/
    printf("%s : %d %lf, init value %lf\n",command,kernel_model,r_cutoff,a);
  }
  else if(strcmp(command,"h_ratio")==0){ /* h/dx ratio */
    maxarg = 1;
    parse(ptr, arg, maxarg);
    h_global = atof(arg[0]);
    printf("Ratio h/dx: %lf\n",h_global);
   
  } else if(strcmp(command,"update_density")==0){ /*Density Update rule*/
    maxarg = 3;
    parse(ptr, arg, maxarg);

    density = atoi(arg[0]);
    rho_sum = atoi(arg[1]);
    h_sum = atoi(arg[2]);

    printf("Update_density: %d %d %d\n", density, rho_sum, h_sum);
  } 

  else if(strcmp(command,"time")==0){ /*Time params*/
    maxarg = 4;
    parse(ptr, arg, maxarg);    
    sim_time = atof(arg[0]);
    end_time = atof(arg[1]);
    dt = atof(arg[2]);
    dt_0 = dt;
    set_dt = atoi(arg[3]);

    printf("Time: %lf %lf %2.8lf %d\n", sim_time, end_time, dt, set_dt);
  }
  else if(strcmp(command,"write")==0) { /*What files to write and when*/
    maxarg = 7;
    parse(ptr, arg, maxarg);
/*    file_prefix = (char *) malloc (sizeof(char)*20);*/
    int type;
    type = atoi(arg[0]);
    switch(type){
      case 0:
        timefile_is_on = true;
        timefile= (simoutput *) malloc (sizeof(simoutput));
        timefile->file_prefix = (char *) malloc (sizeof(char)*20);
        memcpy(timefile->file_prefix, arg[1],10);
        timefile->dt = atof(arg[2]);
        timefile->datatype = atoi(arg[3]);
        timefile->c_time=0.0;
        printf("write option %s\n",timefile->file_prefix);
        break;
      case 1:
        VTKparticles_is_on = true;
        VTKparticles= (simoutput *) malloc (sizeof(simoutput));
        VTKparticles->file_prefix = (char *) malloc (sizeof(char)*20);
        memcpy(VTKparticles->file_prefix, arg[1],10);
        VTKparticles->dt = atof(arg[2]);
        VTKparticles->datatype = atoi(arg[3]);
        VTKparticles->c_time=0.0;
        VTKparticles->gridx =1;
        VTKparticles->gridy =1;
        VTKparticles->gridz =1;
        printf("write option %s\n",VTKparticles->file_prefix);
        break;
      case 2:
        VTKgrid_is_on = true;
        VTKgrid= (simoutput *) malloc (sizeof(simoutput));
        VTKgrid->file_prefix = (char *) malloc (sizeof(char)*20);
        memcpy(VTKgrid->file_prefix, arg[1],10);
        VTKgrid->dt = atof(arg[2]);
        VTKgrid->datatype = atoi(arg[3]);
        VTKgrid->gridx = atoi(arg[4]);
        VTKgrid->gridy = atoi(arg[5]);
        VTKgrid->gridz = atoi(arg[6]);
        VTKgrid->c_time=0.0;
        printf("write option %s\n",VTKgrid->file_prefix);
        break;
      case 3:
        Tecplot_is_on = true;
        Tecplot= (simoutput *) malloc (sizeof(simoutput));
        Tecplot->file_prefix = (char *) malloc (sizeof(char)*20);
        memcpy(Tecplot->file_prefix, arg[1],10);
        Tecplot->dt = atof(arg[2]);
        Tecplot->datatype = atoi(arg[3]);
        Tecplot->gridx = atoi(arg[4]);
        Tecplot->gridy = atoi(arg[5]);
        Tecplot->gridz = atoi(arg[6]);
        Tecplot->c_time=0.0;
        printf("write option %s\n",Tecplot->file_prefix);
        break;
    }
  }
  else if(strcmp(command,"write_restart")==0) { /*write particles in VTK format*/
    maxarg = 1;
    parse(ptr, arg, maxarg);
    wrstart = atof(arg[0]);

    printf("Write restart: %lf\n", wrstart);
  }

  else if(strcmp(command,"body_force")==0) { /*If Body force*/
    if(dim < 1 || dim > 3) {
      printf("Dimension of the problem not defined \n");
      exit(1);
    } 
    maxarg = dim;
    parse(ptr,arg,maxarg);
    int i;   
    body_force = true;   
    for (i = 0; i < maxarg; i++)      
      (&bodyforce.x)[i] = atof(arg[i]);  
    printf("Body Force: %lf %lf\n", bodyforce.x, bodyforce.y);
  }
  else if(strcmp(command,"surface_tension")==0) { /*If Body force*/            
    maxarg = 1;                                                                

    parse(ptr,arg,maxarg);                                                     
    surf_tens = true;                                                          
    surf_sigma = atof(arg[0]);                                                 
    printf("Surface tension: %lf\n", surf_sigma);                              
  }
  else if(strcmp(command,"pressure")==0) { /*Pressure*/
    maxarg = 1;
    pressure = true;
    parse(ptr, arg, maxarg);
    press_model = atoi(arg[0]);
    switch (press_model){
    case 0:  maxarg = 3;/*For water Monaghan (Batchelor)*/
      parse(ptr, arg, maxarg);
      press_params[0] = atof(arg[0]);
      press_params[1] = atof(arg[1]);
      press_params[2] = atof(arg[2]);
      break;
    case 1: maxarg = 1;/*For water Morris: Speed of sound based*/
      parse(ptr, arg, maxarg);
      press_params[0] = atof(arg[0]);
      break;
    case 2: maxarg = 1;/*For gas*/
      parse(ptr, arg, maxarg);
      press_params[0] = atof(arg[0]);
      break;
    }
    printf("press_params: %d %lf\n",press_model,press_params[0]);
  }
  else if(strcmp(command,"potential")==0) { /*Potential*/                       
    potential = true;                                                           
    maxarg = 4;                                                                 
    parse(ptr, arg, maxarg);                                                    
    int i;                                                                      
    for(i = 0; i < maxarg; i++)                                                 
      potential_params[i] = atof(arg[i]);                                       
    printf("potential_params: %lf %lf\n",potential_params[0],potential_params[1]);
  }  
  else if(strcmp(command,"viscous")==0) { /*Viscous flow*/
    viscous = true;
    maxarg = 1;
    parse(ptr, arg, maxarg); 
    viscous_model = atoi(arg[0]);
    printf("Viscous Model: %d\n",viscous_model);
  }
  else if(strcmp(command,"viscosity")==0) { /*Viscous flow*/
    maxarg = 2;
    parse(ptr, arg, maxarg); 
    int phase;
    phase =  atoi(arg[0]);
    if(phase<=0){
      printf("Visc. defined for negative phasees\n");
exit(1);
    }else viscosity[phase] = atof(arg[1]);
    printf("Phase %d's viscosity: %lf\n",phase, viscosity[phase]);
  }
  else if(strcmp(command,"density_ratio")==0) { /*Viscous flow*/
    maxarg = 2;
    parse(ptr, arg, maxarg); 
    int phase;
    phase =  atoi(arg[0]);
    if(phase<=0){
      printf("Density. defined for negative phasees\n");
exit(1);
    }else rho[phase] = atof(arg[1]);
    printf("Phase %d's density: %lf\n",phase, rho[phase]);
  }
  else if(strcmp(command,"rigidbody")==0) { /*rigid body*/                     
    rigid_body_sim = true;                                                     
    maxarg = 3;                                                                
    parse(ptr, arg, maxarg);                                                   
    int k;
    for(k=0;k<10;k++){
      V_r[k].x = atof(arg[0]);                                                      
      V_r[k].y = atof(arg[1]);                                                      
      V_r[k].z = atof(arg[2]);
    }
    printf("Rigid body initial velocity: %lf %lfi %lf\n",V_r[0].x, V_r[0].y,V_r[0].z);             
  }                                                                            
  else if(strcmp(command,"wall_velocity")==0) { /*velocity of no slip wall*/                     
    maxarg = 1;                                                                
    parse(ptr, arg, maxarg);                                                   
    wall_velocity.x = atof(arg[0]);                                                  
    wall_velocity.y = atof(arg[1]);                                                  
    wall_velocity.z = atof(arg[2]);                                                  
    printf("No slip wall velocity: %lf  %lf %lf \n",wall_velocity.x, wall_velocity.y, wall_velocity.z);             
  }                                                                            
  else if(strcmp(command,"fixed_velocity")==0) { /*any fixed velocity input*/  
    rigid_fixed_velocity = true;                                               
    maxarg = 2;                                                                
    parse(ptr, arg, maxarg);                                                   
    int k;
    for(k=0;k<10;k++){
      V_r[k].x = atof(arg[0]);                                                      
      V_r[k].y = atof(arg[1]);                                                      
      V_r[k].z = atof(arg[2]);
    }
    printf("Fixed velocity: %lf %lf\n",V_r[0].x, V_r[0].y);                          
  }                    
  else if(strcmp(command,"integrator")==0) { /*Integrator choice*/
    maxarg = 1;
    parse(ptr, arg, maxarg);
    integrator_model = atoi(arg[0]);
    printf("Integrator model: %d\n",integrator_model);
  }
  else if(strcmp(command,"particles")==0) { /*Particles read*/
    maxarg = 1;
    parse(ptr, arg, maxarg);
    if(strcmp(arg[0],"file")==0) {
      maxarg = 1;
      parse(ptr, arg, maxarg);
      FILE *file_particles;
      file_particles = fopen(arg[0], "r");
      if(file_particles==NULL){
        printf ("Unable to open particle file\n");
        exit(1);
      }      
    } else if (strcmp(arg[0],"model")==0) {
      maxarg = 2;
      parse(ptr, arg, maxarg);

      int particle_model = atoi(arg[0]);
      particle_dx = atof(arg[1]);

      pd = create_particles(particle_model);
    } else if (strcmp(arg[0],"expression")==0) {
      maxarg = 3 ;
      parse(ptr, arg, maxarg);
      particle_dx = atof(arg[0]);
      num_phases = atoi(arg[1]);
      int expr_phase[10];
      char * expr_string[10];
      if(strcmp(arg[2], "phases")==0) {
        maxarg = num_phases*2;
        parse(ptr, arg, maxarg);
        int c;
        for(c=0;c<num_phases;c++){
          expr_phase[c] = atoi(arg[2*c ]);
          expr_string[c] =arg[2*c + 1] ;
        }
      }else {
        printf("Phases geometry description not found \n ");
        exit(1);
      }
      pd = create_particles_from_expression(expr_phase, expr_string);
      printf("Expression to create particles: \n ");
      printf("Total number of phases %d \n", num_phases);
      int c;
      for(c=0;c<num_phases;c++)
        printf("\t Phase %d : %s \n ", expr_phase[c], expr_string[c]);
    } else {
      printf ("Check particle input\n");
      exit(1);
    }
  }
  free(arg);
  return;
}

static void parse(char *ptr, char **arg, int maxarg)
{
  int narg = 0;
  while (1) {
    if (narg == maxarg){
      printf("more than required %d args: Ignoring rest\n",maxarg);
      break;
    }
    arg[narg] = strtok(NULL," \t\n\r\f");
  
    if(!arg[narg]){
      printf("Error: number of parameters insufficient: %s\n", command);
      exit(1);
    }    
    while (*ptr) {
      if (*ptr == '#') {
	*ptr = '\0';
	break;
      }
      ptr++;
    }
    if (arg[narg]) narg++;
    if(narg==maxarg) break;
  }
}

particleData * read_restart(FILE *fp){                                             
  int i,c,err = 0;                                                                      
  int read_version = 6;                                                         
  int file_version;                                                             
  err = fscanf(fp,"%d\n",&file_version);                                              
  if(file_version != read_version){                                             
    printf("Restart file does not correspond to reading function \n Continuing with risk...\n");                                             
  }                                                                             
  /*time info */                                                                
 err += fscanf(fp,"%d %lf %lf %lf %d\n",&step, &sim_time, &end_time, &dt, &set_dt);                        

  /*Domain information */                                                       
 err +=  fscanf(fp,"%d\n",&dim);                                                       
  xlow = (vector *) malloc (sizeof(vector));                                    
  xhigh = (vector *) malloc (sizeof(vector));                                   
  for(c=0;c<dim;c++) err += fscanf(fp,"%lf\n",&(&xlow->x)[c]);                         
  for(c=0;c<dim;c++) err +=fscanf(fp,"%lf\n",&(&xhigh->x)[c]);                        
  for(c=0;c<3;c++) err +=fscanf(fp,"%d\n",&periodic[c]);                              

  /*Boundary */                                                                 
 err +=   fscanf(fp,"%u\n",&GHOSTBC);                                                   
  for(c=0;c<2*dim;c++) err += fscanf(fp,"%d\n",&domain_bc[c]);                         
 err +=   fscanf(fp,"%lf %lf %lf\n",&wall_velocity.x,&wall_velocity.y,&wall_velocity.z);                                            
 err +=   fscanf(fp,"%lf\n",&kappa);                                                    

  /*Pressure Solver info */                                                     
 err +=   fscanf(fp,"%d %d %d %le\n",&L_solver, &laplacian_type, &max_iter,          
      &bicg_eps);                                                    
  printf("Bicgeps %le\n",bicg_eps);

  /*Difference approximations*/                                                 
 err +=   fscanf(fp,"%d %d %u %d \n",&press_gradient, &divergence, &viscous, &viscous_model);
  printf("Derivatives %d %d %d %d \n",press_gradient, divergence, viscous, viscous_model);

  /*kernel and related*/                                                        
 err +=   fscanf(fp,"%d %lf %lf %lf\n",&kernel_model, &r_cutoff, &h_global, &particle_dx);                

  /*Constant parameters*/                                                       
  for(c=0;c<dim;c++) err+= fscanf(fp,"%lf ",&(&bodyforce.x)[c]);                      
 err +=   fscanf(fp,"\n");                                                                 
  printf("body force %lf %lf\n",bodyforce.x, bodyforce.y);                                       
 err +=   fscanf(fp,"%lf %lf %lf %lf %lf\n",&viscosity[0], &viscosity[1],               
      &viscosity[2], &viscosity[3], &viscosity[4]);                             
  printf("Viscosity of phase 1  %lf\n",viscosity[1]);                                          
 err +=   fscanf(fp,"%u\n",&rigid_body_sim);                                            
 err +=   fscanf(fp,"%d\n",&integrator_model);                                          
  printf("Integrator model %d\n",integrator_model);                                          

  /*Outputs*/                                                                   
  timefile_is_on = true;                                                        
  timefile= (simoutput *) malloc (sizeof(simoutput));                           
  timefile->file_prefix = (char *) malloc (sizeof(char)*20);                    
 err +=   fscanf(fp,"%[^\n]",timefile->file_prefix);                                     
 err +=   fscanf(fp,"%lf\n",&timefile->dt);                                             
  err +=  fscanf(fp,"%d\n",&timefile->datatype);                                        
  timefile->c_time = 0.0;                                                       
  printf("file prefix %s\n",timefile->file_prefix);                                          

  err +=  fscanf(fp,"%u\n",&VTKparticles_is_on);                                        
  if(VTKparticles_is_on == 1){                                                  
    VTKparticles= (simoutput *) malloc (sizeof(simoutput));                     
    VTKparticles->file_prefix = (char *) malloc (sizeof(char)*20);              
    err +=  fscanf(fp,"%[^\n]",VTKparticles->file_prefix);                               
    err +=  fscanf(fp,"%lf\n",&VTKparticles->dt);                                       
    err +=  fscanf(fp,"%d\n",&VTKparticles->datatype);                                  
    err +=  fscanf(fp,"%d\n",&VTKparticles->gridx);                                     
    err +=  fscanf(fp,"%d\n",&VTKparticles->gridy);                                     
    err +=  fscanf(fp,"%d\n",&VTKparticles->gridz);                                     
    VTKparticles->c_time = 0.0;                                                 
    printf("Dt of vtk particles %lf\n",VTKparticles->dt);                                       
  }                                       

  err +=  fscanf(fp,"%u\n",&VTKgrid_is_on);                                             
  if(VTKgrid_is_on == 1){                                                       
    VTKgrid= (simoutput *) malloc (sizeof(simoutput));                          
    VTKgrid->file_prefix = (char *) malloc (sizeof(char)*20);                   
    err +=  fscanf(fp,"%[^\n]",VTKgrid->file_prefix);                                   
    err +=  fscanf(fp,"%lf\n",&VTKgrid->dt);                                            
    err +=  fscanf(fp,"%d \n",&VTKgrid->datatype);                                      
    err +=  fscanf(fp,"%d \n",&VTKgrid->gridx);                                         
    err +=  fscanf(fp,"%d \n",&VTKgrid->gridy);                                         
    err +=  fscanf(fp,"%d \n",&VTKgrid->gridz);                                         
    VTKgrid->c_time = 0.0;                                                      
    printf("Dt of vtk grid %lf\n",VTKgrid->dt);                                            
  }                                                                             
  /*read_particles*/                                                            
  err +=  fscanf(fp,"%d \n",&N);
printf("Number of particles read  %d",N);
  particleData *pdr;                                                          
  pdr = malloc(N*sizeof(particleData ));                                      
  /* pos vel phase pressure rho mass h */                                       
  for(i=0;i<N;i++){                                                             
    //fscanf(fp,"%4.10f %4.10f %4.10f %4.10f %4.10f %4.10f %d %4.10f %lf %lf %4.10f %d\n",&pdr[i].pos.x,&pdr[i].pos.y,&pdr[i].pos.z,&pdr[i].vel.x,&pdr[i].vel.y,&pdr[i].vel.z, &pdr[i].phase, &pdr[i].pressure, &pdr[i].rho, &pdr[i].mass, &pdr[i].h, &pdr[i].freesurface);
    err +=  fscanf(fp,"%lf %lf %lf %lf %lf %lf %d %lf %lf %lf %lf %u\n",&pdr[i].pos.x,&pdr[i].pos.y,&pdr[i].pos.z,&pdr[i].vel.x,&pdr[i].vel.y,&pdr[i].vel.z, &pdr[i].phase, &pdr[i].pressure, &pdr[i].rho, &pdr[i].mass, &pdr[i].h, &pdr[i].freesurface);
  }            

  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));    
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));    
  if(dim ==2){                                                                  
    cells.dim.z = 1;                                                            
  } else if (dim ==3) {                                                         
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));  
  }                                                                             
if(err >0){
  printf("Error reading restart file \n");
//  exit(1);
}
printf("Cell dimensions  %d %d %d",cells.dim.x, cells.dim.y, cells.dim.z);
  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/                 

  allocate_cells();    

  printf("Read %d particles from the restart file at sim time %lf \n",N, sim_time);
  return pdr;                                                                 
}              
