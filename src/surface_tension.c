#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include "forces.h"
#include "kernel.h"
#include "particles.h"

static void threepoint_circle(particleData * pdata ,int , int , int, vector * );
static vector local_transform(vector pos, double alpha);
//static double  dot_product(vector a, vector  b);
//static transform(vector * basis_l, vector pos);
static double lagrange( double x,  double* xn,int n, int i) ;
static double Derivative_lagrange(double x, double* xn, int n, int i)  ;
static double Second_Derivative_lagrange(double x, double* xn, int n, int i);



void calculate_n_kab(particleData * pdata, int a, int b, interactParams params, boolean ghost)
{
  int i;
/*  for(i = 0;i < dim;i++){                                                       


    (&pdata[a].n.x)[i] += pdata[a].mass*(&params.dwdx.x)[i]/pdata[b].rho;  
    (&pdata[b].n.x)[i] += -pdata[a].mass*(&params.dwdx.x)[i]/pdata[a].rho;    
  }*/
   double rdotdwdx = 0.0;                                                        
   for(i=0;i<dim;i++){                                                           
     rdotdwdx += (&params.xr.x)[i]*(&params.dwdx.x)[i];                          
   }    
 
  double rdotr = params.rdotr;
 double h = params.h; 
  double Fab = rdotdwdx/(rdotr + 0.0001*h*h);                                 
  double Kab,Kba;                                                             
  Kab = (pdata[b].mass)*(4.0/(pdata[a].rho + pdata[b].rho))*Fab; 
  Kba = (pdata[a].mass)*(4.0/(pdata[a].rho + pdata[b].rho))*Fab; 
  if(a != b) {                                                                
    pdata[a].kab += Kab;                                                      
    pdata[b].kab += Kba;                                                      
  }              

return;
}
void calculate_normal(particleData * pdata, int a, int b, interactParams params, boolean ghost)
{
  int i;

 // double cr =  pdata[b].color- pdata[a].color;
  double cr =  pdata[a].color + pdata[b].color;

  for(i = 0;i < dim;i++){                                                       


    (&pdata[a].n.x)[i] += pdata[b].mass*cr*(&params.dwdx.x)[i]/pdata[b].rho;  
    (&pdata[b].n.x)[i] -= pdata[a].mass*cr*(&params.dwdx.x)[i]/pdata[a].rho;    
    (&pdata[a].n_0.x)[i] -= pdata[b].mass*pdata[a].color*(&params.dwdx.x)[i]/pdata[b].rho;  
    (&pdata[b].n_0.x)[i] += pdata[a].mass*pdata[b].color*(&params.dwdx.x)[i]/pdata[a].rho;    
  }

return;
}
void calculate_surf_kappa(particleData * pdata, int a, int b, interactParams params, boolean ghost)
{
  int i;

  double rdotdwdx=0.;                                                           
  double rdotr =  params.rdotr;                                                 
  double h = params.h;                                                          
  double q;   

  double epsilon = 0.0001*h * h;
  //  double modna=0.0, modnb=0.0;
  vector nacap,nbcap,nr;
  vector dwdx = params.dwdx;
  //vector nr;
  double ndotdwdx;

  for(i=0; i<dim; i++){
    (&nr.x)[i] = (&pdata[a].n.x)[i] - (&pdata[b].n.x)[i];
    rdotdwdx += (&params.xr.x)[i]*(&params.dwdx.x)[i]; 
    ndotdwdx += (&nr.x)[i]*(&params.dwdx.x)[i]; 
  }
  double Fab = rdotdwdx/(params.rdotr+epsilon);

  if(ghost==false  ){
      pdata[a].s_kappa += -(pdata[a].mass/pdata[a].rho)*ndotdwdx;
      pdata[b].s_kappa += -(pdata[a].mass/pdata[a].rho)*ndotdwdx;
  }

  /*
  
  
  
  if(ghost==false  ){
    //    if(pdata[a].wsum < 0.975)
//    if(pdata[a].modC + pdata[b].modC > DELTA){
      //    pdata[a].s_kappa += pdata[b].color*(pdata[b].mass/pdata[b].rho)*(4.0/(pdata[a].modC +pdata[b].modC))*Fab;
      //  pdata[b].s_kappa += pdata[a].color*(pdata[a].mass/pdata[a].rho)*(4.0/(pdata[a].modC +pdata[b].modC))*Fab;
      pdata[a].s_kappa += (pdata[a].color - pdata[b].color)*(pdata[b].mass/pdata[b].rho)*(2.0)*Fab/pdata[a].modC;
      //  if(pdata[b].wsum < 0.975)
      pdata[b].s_kappa += (pdata[b].color - pdata[a].color)*(pdata[a].mass/pdata[a].rho)*(2.0)*Fab/pdata[b].modC;
  //  }
  }
*/
  return;
}

void pad_surf_kappa(particleData *pdata )
{
  int c;
  int i;
  for(i=0;i<N;i++){
    //  vector n =  pdata[i].n;
    //  double modn = sqrt( n.x*n.x + n.y*n.y + n.z*n.z);
    if(pdata[i].modC < DELTA){
      pdata[i].s_kappa = 0.0;
      //pdata[i].s_kappa =  (-kappa*pdata[i].color * pdata[i].rho + pdata[i].s_kappa)/pdata[i].modC;
   // }else {
     // pdata[i].s_kappa = 0.0;
    }
  } 
  return;
}


/*
void calculate_surf_kappa(particleData * pdata, int a, int b, interactParams params, boolean ghost)
{
  int i;


  double modna=0.0, modnb=0.0;
  vector nacap,nbcap,nr;
  vector dwdx = params.dwdx;

  double epsilon = 0.01*particle_dx * h_global;

  for(i=0;i<dim;i++){
    modna += (&pdata[a].n.x)[i]*(&pdata[a].n.x)[i];
    modnb += (&pdata[b].n.x)[i]*(&pdata[b].n.x)[i];
  }

  for(i = 0;i<dim;i++){                                                         
    if ((modna)>epsilon){                                                         
      (&nacap.x)[i] = (&pdata[a].n.x)[i]/sqrt(modna);                                
    }                                                                           
    else (&nacap.x)[i] = 0.;                                                    
    if ((modnb) >epsilon){                                                        
      (&nbcap.x)[i] = (&pdata[b].n.x)[i]/sqrt(modnb);                                
    }                                                                           
    else (&nbcap.x)[i]= 0.;                                                     

    (&nr.x)[i] = (&nbcap.x)[i] - (&nacap.x)[i];                                 
  }                                                                             
                                                                      
//if(modna>epsilon && modnb>epsilon){  
  for(i = 0;i<dim;i++){                                                         
    pdata[a].s_kappa += pdata[b].mass*(&nr.x)[i]*(&dwdx.x)[i]/pdata[b].rho;
    pdata[b].s_kappa += pdata[a].mass*(&nr.x)[i]*(&dwdx.x)[i]/pdata[a].rho;                      
  }       
//}
return;
}
*/
void find_interface_neighbors(particleData * pdata, int a, int b, interactParams params, boolean ghost)
{
  int c;
  if(a!=b){
    for(c=0;c<dim;c++){
      (&pdata[a].nearest.x)[c] += (&pdata[b].pos.x)[c];
      (&pdata[b].nearest.x)[c] += (&pdata[a].pos.x)[c];
    }
  }
  return;
}
void surface_tension(particleData *pdata)
{

  int c;

  int i;
  for(i=0;i<N;i++){
    vector n =  pdata[i].n;

//    double modn = sqrt( n.x*n.x + n.y*n.y + n.z*n.z);
    if(pdata[i].interface_tag==1){
      for(c = 0;c < dim;c++)
      {   
        (&pdata[i].acc.x)[c] += surf_sigma*pdata[i].s_kappa*(&pdata[i].n.x)[c]/(pdata[i].rho* particle_dx);
        //(&pdata[i].acc.x)[c] += surf_sigma*pdata[i].s_kappa*(&n.x)[c]/pdata[i].rho;
      
      } 
    }
  }
  return;
} 
void interface_arrange(particleData * pdata)
{
  int i,j,c;
  double rdotr;
  vector /*pos_i_t,*/ pos_j_t;
  vector xr;
  for(i=0;i<N;i++){
    if(pdata[i].interface_tag == 1){
      for(c = 0;c < dim;c++)
        (&pdata[i].nearest.x)[c] = (&pdata[i].nearest.x)[c]/pdata[i].num_neighbors;
      vector r_axes;
      for(c=0;c<dim;c++)
        (&r_axes.x)[c] = (&pdata[i].pos.x)[c] - (&pdata[i].nearest.x)[c];
      double alpha;
      if(r_axes.x > 0.0)
        alpha = 2.0*PI-acos(r_axes.y/sqrt(r_axes.x*r_axes.x + r_axes.y*r_axes.y));
      else
        alpha = acos(r_axes.y/sqrt(r_axes.x*r_axes.x + r_axes.y*r_axes.y));
     // vector pos_t;
      //pos_i_t = local_transform(pdata[i].pos,alpha);
      double distance1 =100.0, distance2=100.0;
      for(j=0;j<N;j++){
        if(j!=i && pdata[j].interface_tag == 1){
          rdotr = 0.0;
          for(c = 0; c<dim;c++){                                                  
            (&xr.x)[c] = (&pdata[i].pos.x)[c] - (&pdata[j].pos.x)[c];
            rdotr += (&xr.x)[c]*(&xr.x)[c];                  
          }
          if(sqrt(rdotr) < 2.0*pdata[i].h*r_cutoff){
            pos_j_t = local_transform(pdata[j].pos,alpha);
            if(pos_j_t.x>0.0){
              if(pos_j_t.x<distance1){
                distance1 = pos_j_t.x;
                pdata[i].nearest_id = j;
              }
            }
            if(pos_j_t.x<=0.0){
              if((-pos_j_t.x)<distance2){
                distance2 = -pos_j_t.x;
                pdata[i].nearest_id2 = j;
              }
            }
          }
        }
      }
      //
     // printf("two neighbors of %d are  %d \t %d \n",i, pdata[i].nearest_id, pdata[i].nearest_id2);
    }
  }

/*

  for(i=0;i<N;i++){
    if(pdata[i].interface_tag == 1){
      pdata[i].nearest = 100.0;
      pdata[i].nearest_id = -1;
      for(j=0;j<N;j++){
        if(pdata[j].interface_tag ==1 && i!=j) {
          rdotr = 0.0;
          for(c = 0; c<dim;c++){                                                  
            (&xr.x)[c] = (&pdata[i].pos.x)[c] - (&pdata[j].pos.x)[c];
            rdotr += (&xr.x)[c]*(&xr.x)[c];                  
          }
          if(sqrt(rdotr) < pdata[i].nearest){

            pdata[i].nearest = sqrt(rdotr);
            pdata[i].nearest_id = j;
          }
        }
      }
      pdata[i].nearest = 100.0;
      for(j=0;j<N;j++){
        if(pdata[j].interface_tag ==1 && i!=j && j != pdata[i].nearest_id){
          rdotr = 0.0;
          for(c = 0; c<dim;c++){                                                  
            (&xr.x)[c] = (&pdata[i].pos.x)[c] - (&pdata[j].pos.x)[c];
            rdotr += (&xr.x)[c]*(&xr.x)[c];                  
          }
          if(sqrt(rdotr) < pdata[i].nearest){

            pdata[i].nearest = sqrt(rdotr);
            pdata[i].nearest_id2 = j;
          }
        }
      }
    }
  }

  for(i=0;i<N;i++){
    if(pdata[i].interface_tag == 1){
      printf("two neighbors of %d are  %d \t %d \n",i, pdata[i].nearest_id, pdata[i].nearest_id2);
    }
  }
  */
}

void calculate_threepoint_kappa(particleData * pdata)
{
  int i,c;
  for(i=0;i<N;i++){

    if(pdata[i].interface_tag==1){
      vector circle_center, normal;
      threepoint_circle(pdata , i , pdata[i].nearest_id, pdata[i].nearest_id2, &circle_center);

      if(circle_center.x != 1000.0){
        pdata[i].s_kappa = 1.0/sqrt(pow((pdata[i].pos.x - circle_center.x),2.0) + pow((pdata[i].pos.y -circle_center.y),2.0) );
        double mag=0.0;
        for(c=0;c<dim;c++){
          (&normal.x)[c] = (&circle_center.x)[c] - (&pdata[i].pos.x)[c];
          mag+= (&normal.x)[c] *(&normal.x)[c]; 
        }
        for(c=0;c<dim;c++){
          (&pdata[i].n.x)[c] = (&normal.x)[c]/sqrt(mag);
        }
      }else{
        pdata[i].s_kappa = 0.0;

      }
      for(c = 0;c < dim;c++)                                                   
      {                                                                        
        (&pdata[i].acc.x)[c] += surf_sigma*pdata[i].s_kappa*(&pdata[i].n.x)[c]/(pdata[i].mass  /* particle_dx*/);
      }       
      


    }
  }

}

static void threepoint_circle(particleData * pdata, int id_2, int id_1, int id_3, vector * circle)
{

  double yDelta_a =  pdata[id_2].pos.y-pdata[id_1].pos.y;                                   
  double xDelta_a =  pdata[id_2].pos.x-pdata[id_1].pos.x;           
  double yDelta_b =  pdata[id_3].pos.y-pdata[id_2].pos.y;
  double xDelta_b =  pdata[id_3].pos.x-pdata[id_2].pos.x; 
  double slope_a ; 
  double slope_b ; 
  if(fabs(xDelta_a) != 0.0){
  slope_a=   yDelta_a/xDelta_a;
  }  else{
    slope_a = 999999999;
  }
  if(fabs(xDelta_b) != 0.0){
  slope_b=  yDelta_b/xDelta_b;
  } else {
    slope_b = 999999999;
  }


  if(slope_a !=  slope_b){
    circle->x = (slope_a*slope_b*(pdata[id_1].pos.y-pdata[id_3].pos.y) + slope_b*(pdata[id_1].pos.x+pdata[id_2].pos.x) - slope_a*(pdata[id_2].pos.x+pdata[id_3].pos.x))/(2.*(slope_b-slope_a));
    if(slope_a!= 0.0){
    circle->y = -1.*(circle->x - (pdata[id_1].pos.x+pdata[id_2].pos.x)/2.)/slope_a + (pdata[id_1].pos.y+pdata[id_2].pos.y)/2.0;                              
    }else{
    circle->y = -1.*(circle->x - (pdata[id_1].pos.x+pdata[id_2].pos.x)/2.)/DELTA + (pdata[id_1].pos.y+pdata[id_2].pos.y)/2.0;                              

    }
  }else{
    circle->x = 1000.0; circle->y=0.0; 
  }
//  printf("centre of circle is %.3lf %.3lf,\n ",circle->x, circle->y);

  circle->z =0.0;

  return ; 
}

static vector local_transform(vector pos, double alpha)
{

  vector pos_t;
  pos_t.x = pos.x * cos(alpha) + pos.y*sin(alpha);
  pos_t.y = -pos.x * sin(alpha) + pos.y*cos(alpha);

  return pos_t;

}

void calculate_padding_kappa(particleData * pdata)
{
  //compute the gradient of color function
  // \nabla C . Find | \nabla C| . use 
  // this value instead of \rho in the pressure
  // equation approximation. 
  // Assumption: \nabla C does not vary along the interface.



}

void calculate_MLS_kappa(particleData * pdata)
{
  double cutoff = 2.25*particle_dx;
  int i,j,c,d;
  vector xr;
  double r;
  int neighb_max = 0;
  int nn;
  for(i=0;i<N;i++){
    nn=0;
    for(j=0;j<N;j++){
      if(pdata[i].interface_tag==1 && pdata[j].interface_tag==1){
        for(c=0;c<dim;c++){
          (&pdata[i].nearest.x)[c] = 0.0;
        }
        xr.x = pdata[i].pos.x - pdata[j].pos.x;
        xr.y = pdata[i].pos.y - pdata[j].pos.y;
        xr.z = pdata[i].pos.z - pdata[j].pos.z;
        r = sqrt(xr.x*xr.x + xr.y*xr.y +xr.z*xr.z);
        if(r<=cutoff){
          nn++ ;
        }
      }
    }
    if(nn>neighb_max)
      neighb_max = nn;
  }
  int k;//k for part, l for polyn degrees
  gsl_matrix * P = gsl_matrix_alloc(neighb_max,6);
  gsl_matrix * A = gsl_matrix_alloc(6,6);
  gsl_matrix * trans = gsl_matrix_alloc(3,3);
  gsl_matrix * invtrans = gsl_matrix_alloc(3,3);
  gsl_vector * z_ = gsl_vector_alloc(neighb_max);
  gsl_vector * b_ = gsl_vector_alloc(6);
  gsl_vector * a_ = gsl_vector_alloc(6);
  gsl_vector * n = gsl_vector_alloc(3);
  gsl_vector * t_n = gsl_vector_alloc(3);

  printf("Initiated surf . Max neighb =  %d\n", neighb_max);
  vector pos_i, pos_j, pos_j_local;
  tensor loc; // unit vectors of local coordinates 
  tensor ref; // unit vectors of orig coordinates 
  double mag = 0.0;
  for(i=0;i<dim;i++){
    for(c=0;c<dim;c++){
      (&(&ref.xx)[i].x)[c] = (i==c)? 1.0:0.0; 
      (&(&loc.xx)[i].x)[c] = 0.0; 
    }
  }

  for( i = 0; i<N;i++){ // particle loop
    if(pdata[i].interface_tag==1 ){ 
      // unit vectors
      pos_i = pdata[i].pos;
      for(c=0;c<dim;c++){
        (&loc.zz.x)[c] = (&pos_i.x)[c] - (&pdata[i].nearest.x)[c]; 
      }
      normalize(&loc.zz);
      cross_product( ref.yy , loc.zz, &loc.xx, dim);
      if(fabs(loc.xx.x) < DELTA && fabs(loc.xx.y) < DELTA && fabs(loc.xx.z) < DELTA )
        cross_product( ref.xx , loc.zz, &loc.xx, dim);

      normalize(&loc.xx);
      cross_product( loc.zz , loc.xx, &loc.yy, dim);
      normalize(&loc.yy);
      // tensor loc is the basis for the local coordinate system
      // Initialize P and z with values of i
      vector pos_t_i =  transform(loc, pdata[i].pos);
      for(k=0;k<neighb_max;k++){
        gsl_matrix_set (P, k, 0, 1.0);
        gsl_matrix_set (P, k, 1, pos_t_i.x);
        gsl_matrix_set (P, k, 2, pos_t_i.y);
        gsl_matrix_set (P, k, 3, pos_t_i.x*pos_t_i.x);
        gsl_matrix_set (P, k, 4, pos_t_i.x*pos_t_i.y);
        gsl_matrix_set (P, k, 5, pos_t_i.y*pos_t_i.y);
        gsl_vector_set (z_, k, pos_t_i.z);
      }
      if(i==0)
        printf("Redundance =  \n");
      k=0 ;
      for(j = 0  ;  j<N; j++){
        if(pdata[j].interface_tag==1){
          //Rotate the coordinates , for spherical
          //End rotation
          xr.x = pdata[i].pos.x - pdata[j].pos.x;
          xr.y = pdata[i].pos.y - pdata[j].pos.y;
          xr.z = pdata[i].pos.z - pdata[j].pos.z;
          r = sqrt(xr.x*xr.x + xr.y*xr.y+xr.z*xr.z);
          if(r<=cutoff){
            vector pos_t_j =  transform(loc, pdata[j].pos);
            // make P and find Ainv B;
            gsl_matrix_set (P, k, 0, 1.0);
            gsl_matrix_set (P, k, 1, pos_t_j.x);
            gsl_matrix_set (P, k, 2, pos_t_j.y);
            gsl_matrix_set (P, k, 3, pos_t_j.x*pos_t_j.x);
            gsl_matrix_set (P, k, 4, pos_t_j.x*pos_t_j.y);
            gsl_matrix_set (P, k, 5, pos_t_j.y*pos_t_j.y);
            gsl_vector_set (z_, k, pos_t_j.z);
            k++;
          }
        }
      }
      gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, P, P, 0.0, A); // A =  P'P
      gsl_blas_dgemv(CblasTrans, 1.0, P, z_, 0.0, b_); // A =  P'P 
      int s;
      gsl_permutation * p = gsl_permutation_alloc(6);
      gsl_linalg_LU_decomp (A, p, &s);
      gsl_linalg_LU_solve (A, p, b_, a_);
      if(i == 40){
        printf ("coeff = \n");
        gsl_vector_fprintf (stdout, a_, "%g");
      }

      double fx, fy, fxx, fyy, fxy;
      fx  =  gsl_vector_get(a_,1) + 2.0*gsl_vector_get(a_,3)*pos_t_i.x + gsl_vector_get(a_,4)*pos_t_i.y;
      fy  =  gsl_vector_get(a_,2) +  gsl_vector_get(a_,4)*pos_t_i.x + 2.0*gsl_vector_get(a_,5)*pos_t_i.y;
      fxx =  2.0*gsl_vector_get(a_,3) ;
      fxy =  gsl_vector_get(a_,4);
      fyy =  2.0*gsl_vector_get(a_,5);

      pdata[i].s_kappa = ((1.0+fx*fx)*fyy - 2.0*fx*fy*fxy + (1.0+fy*fy)*fxx)/(2.0 *pow((1.0 + fx*fx + fy*fy),1.5));

      gsl_vector_set (t_n, 0, -fx);
      gsl_vector_set (t_n, 1, -fy);
      gsl_vector_set (t_n, 2, 1.0);
      for(c=0;c<3;c++)
        for(d=0;d<3;d++)
          gsl_matrix_set(trans, c, d, (&(&loc.xx)[c].x)[d]);
      gsl_permutation * ptrans = gsl_permutation_alloc(3);
      int t;
      gsl_linalg_LU_decomp (trans, ptrans, &t);
      gsl_linalg_LU_solve (trans, ptrans, t_n, n);
      pdata[i].n.x = gsl_vector_get(n,0);
      pdata[i].n.y = gsl_vector_get(n,1);
      pdata[i].n.z = gsl_vector_get(n,2);
      normalize(&pdata[i].n);
    }
  }
}
void interface_detect(particleData * pdata)
{
  int i,j,k,c;
  vector xr_j, xr_k;
  double cutoff = 2.25*particle_dx;
  double dot;
  for (i=0 ;i <N;i++){
    if(pdata[i].wsum <0.9){
      int  fs = 0 ;
      for (j=0 ;j<N;j++){
        for(c=0;c<dim;c++)
          (&xr_j.x)[c] = (&pdata[i].pos.x)[c] - (&pdata[j].pos.x)[c];
        if(magnitude(&xr_j) <= cutoff && i!=j){

          normalize(&xr_j); // reflected j
          for (k=0 ;k<N;k++){
            for(c=0;c<dim;c++)
              (&xr_k.x)[c] = (&pdata[k].pos.x)[c] - (&pdata[i].pos.x)[c];

            if(magnitude(&xr_k) <= cutoff && (j!=k && k!=i) /*&& pdata[k].wsum<0.9*/){
              normalize(&xr_k);

              dot = dot_product(xr_j, xr_k);
              if(dot >= 0.50){ // if near // half cone angle 60 degrees
                fs = 0;
                k=N; break; // go to next j
              } else
                fs = 1; // no neighbor nnear j yet
            }
          }
          // check fs
          if(fs ==1){ // means one j has no neighbor
            pdata[i].interface_tag = 1;
            j = N;
            break; // go to next i
          }
        }
        /* if(fs==1)
           break;*/

      }
    }
  }
  return;
}
void calculate_lagrange_kappa(particleData * pdata)
{
  int i,c,j;
  for(i=0;i<N;i++){
    if(pdata[i].interface_tag==1){
      int num_neighb = pdata[i].num_interface_neighbors;
      double xx[num_neighb], yy[num_neighb], s_kappa=0.0,slope=0.0;
      int id_j; vector pos_j,pos_i;
      vector normal;
      for(j=0;j<num_neighb;j++){
        id_j = pdata[i].near_id[j];
        pos_j = local_transform(pdata[id_j].pos ,pdata[i].alpha)  ;
        xx[j] = pos_j.x;
        yy[j] = pos_j.y;
        if(i==id_j)
          pos_i = local_transform(pdata[id_j].pos, pdata[i].alpha);
      }   
      for(j=0;j<num_neighb;j++){
        s_kappa += yy[j]*Second_Derivative_lagrange(pos_i.x,xx,num_neighb,j);
      } 
      pdata[i].s_kappa = -s_kappa;
      for(j=0;j<num_neighb;j++){
        slope += yy[j]*Derivative_lagrange(pos_i.x,xx,num_neighb,j);
      } 
        normal.x = slope*cos(pdata[i].alpha)+ sin(pdata[i].alpha);
        normal.y = slope*sin(pdata[i].alpha)- cos(pdata[i].alpha);
      double mag= 0.0;
      for(c=0;c<dim;c++)
        mag += (&normal.x)[c]*(&normal.x)[c];
      mag = sqrt(mag);
      for(c=0;c<dim;c++)
        (&normal.x)[c] = (&normal.x)[c]/mag;
      for(c = 0;c < dim;c++)
      {                     
        (&pdata[i].acc.x)[c] += surf_sigma*pdata[i].s_kappa*(&normal.x)[c]/*sqrt(pdata[i]    .n.x*pdata[i].n.x + pdata[i].n.y*pdata[i].n.y*/ / (particle_dx* pdata[i].rho);
      }       
    } 
  } 
}
static double Derivative_lagrange(double x, double* xn, int n, int i) {                        int index = -1;
   double sum = 0.0;
   double product = 1.0;
   int j;               
   for( j=0;j<n ;j++) {
     if(j != i) {      
       if( fabs(x-xn[j]) < DELTA){
         index = j;               
       }           
     }  
   }  
   if(index == -1) {
     for( j=0;j<n;j++) {
       if(j != i) {     
         product = product*(x-xn[j])/(xn[i]-xn[j]);
         sum = sum + 1.0/(x-xn[j]);                
       }                                           
     }                             
     product = product*sum;
   }                       
   else {                  
     for(j=0;j<n;j++) {
       if((j != i) && (j != index)) {
         product = product*(x-xn[j])/(xn[i]-xn[j]);
       }                                           
     }                                             
     product = product/(xn[i]-xn[index]);
   }                                     
   return product;                       
 } 
 static double Second_Derivative_lagrange(double x, double* xn, int n, int i) {                
  int index = -1;
  double square_of_sum, sum_of_squares,sum;
  double product = 1.0;                    
  int j;                                   
  for( j=0;j<n ;j++) { 
    if(j != i) {      
      if( fabs(x-xn[j]) < DELTA){
        index = j;               
      }                          
    }             
  }    
  
  if(index == -1) {
    square_of_sum = 0.0; sum_of_squares = 0.0;
    for( j = 0;j < n;j++) {                   
      if(j != i) {                            
        square_of_sum = square_of_sum + 1.0/(x - xn[j]);
        sum_of_squares = sum_of_squares + 1.0/((x - xn[j])*(x - xn[j])) ;
      }
    }
    square_of_sum = square_of_sum*square_of_sum;
    product = lagrange(x,xn,n,i)*(square_of_sum - sum_of_squares);
  }
  else {
    sum = 0.0;
    for( j=0;j<n;j++) {
      if((j != i) && (j != index)) {
        product = product*(x-xn[j])/(xn[i]-xn[j]);
        sum = sum + 1.0/(x - xn[j]);
      }
    }
    product = 2.0*product*sum/(xn[i]-xn[index]);
  }
  return product;
}
static double lagrange( double x,  double* xn,int n, int i) {                                
  double product = 1.0;
  int j;
  for( j=0;j<n;j++) {
    if(i==j)
      continue;
    else
      product = product*(x-xn[j])/(xn[i]-xn[j]);

  }
  return product;
}




