#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "sph.h"
#include "particles.h"
#include "kernel.h"
#include "boundary.h"
#include "forces.h"
#include "isph.h"

void divergence_velocity( particleData * p_a, particleData * p_b, interactParams params, boolean ghost )
{
//printf( "came here \n");
//exit(1);
  double dotprod;

  int i;
  vector vr;
//    for(i = 0; i < dim; i++){
//      (&vr.x)[i] = (&p_a->vel.x)[i] - (&p_b->vel.x)[i];
//      }
  vector dwdx = params.dwdx;
//  vector xr = params.xr;
   vr = params.vr;
  vector uab ;

  if(divergence ==0){
      for(i = 0; i < dim; i++)
        (&uab.x)[i] = (&p_a->vel.x)[i]/(p_a->rho*p_a->rho) +(&p_b->vel.x)[i]/(p_b->rho*p_b->rho) ;
      dotprod =0.0;
      for(i = 0; i < dim; i++)
        dotprod += (&uab.x)[i]*(&dwdx.x)[i];

      p_a->u += p_a->rho*p_b->mass*dotprod;
//      p_b->u -= p_b->rho*p_a->mass*dotprod;
  }
  else if(divergence == 1){
    double vdotdwdx = 0.;
    for(i = 0; i < dim; i++){
      vdotdwdx += (&vr.x)[i]*(&dwdx.x)[i];
    }

    p_a->u += -p_b->mass*vdotdwdx/p_a->rho;
  //  p_b->u += -p_a->mass*vdotdwdx/p_b->rho;/*rho values exchanged*/

  }

  else if(divergence == 2){/*Szewc*/
    double vdotdwdx_a = 0.;
    double vdotdwdx_b = 0.;
    vector vb;
    //    double rdotdwdx =0.;
    for(i = 0; i < dim; i++)
      (&vb.x)[i] = (&p_a->vel.x)[i] - (&vr.x)[i];
    for(i = 0; i < dim; i++){
      vdotdwdx_b += (&vb.x)[i]*(&dwdx.x)[i];
      vdotdwdx_a += (&p_a->vel.x)[i]*(&dwdx.x)[i];
    }
      p_a->u += p_a->mass*vdotdwdx_b/p_a->rho;
    //  p_b->u += -p_b->mass*vdotdwdx_a/p_b->rho;

  }

  else if(divergence == 3){/*Hu and addams*/
    double vdotdwdx = 0.;
    //    double rdotdwdx =0.;
    for(i = 0; i < dim; i++){
      vdotdwdx += (&vr.x)[i]*(&dwdx.x)[i];
    }
    if(ghost==false){
      p_a->u += -p_a->mass*vdotdwdx/p_a->rho;
    //  p_b->u += -p_b->mass*vdotdwdx/p_b->rho;
    } else {
    /*  vector posb; 
      vector velb;
      for(i=0;i<dim;i++){
        (&posb.x)[i] = (&p_a->pos.x)[i] - (&params.xr.x)[i];
        (&velb.x)[i] = (&p_b->vel.x)[i];
      }
      if(posb.x>xhigh->x || posb.x<xlow->x){
        velb.x = - p_b->vel.x;
      } 
      if(posb.y<xlow->y || posb.y>xhigh->y){
        velb.y = - p_b->vel.y;
      }
      for(i = 0; i < dim; i++){
        vdotdwdx += ((&p_a->vel.x)[i] - (&velb.x)[i])*(&dwdx.x)[i];
      }
      */
      p_a->u += -p_a->mass*vdotdwdx/p_a->rho;
    }
  }
    //  p_a->div_pos += -p_a->mass*rdotdwdx/p_a->rho;
    //  p_b->div_pos += -p_a->mass*rdotdwdx/p_a->rho;
  else if(divergence == 4){/*Szwec et. al.*/
    for(i = 0; i < dim; i++){
      (&p_a->uxx.x)[i] += p_b->mass* (&p_b->vel.x)[i]*(&params.dwdx.x)[i]/p_b->rho;
    //  (&p_b->uxx.x)[i] +=-p_a->mass* (&p_a->vel.x)[i]*(&params.dwdx.x)[i]/p_a->rho;
    }
    //    p_a->u += (p_b->mass*vbdotdwdx/p_b->rho)/(p_a->x + pdata[a]y);
    //    p_b->u += -p_a->mass*vadotdwdx/p_a->rho/(p_b->x + p_b->y);
    //  p_a->div_pos += -p_a->mass*rdotdwdx/p_a->rho;
    //  p_b->div_pos += -p_a->mass*rdotdwdx/p_a->rho;

  }else if(divergence == 5){/* Free slip*/
    double vdotdwdx = 0.;
    double vab=0.0, vba=0.0;
    //    double rdotdwdx =0.;
      for(i = 0; i < dim; i++){
        vdotdwdx += (&vr.x)[i]*(&dwdx.x)[i];
        // vdotdwdx += ((&p_a->vel.x)[i]+ (&p_b->vel.x)[i])*(&dwdx.x)[i];
        vab += (&p_b->vel.x)[i]*(&dwdx.x)[i];
        vba += (&p_a->vel.x)[i]*(&dwdx.x)[i];
      }
       p_a->u += p_a->mass*vab/p_a->rho;
      //   p_b->u += -p_b->mass*vba/p_b->rho;
    /*  p_a->u += -p_a->mass*vdotdwdx/p_a->rho;
      p_b->u += -p_b->mass*vdotdwdx/p_b->rho;*/
    //  p_a->div_pos += -p_a->mass*rdotdwdx/p_a->rho;
    //  p_b->div_pos += -p_a->mass*rdotdwdx/p_a->rho;

  }

  else if(divergence == 6){/*Hu and addams*/
    double vdotdwdx = 0.;
    //    double rdotdwdx =0.;
      for(i = 0; i < dim; i++){
        vdotdwdx += ((&p_a->vel.x)[i] + (&p_b->vel.x)[i])*(&dwdx.x)[i];
      }
      p_a->u += p_a->mass*vdotdwdx/p_a->rho;
    //  p_b->u += -p_b->mass*vdotdwdx/p_b->rho;
    //  p_a->div_pos += -p_a->mass*rdotdwdx/p_a->rho;
    //  p_b->div_pos += -p_a->mass*rdotdwdx/p_a->rho;

  }
  else if(divergence == 7){/* Free slip*/
    double vdotdwdx = 0.;
    double vab=0.0, vba=0.0;
    //    double rdotdwdx =0.;
      for(i = 0; i < dim; i++){
        vdotdwdx += (&vr.x)[i]*(&dwdx.x)[i];
        vab += (&p_b->vel.x)[i]*(&dwdx.x)[i];
        vba += (&p_a->vel.x)[i]*(&dwdx.x)[i];
      }
     //  p_a->u += p_a->mass*vab/p_a->rho;
     //    p_b->u += -p_b->mass*vba/p_b->rho;
      p_a->u += -p_a->mass*vdotdwdx/p_a->rho;
    //  p_b->u += -p_b->mass*vdotdwdx/p_b->rho;
    //  p_a->div_pos += -p_a->mass*rdotdwdx/p_a->rho;
    //  p_b->div_pos += -p_a->mass*rdotdwdx/p_a->rho;

  }
  else if(divergence == 8){/* No slip*/
    double vdotdwdx = 0.;
    //double vab=0.0, vba=0.0;
    //    double rdotdwdx =0.;
      for(i = 0; i < dim; i++){
        vdotdwdx += ((&p_a->vel.x)[i] + (&p_b->vel.x)[i])*(&dwdx.x)[i];
      }
     //  p_a->u += p_a->mass*vab/p_a->rho;
     //    p_b->u += -p_b->mass*vba/p_b->rho;
      p_a->u += p_a->mass*vdotdwdx/p_a->rho;
 //     p_b->u += -p_b->mass*vdotdwdx/p_b->rho;
    //  p_a->div_pos += -p_a->mass*rdotdwdx/p_a->rho;
    //  p_b->div_pos += -p_a->mass*rdotdwdx/p_a->rho;

  }

  else{
    printf("\n Divergence Scheme not defined \n");
  }


}

void deform_grad(particleData *pdata, int a, int b, interactParams params,boolean ghost )
{
  vector dwdx = params.dwdx; 
  vector sr = params.sr; 

  int i, j;

  double term;

  for(i=0;i<dim;i++){
    for(j=0;j<dim;j++){
 //     term =  (&pdata[b].pos_s.x)[j] -  (&pdata[a].pos_s.x)[j] ;                          
      term =  -(&sr.x)[j] ;                          
     
     if(ghost==false){
      (&(&pdata[a].def_grad_n.xx)[i].x)[j] += pdata[b].mass*term*(&dwdx.x)[i]/pdata[b].rho;    
      (&(&pdata[b].def_grad_n.xx)[i].x)[j] += pdata[a].mass*term*(&dwdx.x)[i]/pdata[a].rho; 
     }else{
      (&(&pdata[a].def_grad_n.xx)[i].x)[j] += pdata[b].mass*term*(&dwdx.x)[i]/pdata[b].rho;    

     }
    }
  }


}

void compute_determinant_F(particleData *pdata, double dt)
{
  int i;
  for(i=0;i<N;i++){
    if(dim==2){
      pdata[i].det_F = (pdata[i].def_grad_n.xx.x + 1.0)*(pdata[i].def_grad_n.yy.y+1.0)
        - pdata[i].def_grad_n.xx.y*pdata[i].def_grad_n.yy.x;
    } else if(dim == 3){
      printf("\n Determinant not defined for 3 dimensions \n");
      exit(1);
    }
    if(fabs(pdata[i].det_F) <= DELTA)
    {
      printf("\n det F going to zero \n");
    }
    pdata[i].u =  (pdata[i].det_F - 1.0)/(pdata[i].det_F *  dt);
  }
}

