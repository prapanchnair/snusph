#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "sph.h"
//#include "particles.h"
#include "forces.h"

double magnitude(vector * a){
  double mag = 0.0 ;
  if(dim == 2)
    mag = sqrt(a->x*a->x + a->y*a->y);
  else if(dim ==3)
    mag = sqrt(a->x*a->x + a->y*a->y + a->z*a->z);
  return mag; 
}
void normalize(vector * a){
  int c;
  double mag = magnitude(a);
  for(c =0;c<dim;c++)
     (&a->x)[c] = (&a->x)[c]/ mag;

}

void cross_product(vector a, vector b, vector *c , int dim)
{
  c->x  = a.y*b.z - a.z*b.y;
  c->y  = a.z*b.x - a.x*b.z;
  c->z  = a.x*b.y - a.y*b.x;
/*  if(dim ==2){
    c->x =0.0;
    c->y = 0.0;
  }*/
  return;
}
double  dot_product(vector a, vector  b){
  int c;
  double dot=0.0;
  for(c=0;c<dim;c++)
    dot += (&a.x)[c] * (&b.x)[c];
  return dot;
}
vector difference(vector a, vector b){                                          
  int i;                                                                        
  vector c;                                                                     
  for(i=0;i<3;i++)                                                              
    (&c.x)[i] = (&a.x)[i] - (&b.x)[i];                                          
  return c;                                                                     
}  
vector transform(tensor basis, vector pos)
{
  vector pos_local;
  pos_local.x  = dot_product( pos, basis.xx);
  pos_local.y  = dot_product( pos, basis.yy);
  pos_local.z  = dot_product( pos, basis.zz);
  return pos_local;
}




/*static void particle_loop(particles *plist);
static void set_to_zero(particles *);
static void compute_deform_grad_sum(particles * p_me, particles *pneighbor, interact_params params);
static void compute_determinant_F(particles *plist,double dt);

void compute_deform_grad(particles *plist,double dt)
{
  set_to_zero(plist); 
  if(neighbor==particle_based)
    particle_loop(plist);

  compute_determinant_F(plist,dt);
}
static void set_to_zero(particles * plist)
{
  while(plist){
    particle *p = plist->p;
    plist = plist->next;

    int i,j;
    
    for(i=0; i<dim; i++)
      for(j=0; j<dim; j++)
        (&(&p->def_grad_n.xx)[i].x)[j] = 0.;
  }


}
static void particle_loop(particles *plist)
{
  while(plist){
    neighbor_list *temp_list = plist->neighbors;
    while(temp_list){
      interact_params params;
      params.w = temp_list->w;
      params.xr = temp_list->xr;
      params.vr = temp_list->vr;
      params.dwdx = temp_list->dwdx;

      compute_deform_grad_sum(plist, temp_list->p, params);

      temp_list = temp_list->next;
    }
    plist = plist->next;
  }
}
static void compute_deform_grad_sum(particles *p_me, particles *pneighbor, interact_params params)
{
  if(pneighbor==NULL || p_me==NULL) return;

  particle *pa = p_me->p;
  particle *pb = pneighbor->p;

  double w = params.w;
  vector dwdx = params.dwdx;
  vector xr;

  int i,j;

  for(i=0;i<dim;i++){
    (&xr.x)[i] = (&pa->pos_s.x)[i] - (&pb->pos_s.x)[i];
  }

  double term;

  for(i=0; i<dim; i++) {
    for(j=0; j<dim; j++){
      term =  (&pb->pos_s.x)[j] -  (&pa->pos_s.x)[j] ;
      (&(&pa->def_grad_n.xx)[i].x)[j] += pb->mass*term*(&dwdx.x)[i]/pb->rho;
      (&(&pb->def_grad_n.xx)[i].x)[j] += pa->mass*term*(&dwdx.x)[i]/pa->rho;



    }
  }
}

static void compute_determinant_F(particles * plist,double dt)
{

  while(plist){
    particle * p = plist->p;
    plist = plist->next;

    if(dim==2){
      p->det_F = (p->def_grad_n.xx.x+1.0)*(p->def_grad_n.yy.y+1.0) -  p->def_grad_n.xx.y*p->def_grad_n.yy.x; 
    } else if(dim==3)
    {
      printf("\n Determinant not defined for 3 dimensions \n");
      exit(1);

    }
    if (fabs(p->det_F) <= DELTA)
    {
      printf("\n det F going to zero \n");
    }
    
    p->u =  (p->det_F -1 )/(p->det_F *dt); 

  }
}

void cg_rigid(particles * plist)
{
  int i;
  double vol= 0.0;
  m_i=0.0;
  m_r =0.0;
  
  particles * temp = plist;
  while(plist){
    particle * p = plist->p;
    plist = plist->next;
    if(p->phase == 0) 
    {
      for(i=0;i<dim;i++){
        (&c_g.x)[i] += (p->mass/p->rho)*(&p->pos.x)[i];
      }
      vol         += p->mass/p->rho;

    }
  }
  for(i=0;i<dim;i++){
    (&c_g.x)[i] = (&c_g.x)[i]/vol;
  }
  
  printf("\n center of mass of rigid: %lf %lf \n", c_g.x, c_g.y);

  int helpme; 
  while(temp){

    particle * p = temp->p;
    temp = temp->next;
    if(p->phase == 0) 
    {
      double rdotr=0;

      for(i=0;i<dim;i++){
        rdotr += ((&p->pos.x)[i]- (&c_g.x)[i])*((&p->pos.x)[i]- (&c_g.x)[i]);
      }
      m_i += p->mass*rdotr;
m_r+= p->mass;
    }




  }
  printf("\n MI, mass of rigid  = %lf %lf", m_i,m_r); 

}
*/
