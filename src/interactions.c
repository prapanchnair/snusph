#include <math.h>
#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include "sph.h"
#include "particles.h"
#include "kernel.h"
#include "cell.h"
#include "run.h"
#include "forces.h"
#include "isph.h"
#include "lis.h"

static void compute_wsum(particleData * p_a, particleData * p_b, interactParams params,boolean);
static void count_neighbor_particles(particleData * p_a, particleData * p_b, interactParams params, boolean ghost);
static void pair_interact(particleData *p_a, particleData *p_b, interact_type  t_interact, interactParams params,boolean);
static vector coordinate_transform( vector pos, double theta, vector pos_o);
//static void clear_neighbors(particleData * p_a);

/** @brief Interaction makes used of the linked list of neighbors
*/
/*
void interact(particleData *pdata, interact_type * t_interact, int num_interact, double dt)
{

}
*/
static void count_neighbor_particles(particleData * p_a, particleData * p_b, interactParams params, boolean ghost)
{
  if(p_a != p_b){
    if(fabs(p_a->pos.x - p_b->pos.x)<0.5*(xhigh->x - xlow->x) && fabs(p_a->pos.y - p_b->pos.y)<0.5*(xhigh->y - xlow->y))
          p_a->num_neighbors += 1;
  }

  return;
}
static void compute_wsum(particleData * p_a, particleData * p_b, interactParams params, boolean ghost)
{
  double w=kernel(params.q,params.h);
  vector dwdx = params.dwdx;                                                    
  vector xr = params.xr;
  double rdotdwdx = 0.0;
  int i; 
  for(i = 0; i < dim; i++){                                                   
    rdotdwdx += (&xr.x)[i]*(&dwdx.x)[i];                                      
  }   
  int c;
  if(ghost != true){
    p_a->wsum += (p_a->mass/p_a->rho)*w;
 //   if(p_a!= p_b){
 //     p_b->wsum += (p_a->mass/p_a->rho)*w;
 //   }
    for(c=0;c<dim;c++){
      (&p_a->vel_xsph.x)[c] +=  (&p_b->vel.x)[c] *w * p_b->mass/p_b->rho;
   //   if(p_a!=p_b) 
     //   (&p_b->vel_xsph.x)[c] +=  (&p_a->vel.x)[c]* w * p_a->mass/p_a->rho;
    }
  }else{
    p_a->wsum += (p_b->mass/p_b->rho)*w;

  }
  /*
     pdata[a].div_pos += -pdata[b].mass*rdotdwdx/pdata[b].rho;                   
     pdata[b].div_pos += -pdata[a].mass*rdotdwdx/pdata[a].rho;     

     for(i = 0; i < dim; i++){                                                   

     (&pdata[a].gradW.x)[i] += pdata[b].mass*(&dwdx.x)[i]/pdata[b].rho;
     (&pdata[b].gradW.x)[i] += -pdata[a].mass*(&dwdx.x)[i]/pdata[a].rho;
     }
     */
  return;
}


static void pair_interact(particleData *p_a, particleData *p_b, interact_type t_interact, interactParams params,boolean ghost)
{
  int id_a, id_b;
  particleData *pdata;

  if(t_interact ==  visc){
    if(viscous) viscous_forces(p_a, p_b ,params,ghost);
  } else if(t_interact == pair_potential){
    if(potential) potential_forces(p_a, p_b ,params,ghost);
  } else if(t_interact == wsum){
    compute_wsum(p_a, p_b, params,ghost);
  } else if(t_interact == count_neighbors){
    count_neighbor_particles(p_a, p_b,params,ghost);
  } else if(t_interact == divV){
    divergence_velocity(p_a, p_b, params,ghost);
  } else if(t_interact == pressSolve){                                    
    pressure_matrix_build(p_a,p_b,params,ghost);                          
  } else if(t_interact == defGrad){
    deform_grad(pdata,id_a,id_b,params, ghost);
  } else if(t_interact == pressForce){
    pressure_forces(p_a, p_b,params,ghost);
  } else if(t_interact == surfNorm){                                    
    calculate_normal(pdata,id_a,id_b,params,ghost);                          
  } else if(t_interact == surfKappa){                                   
    calculate_surf_kappa(pdata,id_a,id_b,params,ghost);                      
  } else if(t_interact == interfaceNeighbors){                                   
    find_interface_neighbors(pdata,id_a,id_b,params,ghost);                      
  }                                  
/*
  if(t_interact == visc){
    if(id_a != id_b)
    {
      if(ghost == false){
#pragma omp atomic
        pdata[id_a].num_neighbors += 1; 
#pragma omp atomic
        pdata[id_b].num_neighbors += 1; 
      }
    }
  } */
}

void interact_isph(particleData *pdata)
{
  return;
}

void interact_freesurface_detection( particleData *pdata)
{
  printf("\n\n Detecting Free surface particles \n");
  int i,c,k;
  int id_a, id_b , id_c;
  intvector coordi,coord_neighbor;
  int neighb_id;
  int p,q,r;
  vector periodic_corr;
  for(c=0;c<dim;c++)
    (&periodic_corr.x)[c] = (&xhigh->x)[c] - (&xlow->x)[c];
  /* i is current CELL*/
//#ifdef _OPENMP
//#pragma omp parallel for default(shared) private(i,c,id_a,id_b,coordi,coord_neighbor, p,q,r,neighb_id,k)
//#endif
  for(i=0;i<(cells.dim.x*cells.dim.y*cells.dim.z);i++)
  {
    id_a = cells.partincell[i];
    coordi = id_to_coord(i);
    while(id_a >=0)
    {
      /* same cell*/
      id_b = id_a;
      while(id_b >=0)
      {
        interactParams params;
        params.rdotr = 0;
        params.h = (pdata[id_a].h + pdata[id_b].h)/2.0;
        for(c = 0; c<dim;c++){
          (&params.xr.x)[c] = (&pdata[id_a].pos.x)[c] - (&pdata[id_b].pos.x)[c];
          params.rdotr += (&params.xr.x)[c]*(&params.xr.x)[c];
        }
        params.q = sqrt(params.rdotr)/params.h;
        if(params.q<=1.0 /*r_cutoff*/ && (id_a!=id_b)){
          //-------------------------------------------- 
          // get positions of points a and b.
          vector local_r;
         // vector local_origin;
          for(c=0;c<dim;c++)
            (&local_r.x)[c] =(&pdata[id_b].pos.x)[c] - (&pdata[id_a].pos.x)[c];


          // make a the centre of the coordinate
          //that is the pos of particle a;
          // a->b is the x axis - get the theta between a->b and x axis

          double theta;
          double mag_local_r  = sqrt(local_r.x*local_r.x + local_r.y*local_r.y);
          if(local_r.y >0.0)
            theta = acos(local_r.x/ mag_local_r);
          else
            theta = 2.0*PI - acos(local_r.x/ mag_local_r);
          // use the angle to transform coordinates of point b. Which in this case will be
          // (|local_r|,0.0 )
          //coordinate_transform();
          // shift coordinates origin to pos of a
          // get the local coordinate transform of b
          // run loop of all paticles within cutoff of a
          int lower_interior=0, upper_interior=0;
          for(id_c = 0; id_c <N; id_c++){
            if((id_c!= id_a) && (id_c!=id_b)){
              vector xr_c;
              double xrdot=0.0;
              //calculate distance and check distance
              for(c = 0; c<dim;c++){
                (&xr_c.x)[c] = (&pdata[id_a].pos.x)[c] - (&pdata[id_c].pos.x)[c];
                xrdot += (&xr_c.x)[c]*(&xr_c.x)[c];
              }
              xrdot = sqrt(xrdot);
              if(xrdot/pdata[id_a].h < 3.0){ 
                vector local_c;
                local_c = coordinate_transform(pdata[id_c].pos, theta, pdata[id_a].pos);
                //lower
                if(local_c.y<0.0){
                  if(local_c.x >=0.0 && local_c.x <= mag_local_r){
                    ++lower_interior;
                  }
                } 
                if(local_c.y>=0.0){
                  //upper
                  if(local_c.x >=0.0 && local_c.x <= mag_local_r){
                    ++upper_interior;
                    //   printf("anybody \n");
                  }
                }
                // transform points to local coordinate. Check 1: if any point's x' coord is 
                // greater than zero and less than x' of b in positive y. Check 2: in negative y.
                //
                //
              }
            }
          }
          printf("lower_upper %d %d \n",lower_interior, upper_interior); 
          if(lower_interior == 0 || upper_interior == 0){
            pdata[id_a].interface_tag =1;
            pdata[id_b].interface_tag =1;
          }

        } 
        //----------------------------------------------
        id_b = cells.cellofpart[id_b];
      }
      /* neighboring cells */
      for(p=coordi.x-1;p<=coordi.x+1;p++){
        for(q=coordi.y-1;q<=coordi.y+1;q++){
          for(r=coordi.z-1;r<=coordi.z+1;r++){
            coord_neighbor.x = p;
            coord_neighbor.y = q;
            coord_neighbor.z = r;
            /* periodic...*/
            for(k=0;k<dim;k++)
            {
              if(periodic[k] ==1){
                if( (&coord_neighbor.x)[k] == (&cells.dim.x)[k] ){                  
                  (&coord_neighbor.x)[k] = 0;
                }else if((&coord_neighbor.x)[k] == -1){
                  (&coord_neighbor.x)[k] = (&cells.dim.x)[k] - 1;
                }
              }
            }
            /*...periodic*/
            neighb_id = coord_to_id(coord_neighbor);
            if(neighb_id>i)
            {
              id_b = cells.partincell[neighb_id];
              while(id_b >=0 ){
                interactParams params;
                params.rdotr = 0.0;
                params.h = (pdata[id_a].h + pdata[id_b].h)/2.0;
                for(c = 0; c<dim;c++){
                  (&params.xr.x)[c] = (&pdata[id_a].pos.x)[c]-(&pdata[id_b].pos.x)[c];
                  if(periodic[c] && fabs((&params.xr.x)[c])>0.5*fabs((&periodic_corr.x)[c])){
                    if((&params.xr.x)[c] > 0.)                                  
                      (&params.xr.x)[c] += -(&periodic_corr.x)[c];
                    else                                                        
                      (&params.xr.x)[c] += (&periodic_corr.x)[c]; 
                  }    
                  params.rdotr += (&params.xr.x)[c]*(&params.xr.x)[c];
                }
                params.q = sqrt(params.rdotr)/params.h;
                /*        if(params.q<=r_cutoff){
                          for(c = 0; c < dim; c++)
                          (&params.dwdx.x)[c] = (&params.xr.x)[c]*kernel_Fab(params.q, params.h);
                          pair_interact(pdata, id_a, id_b, t_interact, params);
                          } */
                if(params.q<=1.0 /*r_cutoff*/ && (id_a != id_b)){
                  //-------------------------------------------- 
                  // get positions of points a and b.
                  vector local_r;
                  //vector local_origin;
                  for(c=0;c<dim;c++)
                    (&local_r.x)[c] =(&pdata[id_b].pos.x)[c] - (&pdata[id_a].pos.x)[c];


                  // make a the centre of the coordinate
                  //that is the pos of particle a;
                  // a->b is the x axis - get the theta between a->b and x axis

                  double theta;
                  double mag_local_r  = sqrt(local_r.x*local_r.x + local_r.y*local_r.y);
                  if(local_r.y >0.0)
                    theta = acos(local_r.x/ mag_local_r);
                  else
                    theta = 2.0*PI - acos(local_r.x/ mag_local_r);
                  // use the angle to transform coordinates of point b. Which in this case will be
                  // (|local_r|,0.0 )
                  // coordinate_transform();
                  // shift coordinates origin to pos of a
                  // get the local coordinate transform of b
                  // run loop of all paticles within cutoff of a
                //  int is_interface_a, is_interface_b;
                  int lower_interior=0, upper_interior=0;
                  for(id_c = 0; id_c <N; id_c++){
                    if((id_c!= id_a) && (id_c!=id_b)){
                      vector xr_c;
                      double xrdot=0.0;
                      //calculate distance and check distance
                      for(c = 0; c<dim;c++){
                        (&xr_c.x)[c] = (&pdata[id_a].pos.x)[c] - (&pdata[id_c].pos.x)[c];
                        xrdot += (&xr_c.x)[c]*(&xr_c.x)[c];
                      }
                      xrdot = sqrt(xrdot);
                      if(xrdot/pdata[id_a].h < 3.0){    
                        vector local_c;
                        local_c = coordinate_transform(pdata[id_c].pos, theta, pdata[id_a].pos);
                        //lower
                        if(local_c.y<0.0){
                          if(local_c.x >=0.0 && local_c.x <= mag_local_r)
                            ++lower_interior;
                        } else {
                          //upper
                          if(local_c.x >=0.0 && local_c.x <= mag_local_r)
                            ++upper_interior;
                        }
                        // transform points to local coordinate. Check 1: if any point's x' coord is 
                        // greater than zero and less than x' of b in positive y. Check 2: in negative y.
                        //
                        //
                      }
                    }
                  }
                  /*        if(lower_interior==0 || upper_interior == 0){
                            pdata[id_a].interface_tag =1;
                            pdata[id_b].interface_tag =1;
                            }*/
                  printf("lower_upper %d %d \n",lower_interior, upper_interior); 
                  if(lower_interior == 0 || upper_interior == 0){
                    //  printf("anybody\n");
                    //
                    pdata[id_a].interface_tag =1;
                    pdata[id_b].interface_tag =1;
                  }


                } 
                //----------------------------------------------
                id_b = cells.cellofpart[id_b];
              }
            }
          }
        }
      }
      id_a = cells.cellofpart[id_a];
    }
  }
  printf("\n done\n");
  return;

}

static vector coordinate_transform( vector pos, double alpha, vector pos_o)
{
 vector pos_t;                                                                 
/*
 pos_t.x = pos.x * cos(alpha) + pos.y*sin(alpha) - pos_o.x;                              
     pos_t.y = -pos.x * sin(alpha) + pos.y*cos(alpha) - pos_o.y;                             
  */                                                                                   
 pos_t.x = (pos.x-pos_o.x) * cos(alpha) + (pos.y-pos_o.y)*sin(alpha); 
 pos_t.y = -(pos.x-pos_o.x )* sin(alpha) + (pos.y- pos_o.y)*cos(alpha) ;                             
 return pos_t; 
}


void interact(particleData * pdata, interact_type * t_interact, int num_interact, double dt)
{

  int i,c,k,d;
  int id_a, id_b;
  intvector coordi,coord_neighbor;
  int neighb_id;
  int p,q,r;
  vector periodic_corr;
  for(c=0;c<dim;c++)
    (&periodic_corr.x)[c] = (&xhigh->x)[c] - (&xlow->x)[c];

  double neighborhood = pow(r_cutoff * pdata[0].h,2.0);
  /* i is current CELL*/
#ifdef _OPENMP
#pragma omp parallel for default(shared) private( i,c,d,id_a,id_b,coordi,coord_neighbor, p,q,r,neighb_id,k)
#endif
  for(i=0;i<(cells.dim.x*cells.dim.y*cells.dim.z);i++)
  {
    id_a = cells.partincell[i];
    coordi = id_to_coord(i);
    while(id_a >=0){  
      /* assuming unsymmetric interaction to preserve local effects in parallelization */
      for(p=coordi.x-1;p<=coordi.x+1;p++){
        for(q=coordi.y-1;q<=coordi.y+1;q++){
          for(r=coordi.z-1;r<=coordi.z+1;r++){
            coord_neighbor.x = p;
            coord_neighbor.y = q;
            coord_neighbor.z = r;
            /* account for periodicity...> */
            for(c=0;c<dim;c++)
            {
              if(periodic[c] ==1){
                if( (&coord_neighbor.x)[c] == (&cells.dim.x)[c] ){                  
                  (&coord_neighbor.x)[c] = 0;
                }else if((&coord_neighbor.x)[c] == -1){
                  (&coord_neighbor.x)[c] = (&cells.dim.x)[c] - 1;
                }
              }
            }
            /* <...periodic*/
           
            /* accounting for ghost interactions for symmetry and noslip wall ... >*/
            /* 
             * int flag_ghost=-1; //
            for(c=0;c<2*dim;c++)
            {// have to check the d value and the division !!!!!!!!!!!!!!!!!!!!! 
              if(domain_bc[c] == 1){
                d=c/2;
                if((coord_neighbor.x)[d] = (&cells.dim.x)[d]){
                  (coord_neighbor.x)[d] = (&cells.dim.x)[d] - 1;
                  flag_ghost = c;
                }else if ((coord_neighbor.x)[d] = -1){
                  (coord_neighbor.x)[d] = 0;
                  flag_ghost = c;
                }
              }
            }*/
            /* <... ghost interactions */
            neighb_id = coord_to_id(coord_neighbor);
            /* Asymmetric interactions, so neighbors in all directions are considered */
            if(neighb_id >= 0 )
            {
              id_b = cells.partincell[neighb_id];
              while(id_b >=0 ){
                interactParams params;
                params.rdotr = 0.0;
                params.h = (pdata[id_a].h + pdata[id_b].h)/2.0;
                for(c = 0; c<dim;c++){
                  (&params.xr.x)[c] = (&pdata[id_a].pos.x)[c]-(&pdata[id_b].pos.x)[c];
                  if(periodic[c] && fabs((&params.xr.x)[c])>0.5*fabs((&periodic_corr.x)[c])){
                    if((&params.xr.x)[c] > 0.)                                  
                      (&params.xr.x)[c] += -(&periodic_corr.x)[c];
                    else                                                        
                      (&params.xr.x)[c] += (&periodic_corr.x)[c]; 
                  }    
                  params.rdotr += (&params.xr.x)[c]*(&params.xr.x)[c];
                }
                if(params.rdotr<=neighborhood){
                  particleData * p_b = &pdata[id_b] ;
                  particleData * p_a = &pdata[id_a] ;
                  //params.h = p_a->h ;                                                       
                  params.h = (p_a->h + p_b->h)/2.0;                                       
                  for(c = 0; c<dim;c++){                                                    
                    (&params.sr.x)[c] = (&p_a->pos_s.x)[c] - (&p_b->pos_s.x)[c];            
                    (&params.vr.x)[c] = (&p_a->vel.x)[c] - (&p_b->vel.x)[c];                
                  }                                                                         
                  params.q = sqrt(params.rdotr)/params.h;                                   
                  for(c = 0; c<dim;c++){                                                    
                    (&params.dwdx.x)[c] = (&params.xr.x)[c]*kernel_Fab(params.q, params.h); 
                  }                                                                         
                  for(k=0;k<num_interact;k++){                                              
                    pair_interact(p_a, p_b, t_interact[k], params,false);                   
                  }                                              
                } 
                id_b = cells.cellofpart[id_b];
              }
            }
          }
        }
      }
      id_a = cells.cellofpart[id_a];
    }
  }
  return;
}
