#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#ifdef HAVE_CONFIG_H
        #include "lis_config.h"
#else
#ifdef HAVE_CONFIG_WIN32_H
        #include "lis_config_win32.h"
#endif
#endif
#include "lis.h"
#include "sph.h"
#include "particles.h"
#include "kernel.h"
#include "run.h"
#include "isph.h"
#include "boundary.h"
#include "forces.h"

static void bicgstab_solve_LIS(particleData *pdata,double dt);
static void set_b(particleData *pdata, double dt);
static void copy_pressure(particleData * pdata, LIS_VECTOR *x);

void solve_pressure(particleData * pdata, double dt)
{
    bicgstab_solve_LIS(pdata,dt);
}

static void bicgstab_solve_LIS(particleData *pdata,double dt)
{
  LIS_VECTOR test;

 // double sec2 = omp_get_wtime();
  int n = N_isph;
  int i;
  nnz = 0;

#pragma omp parallel for default(shared) private(i)
  for(i=0;i<N;i++){
    if(pdata[i].isph_id>=0)
#pragma omp atomic  
    nnz += pdata[i].num_neighbors;
  }
  nnz += n;

#ifdef USE_MPI
  MPI_Comm_size(MPI_COMM_WORLD,&int_nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&int_my_rank);
  nprocs = int_nprocs;
  my_rank = int_my_rank;
#else
  nprocs  = 1;
  my_rank = 0;
#endif
  printf("num non zero is %d \n", nnz);
  row = (int *)malloc(nnz*sizeof(int));
  column =(int *) malloc(nnz*sizeof(int));
  value = (double *) malloc(nnz*sizeof(double));
  dirichlet = (double *) malloc (n*sizeof(double));

#pragma omp parallel for default(shared) private(i)
  for(i=0;i<nnz;i++)
    value[i] = 0.0;
#pragma omp parallel for default(shared) private(i)
  for(i=0;i<n;i++)
    dirichlet[i] = 0.0;

  err = lis_matrix_create(LIS_COMM_WORLD,&A); CHKERR(err);
  lis_solver_create(&solver);

  err = lis_matrix_set_size(A,0,n); CHKERR(err);
  lis_matrix_get_size(A,&n,&gn);
  lis_matrix_get_range(A,&is,&ie);

  coo_id = n;
  interact_type itype[] = {pressSolve};
  interact(pdata, itype, 1, dt); // create row, column, value arrays

  err = lis_matrix_set_coo(nnz, row , column, value,A); CHKERR(err);
//  lis_matrix_set_type(A,LIS_MATRIX_COO);
  err = lis_matrix_assemble(A); CHKERR(err);

  lis_vector_duplicate(A,&b);
  lis_vector_duplicate(A,&test);
  lis_vector_duplicate(A,&x);
  lis_vector_set_all(0.0,x);
#pragma omp parallel for default(shared) private(i)
  for(i=0;i<N;i++){
    if(pdata[i].isph_id >=0 ){
      lis_vector_set_value(LIS_INS_VALUE,pdata[i].isph_id, pdata[i].pressure,x);
    }
  }

  set_b(pdata, dt);
  lis_solver_create(&solver);
  char solver_options_2[100]; char solver_options_1[100];
  sprintf(solver_options_1, "-i bicgstab -p none -maxiter %d",max_iter);       
  if(sim_time >0.0)
	  sprintf(solver_options_2, "-tol %e -print mem ",bicg_eps);                    
  else
	  sprintf(solver_options_2, "-tol %e -print mem ",1.0e-8);                    
  lis_solver_set_option(solver_options_1,solver);                              
  lis_solver_set_option(solver_options_2,solver);

  //lis_solve_kernel(A,b,x,solver,precon) ;
  lis_solve(A,b,x,solver) ;
  lis_solver_get_iter(solver,&iter);
  lis_solver_get_residualnorm(solver,&resid);
  lis_solver_get_solver(solver,&nsol);
  lis_solver_get_solvername(nsol,solvername);
  printf("%s: relative residual 2-norm = %e\n\n",solvername,resid);

  if (solver->retcode == LIS_MAXITER){
    printf("\n return code is max iter \n");
    exit(1);
  }

  if (my_rank==0)
  {
    printf("iter = %d\n",iter);
  }
  copy_pressure(pdata, &x);

  lis_matvec(A,x,test);
    //compare test and b
//  for(i=0;i<n;i++)
  //{
   //  if(fabs(x->value[i] - test->value[i]) < DELTA)
     //  printf("Difference \n");
//  }
  lis_matrix_destroy(A);
  lis_vector_destroy(x); lis_vector_destroy(b);
  lis_solver_destroy(solver);

  return;
}

void pressure_matrix_build(particleData *p_a, particleData * p_b, interactParams params/*, double mrho, double hsquared*/,
boolean ghost)
{

  //LIS_INT err;
  int i;
  int c;
  double rdotdwdx=0.;
  double rdotr =  params.rdotr;
  double h = params.h;
  double q;
  vector dwdx,periodic_corr;
  vector xr = params.xr;
  for(i = 0; i < dim; i++)
    rdotdwdx += (&params.xr.x)[i]*(&params.dwdx.x)[i];
  for(c=0;c<dim;c++)                                                           
    (&periodic_corr.x)[c] = (&xhigh->x)[c] - (&xlow->x)[c];    
  //double Fab = rdotdwdx/(params.rdotr + 0.0001*params.h*params.h);
  double Fab = rdotdwdx/(params.rdotr + 0.0001*h*h);
  double Kab, Kba;
  /*
     Kab = (p_b->mass/p_b->rho) * (4.0/(p_a->rho + p_b->rho))*Fab;
     Kba = (p_a->mass/p_a->rho) * (4.0/(p_a->rho + p_b->rho))*Fab;
     */
  if(defgrad){
    double frac_a, frac_b;                                                  
    double a11= 1.0+p_a->def_grad_n.xx.x;                                    
    double a22= 1.0+p_a->def_grad_n.yy.y;                                    
    double a12= p_a->def_grad_n.xx.y;                                        
    double a21= p_a->def_grad_n.yy.x;                                        

    double b11= 1.0+p_b->def_grad_n.xx.x;                                    
    double b22= 1.0+p_b->def_grad_n.yy.y;                                    
    double b12= p_b->def_grad_n.xx.y;                                        
    double b21= p_b->def_grad_n.yy.x;                                        

    frac_a = a22*(4.0*xr.x*xr.x/rdotr -1.0) + a11*(4.0*xr.y*xr.y/rdotr -1.0)
      -(a12+a21)*(4.0*xr.x*xr.y/rdotr) ;                                    

    frac_b = b22*(4.0*xr.x*xr.x/rdotr -1.0) + b11*(4.0*xr.y*xr.y/rdotr -1.0)
      -(b12+b21)*(4.0*xr.x*xr.y/rdotr) ;                                    

    if(ghost==false){
      Kab = (p_b->mass/p_b->rho) * (1.0/p_a->rho )*frac_a*Fab;         
      Kba = (p_a->mass/p_a->rho) * (1.0/p_b->rho )*frac_b*Fab;      
    }else
      Kab = (p_b->mass/p_b->rho) * (1.0/p_a->rho )*frac_a*Fab;         

  }else{
    /*  Kab = mrho*Fab;
        Kba = mrho*Fab;*/
    if(ghost==false){
      Kab = (p_b->mass/p_b->rho)*(4.0/(p_a->rho + p_b->rho))*Fab;
      Kba = (p_a->mass/p_a->rho)*(4.0/(p_a->rho + p_b->rho))*Fab;
    }else{
      Kab = (p_b->mass/p_b->rho)*(4.0/(p_a->rho + p_b->rho))*Fab;
      /*  Kba = (p_a->mass/p_a->rho)*(4.0/(p_a->rho + pdata[-(b+1)].rho))*Fab;*/
    }
  }

  // ----  done
//  if(ghost==false){
 if(fabs(p_a->pos.x - p_b->pos.x)<0.5*(xhigh->x - xlow->x) &&     fabs(p_a->pos.y - p_b->pos.y)<0.5*(xhigh->y - xlow->y)){
    if(p_a != p_b) {                                                                 
      if(p_a->isph_id>=0 ) {                                                 
        if( p_a->wsum<=0.95 &&  p_a->freesurface ){                      
          row[p_a->isph_id] = p_a->isph_id;                              
          column[p_a->isph_id] = p_a->isph_id;                           
          value[p_a->isph_id] = kappa;                                       
//          lis_matrix_set_value(LIS_INS_VALUE,p_a->isph_id, p_a->isph_id,kappa, A);
        }else{                                                                    
          row[p_a->isph_id] = p_a->isph_id;                              
          column[p_a->isph_id] = p_a->isph_id;                           
          value[p_a->isph_id] += Kab;                                        
//          lis_matrix_set_value(LIS_ADD_VALUE,p_a->isph_id, p_a->isph_id,Kab, A);
        }                                                                        
      }                                                                          
      if(p_a->isph_id>=0 && p_b->isph_id>=0){                            
        int coo_id_local;
#pragma omp atomic capture
        {
          coo_id_local = coo_id;  
          coo_id ++;
        } 
        row[coo_id_local] = p_a->isph_id;                                          
        column[coo_id_local] = p_b->isph_id;                                       
        value[coo_id_local] += -Kab;                                                    
 //       lis_matrix_set_value(LIS_ADD_VALUE,p_a->isph_id, p_b->isph_id,-Kab, A);
        if(coo_id_local >= nnz || coo_id_local <N_isph){                                      
          printf("coo id is wrong/larger than nnz \n");                          
          exit(1);                                                               
        }       
      }
    }    
  }else{ /* ghost particles*/
    if(sim_time==500.0 && !(defgrad)){ //Nuemann first time step
      if(p_a!=p_b){
        if(p_a->isph_id>=0 ) {                                                 
          row[p_a->isph_id] = p_a->isph_id;                              
          column[p_a->isph_id] = p_a->isph_id;        
          value[p_a->isph_id] += Kab;                                        
        }                 
        /* if(p_a->isph_id>=0 && p_b->isph_id>=0){                            
           int coo_id_old,m;
           coo_id_old =-1;
           for(m=0 ; m<nnz;m++){
           if((row[m] == p_a->isph_id && column[m] == p_b->isph_id)
           || (row[m] == p_b->isph_id && column[m] == p_a->isph_id) ){
           coo_id_old = m;
           break;
           }
           }
           if(coo_id_old < 0){
           printf("Issue with ghost matrix build %d %d\n", a,b);
           exit(1);
           }else{
           value[coo_id_old] += -Kab;
           }
           } */ // this loop has to go when the actual particle interact. 
      }
    }else{ //Dirichlet - rest of the time steps
      if(p_a->isph_id>=0 ) {                                                 
        row[p_a->isph_id] = p_a->isph_id;                              
        column[p_a->isph_id] = p_a->isph_id;        
        value[p_a->isph_id] += Kab;                                        
      }                 
      if(p_a->isph_id>=0 && p_b->isph_id>=0){                            

        dirichlet[p_a->isph_id] += Kab*p_b->pressuren;

      }

    }

  }
} 


static void set_b(particleData *pdata,  double dt)
{
  int i;
#pragma omp parallel for default(shared) private(i)
  for(i=0;i<N;i++){
    if(pdata[i].isph_id>=0)
        lis_vector_set_value(LIS_INS_VALUE, pdata[i].isph_id, (pdata[i].u/dt + dirichlet[pdata[i].isph_id]), b);

  }
}

static void copy_pressure(particleData * pdata, LIS_VECTOR *x)
{
  int i;
#pragma omp parallel for default(shared) private(i)
  for(i=0;i<N;i++)
  {
    if(pdata[i].isph_id>=0)
      pdata[i].pressure = (*x)->value[pdata[i].isph_id];
    else 
      pdata[i].pressure = 0.0;
  }
}
