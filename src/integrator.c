#include <stdio.h>
#include <stdlib.h>

#include "integrator.h"

void * integrators (int choice)
{
  switch(choice){
  case 0:  return &integrate_isph;
  case 1:  return &integrate_isph_cummins;
  case 2:  return &integrate_isph_velocity_verlet;
  default: return NULL;
  }
}
