
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include "sph.h"
#include "particles.h"
#include "kernel.h"
#include "forces.h"
#include "expression_parser.h"
#include "string.h"

static void initialize_with_zeros(particleData * pdata);
static particleData * dam_break_3d();
static particleData * sessile_square_3d();
static particleData * perfect_circle();
static particleData * cube();
static particleData * parallel_plates();
static particleData * circular_drop();
static particleData * spherical_drop();
static particleData * lid_driven_cavity();
static particleData * sphere_water_entry();
static particleData * sphere_membrane();
static particleData * liquid_bridge_3D();

static void viscosity_assign(particleData * pdata);
static void density_assign(particleData * pdata);
static vector * points_in_sphere(double R_max, double dx, int * n_sphere);
static vector * points_on_sphere(int  n);

static int variable_callback( void *user_data, const char *name, double *value )  ;       
static int function_callback( void *user_data, const char *name, const int num_args, const double *args, double *value );
static boolean particle_in_geometry(vector current_pos, triangle * faces, int num_faces );
static char ch_cap ( char c )     ;
static int i4_min ( int i1, int i2 ) ;                                           
static boolean ch_is_space ( char c ) ;                                          
static boolean s_eqi ( char *s1, char *s2 );                                     
static triangle * read_geometry(FILE * solid_geometry, int * face_num)   ; 

particleData * create_particles_from_expression( int * expr_phase, char * expr_string[])
{
  //particleData * pdata = NULL;
  int i = 0;
  int count  =0 ;
  double dy= particle_dx;
  double dx = dy;
  double dz = dy;
  FILE * geometry_fp;
  /* Estimate the total number of particles */
  double x_low= xlow->x + dx/2.0;
  double y_low= xlow->y + dy/2.0 ;
  double x_high= xhigh->x ;
  double y_high= xhigh->y;/*+ delx;*/

  double z_low;  //xlow->z;
  double z_high; //xhigh->z;
  if (dim==2){
    z_low = 0.0; z_high = 0.0;
  }else if (dim ==3){
    z_low = xlow->z + dz/2.0 ; z_high = xhigh->z;
  }
  double x = x_low;
  double y = y_low;
  double z = z_low;

  vector local_pos;
  int geometry_expression_bool;

  int value[num_phases]; 
  int q;
// Creating a list of faces of individual geometries
  geomList * glist;
  geomList * temp;
  int gcount = 0;
  for(q=0;q<num_phases;q++){
    char * current_expression = expr_string[q] ;
    if(current_expression[0] == ':'){ //wildcard for reading stl file
      current_expression++;
      geometry_fp = fopen(current_expression, "r");
      if(geometry_fp == NULL){                                               
        printf("Unable to open geometry(stl) file%s \n", current_expression);               
        exit(1);                                                                
      } 
      if(gcount==0){ 
        glist = (geomList *) malloc(sizeof(geomList));
        glist->faces = read_geometry(geometry_fp, &glist->num_faces);
        glist->next = NULL;
        temp = glist;
        printf("Reading in geometry %s ... Num of faces %d \n", current_expression,glist->num_faces);               
      }else {
          temp->next =(geomList *) malloc(sizeof(geomList));
          temp =  temp->next;
          temp->faces = read_geometry(geometry_fp, &temp->num_faces);
        printf("Reading in geometry %s ... Num of faces %d \n", current_expression,temp->num_faces);
        }
      gcount ++;
      fclose(geometry_fp);
    }
  }
  int num_faces;

  while(z<=z_high){
    while(y <= y_high){
      while(x <= x_high){

        local_pos.x = x; local_pos.y = y; local_pos.z = z;
        geometry_expression_bool = 0;

          temp = glist; 
        for(q=0;q<num_phases;q++){
          char * current_expression; 
          current_expression = expr_string[q] ;
          if(current_expression[0] == ':'){ //wildcard for reading stl file
            value[q] = particle_in_geometry(local_pos, glist->faces, glist->num_faces);
            glist =  glist->next;
          }else{
            value[q] = (int) parse_expression_with_callbacks(expr_string[q], variable_callback, function_callback, &local_pos);
          }
          geometry_expression_bool = geometry_expression_bool || value[q]; 
        }
        glist = temp;
        if( geometry_expression_bool) 
          ++count;
        
        x += dx;
      }
      x = x_low ;
      y += dy;
    }
    x=x_low;
    y=y_low;
    z+=dz;
  }
  printf ( "Total Number of Particles = %d\n", count);
  N = count;
  count = 0;

  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf ( "Total Number of Cells = %d\n", cells.dim.x*cells.dim.y*cells.dim.z);

  /* left boundary*/
  //x_low= xlow->x ;
  //y_low= xlow->y ;
  //x_high= xhigh->x ;
  //y_high= xhigh->y;/*+ delx;*/

  x = x_low;
  y = y_low;
  z = z_low;
while(z <= z_high){
  while(y <= y_high){
    while(x <= x_high){  
      local_pos.x = x; local_pos.y = y;local_pos.z = z;
      geometry_expression_bool = 0;
      for(q=0;q<num_phases;q++){
          char * current_expression; 
          current_expression = expr_string[q] ;
          if(current_expression[0] == ':'){ //wildcard for reading stl file
            value[q] = particle_in_geometry(local_pos, glist->faces, glist->num_faces);
            glist =  glist->next;
        }else{
          value[q] = (int) parse_expression_with_callbacks(expr_string[q], variable_callback, function_callback, &local_pos);
        }
        geometry_expression_bool = geometry_expression_bool || value[q]; 
      }
      glist = temp;
      if(geometry_expression_bool){ 
        pdata[i].pos.x = local_pos.x;
        pdata[i].pos.y = local_pos.y; 
        pdata[i].pos.z = local_pos.z;
        pdata[i].vel.x = 0.;
        pdata[i].vel.y = 0.;
        pdata[i].vel.z = 0.;
        pdata[i].acc.x = 0.;
        pdata[i].acc.y = 0.;
        pdata[i].acc.z = 0.;
        pdata[i].incompressible = 0; 
        pdata[i].h = h_global*dx; 

        pdata[i].rho_wcsph = 0.0;
        pdata[i].rho = 1000.0;
        if(dim==2)
          pdata[i].mass = pdata[i].rho*(dx*dy);
        else if(dim==3)
          pdata[i].mass = pdata[i].rho*(dx*dy*dz);
        //pdata[i].sigma0 = p->rho/p->mass;
        pdata[i].pressure = 0.0;
        int kk;
    // problem part acts weird with O3
        for(kk=num_phases;kk>0;kk=kk-1){
          if(value[kk-1] == 1){
            pdata[i].phase = expr_phase[kk-1];
            pdata[i].body_id = kk-1;
          }
        }
          // problem part
          
      /*  
        if(value[0] == 1)
          pdata[i].phase =  expr_phase[0];
        else if(value[1] == 1)
          pdata[i].phase =  expr_phase[1];
        else if(value[2] == 1)
          pdata[i].phase =  expr_phase[2];
        */  
        pdata[i].freesurface = true;
        if(pdata[i].phase <=  0 )
          pdata[i].freesurface = false;

        pdata[i].eta = 0.0001;
        //pdata[i].id = count; 
        ++count;
        ++i;
      }
      x += dx;
    }
    x = x_low;
    y += dy;
  }
    x=x_low;
    y=y_low;
    z+=dz;
  }
  // --------------------------------

  printf("%d particles built \n",count);


  return pdata;
}

particleData * create_particles(int model)
{
  particleData *pdata = NULL;

  switch (model){
    case 0:   
      pdata = cube();
      break;
    case 1:   
      pdata = dam_break_3d();
      break;
    case 2:   
      pdata = parallel_plates();
      break;
    case 3:   
      pdata = circular_drop();
      break;
    case 4:   
      pdata = lid_driven_cavity();
      break;
    case 5:   
      pdata = perfect_circle();
      break;
    case 6:   
      pdata = sphere_water_entry();
      break;
    case 7:   
      pdata = sphere_membrane();
      break;
    case 8:   
      pdata = sessile_square_3d();
      break;
    case 9:   
      pdata = liquid_bridge_3D();
      break;
    case 10:   
      pdata = spherical_drop();
      break;
  }
  return pdata;
}

static particleData * perfect_circle(){

  double dy= particle_dx;
  double dx = dy;

  int i=0;
  int count = 0;


  /* Estimate the total number of particles */
  /* left boundary*/
//  double x_low= -0.1;//xlow->x ;
//  double y_low= -0.1;//xlow->y ;

  double r_high=3.001;
  double t=0.0;
  double r=0.0;
  double dr=dx;


  int tn,ti;

  while(r <= r_high){
    tn=(int)(2.0*PI*r/dr);
    for(ti=0;ti<tn;ti++){
      ++count;
    }
    t  = 0.0;
    r += dr;
  }
  count++; //for the middle point
  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;


  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  initialize_with_zeros(pdata);
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  /* left boundary*/
//  x_low= -0.1;//xlow->x ;  y_low= -0.1;//xlow->y ;
 // x_high= 0.1;//xhigh->x  ;
 // y_high= 0.1;//xhigh->y;/*+ delx;*/

 // x = x_low+ dx/2.0;
 // y = y_low + dy/2.0;
  r = 0.0;
  while(r <= r_high){
    tn=(int)(2.0*PI*r/dr);
    for(ti=0;ti<tn;ti++){
      if(tn==0){
        //    pdata[i].pos.x = 0.0;
        //   pdata[i].pos.y = 0.0; 
      }
      else{
        t=ti* 2*PI/tn;
        pdata[i].pos.x = r*cos(t);
        pdata[i].pos.y = r*sin(t); 
      }
      pdata[i].pos.z = 0.;
      pdata[i].vel.x = 0.;
      pdata[i].vel.y = 0.;
      pdata[i].vel.z = 0.;

      //  pdata[i].kappa = 0.;

      pdata[i].acc.x = 0.;
      pdata[i].acc.y = 0.;
      pdata[i].acc.z = 0.;
      pdata[i].phase = 1 ;
      pdata[i].incompressible = 0; 
      pdata[i].freesurface = true; 
      pdata[i].h = h_global*dx; 

      pdata[i].rho_wcsph = 0.0;
      pdata[i].rho = 1000.0;

      pdata[i].mass = pdata[i].rho*(PI*0.1025*0.1025)/1310;//(dx*dy);
      //pdata[i].sigma0 = p->rho/p->mass;
      pdata[i].pressure = 0.0;
      pdata[i].eta = 0.01;
      //pdata[i].id = count; 
      ++count;
      ++i;
    }
    t = 0.0;
    r += dr;
  }
  //----------

  pdata[i].pos.x = 0.;
  pdata[i].pos.y = 0.;
  pdata[i].pos.z = 0.;
  pdata[i].vel.x = 0.;
  pdata[i].vel.y = 0.;
  pdata[i].vel.z = 0.;

  //  pdata[i].kappa = 0.;

  pdata[i].acc.x = 0.;
  pdata[i].acc.y = 0.;
  pdata[i].acc.z = 0.;
  pdata[i].phase = 1 ;
  pdata[i].incompressible = 0; 
  pdata[i].freesurface = true; 
  pdata[i].h = h_global*dx; 

  pdata[i].rho_wcsph = 0.0;
  pdata[i].rho = 1000.0;

  //pdata[i].mass = pdata[i].rho*(dx*dy);
  pdata[i].mass = pdata[i].rho*(PI*0.1025*0.1025)/1310;//(dx*dy);
  //pdata[i].sigma0 = p->rho/p->mass;
  pdata[i].pressure = 0.0;
  pdata[i].eta = 0.01;
  //pdata[i].id = count; 
  ++count;
  ++i;
  //---------
  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;
}
static particleData * liquid_bridge_3D(){
  int i = 0;
  int count  =0 ;
  double dy= particle_dx;
  double dx = dy;
  double dz = dy;
  /* Estimate the total number of particles */
  double x_low= xlow->x ;
  double y_low= xlow->y ;
  double z_low= xlow->z ;
  double x_high= xhigh->x ;
  double y_high= xhigh->y;/*+ delx;*/
  double z_high= xhigh->z;/*+ delx;*/

  double x = x_low;
  double y = y_low;
  double z = z_low;
  double radius = 1.5e-3 ;
  double radius_low = 0.5e-3;
  while(z <= z_high){
  while(y <= y_high){
    while(x <= x_high){
      //      if(fabs(x) <= radius  && fabs(y)<=radius)
      if( (fabs(x) > radius) ||
          (fabs( x)<= radius && y*y + z*z <= radius_low*radius_low )) 
        ++count;
      x += dx;
    }
    x = x_low ;
    y += dy;
  }
  x=x_low;
  y=y_low;
  z += dz;
  }
  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;

  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf ( "Total Number of Cells = %d\n", cells.dim.x*cells.dim.y*cells.dim.z);

  /* left boundary*/
  x_low= xlow->x ;
  y_low= xlow->y ;
  z_low= xlow->z ;
  x_high= xhigh->x ;
  y_high= xhigh->y;/*+ delx;*/
  z_high= xhigh->z;/*+ delx;*/

  x = x_low;
  y = y_low;
  z = z_low;

  while(z <= z_high){
  while(y <= y_high){
    while(x <= x_high){
      if( (fabs(x) > radius) ||
          (fabs( x)<= radius && y*y + z*z <= radius_low*radius_low )){ 
        pdata[i].pos.x = x;
        pdata[i].pos.y = y; 
        pdata[i].pos.z = z;
        pdata[i].vel.x = 0.;//-100. *x;
        pdata[i].vel.y = 0.;//100.  *y;
        pdata[i].vel.z = 0.;
        //  pdata[i].kappa = 0.;
        pdata[i].acc.x = 0.;
        pdata[i].acc.y = 0.;
        pdata[i].acc.z = 0.;
        pdata[i].incompressible = 0; 
        pdata[i].h = h_global*dx; 

        pdata[i].rho_wcsph = 0.0;
        pdata[i].rho = 1000.0;
        pdata[i].mass = pdata[i].rho*(dx*dy*dz);
        //pdata[i].sigma0 = p->rho/p->mass;
        pdata[i].pressure = 0.0;
        if(fabs(x)>radius)
          pdata[i].phase = -1;
        else
          pdata[i].phase = 1;
        pdata[i].freesurface = true;
        pdata[i].eta = 0.0001;
        //pdata[i].id = count; 
        ++count;
        ++i;
      }
      x += dx;
    }
    x = x_low;
    y += dy;
  }
  x=x_low;
  y=y_low;
  z+=dz;
  }

  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;

}
static particleData * sessile_square_3d(){
  int i = 0;
  int count  =0 ;
  double dy= particle_dx;
  double dx = dy;
  double dz = dy;
  /* Estimate the total number of particles */
  double x_low= xlow->x ;
  double y_low= xlow->y ;
  double z_low= xlow->z ;
  double x_high= xhigh->x ;
  double y_high= xhigh->y;/*+ delx;*/
  double z_high= xhigh->z;/*+ delx;*/

  double x = x_low;
  double y = y_low;
  double z = z_low;
  double radius_up =  1.0;
  double radius_low = 2.5;
  while(z <= z_high){
    while(y <= y_high){
      while(x <= x_high){
        //      if(fabs(x) <= radius  && fabs(y)<=radius)
        if((z>=0 && z<=radius_up && fabs(x)<=radius_up && fabs(y)<=radius_up )  ||
            (z<=0 && z>=-0.7 && fabs(x)<=radius_low && fabs(y)<=radius_low )) 
          ++count;
        x += dx;
      }
      x = x_low ;
      y += dy;
    }
    y=y_low;
    x=x_low;
    z += dz;
  }
  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;

  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf ( "Total Number of Cells = %d\n", cells.dim.x*cells.dim.y*cells.dim.z);

  /* left boundary*/
  x_low= xlow->x ;
  y_low= xlow->y ;
  z_low= xlow->z ;
  x_high= xhigh->x ;
  y_high= xhigh->y;/*+ delx;*/
  z_high= xhigh->z;/*+ delx;*/

  x = x_low;
  y = y_low;
  z=z_low;
  while(z <= z_high){
    while(y <= y_high){
      while(x <= x_high){
        if((z>=0 && z<=radius_up && fabs(x)<=radius_up && fabs(y)<=radius_up )  ||
            (z<=0 && z>=-0.7 && fabs(x)<=radius_low && fabs(y)<=radius_low )){ 
          pdata[i].pos.x = x;
          pdata[i].pos.y = y; 
          pdata[i].pos.z = z;
          pdata[i].vel.x = 0.;//-100. *x;
          pdata[i].vel.y = 0.;//100.  *y;
          pdata[i].vel.z = 0.;
          //  pdata[i].kappa = 0.;
          pdata[i].acc.x = 0.;
          pdata[i].acc.y = 0.;
          pdata[i].acc.z = 0.;
          pdata[i].incompressible = 0; 
          pdata[i].h = h_global*dx; 

          pdata[i].rho_wcsph = 0.0;
          pdata[i].rho = 1.0;
          pdata[i].mass = pdata[i].rho*(dx*dy*dz);
          //pdata[i].sigma0 = p->rho/p->mass;
          pdata[i].pressure = 0.0;
          if(z>0.0)
            pdata[i].phase = 1;
          else
            pdata[i].phase = -1;
          pdata[i].freesurface = true;
          pdata[i].eta = 0.0001;
          //pdata[i].id = count; 
          ++count;
          ++i;
        }
        x += dx;
      }
      x = x_low;
      y += dy;
    }
    x = x_low;
    y = y_low;
    z += dz;
  }
  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;

}

static particleData * cube(){

  double dy= particle_dx;
  double dx = dy;
  double dz = dy;

  int i=0;
  int count = 0;


  /* Estimate the total number of particles */
  /* left boundary*/
  double x_low= -1.0;
  double y_low= -1.0;
  double z_low= -1.0;
  double x_high=1.0 ;
  double y_high=1.0;/*+ delx;*/
  double z_high=1.0;/*+ delx;*/

  double x = x_low+ dx/2.0;
  double y = y_low + dy/2.0;
  double z = z_low + dz/2.0;

  while(z <= z_high){
    while(y <= y_high){
      while(x <= x_high){
          
/*        if( (x-0.7)*(x-0.7) + y*y + z*z < 0.09 ||
            (y*y + z*z < 0.04 && fabs(x)<0.7) || 
       (x+0.7)*(x+0.7) + y*y + z*z < 0.09 ) */
        if( (x)*(x) + y*y + z*z < 1.0 )
          ++count;
        x += dx;
      }
      x = x_low + dx/2.0;
      y += dy;
    }
    x = x_low + dx/2.0;
    y = y_low + dy/2.0;
    z += dz;
  }
  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;


  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  initialize_with_zeros(pdata);
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();


  x = x_low+ dx/2.0;
  y = y_low + dy/2.0;
  z = z_low + dz/2.0;
  while(z <= z_high){
    while(y <= y_high){
      while(x <= x_high){
        if( (x)*(x) + y*y + z*z < 1.0 ){
/*        if( (x-0.7)*(x-0.7) + y*y + z*z < 0.09 ||
            (y*y + z*z < 0.04 && fabs(x)<0.7) || 
       (x+0.7)*(x+0.7) + y*y + z*z < 0.09 ){*/
          pdata[i].pos.x = x;
          pdata[i].pos.y = y; 
          pdata[i].pos.z = z; //sin(x)*sin(y);
          pdata[i].vel.x = 0.;
          pdata[i].vel.y = 0.;
          pdata[i].vel.z = 0.;

          //  pdata[i].kappa = 0.;

          pdata[i].acc.x = 0.;
          pdata[i].acc.y = 0.;
          pdata[i].acc.z = 0.;
          pdata[i].phase = 1;
          if(fabs(x)>x_high -dx || fabs(y)>y_high-dy || fabs(z) >z_high-dz)
            pdata[i].phase = 0;
          pdata[i].incompressible = 0; 
          pdata[i].freesurface = true;
          pdata[i].h = h_global*dx; 
          pdata[i].color = 1.0;


          pdata[i].rho_wcsph = 0.0;
          pdata[i].rho = 1.0;

          pdata[i].mass = pdata[i].rho*(dx*dy*dz);
          //pdata[i].sigma0 = p->rho/p->mass;
          pdata[i].pressure = 0.0;
         // pdata[i].eta = 0.01;
          //pdata[i].id = count; 
          ++count;
          ++i;
        }
        x += dx;
      }
      x = x_low + dx/2.0;
      y += dy;
    }
    x = x_low + dx/2.0;
    y = y_low + dy/2.0;
    z += dz;
  }

  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;
}

static particleData * dam_break_3d(){

  double dy= particle_dx;
  double dx = dy;
  double dz = dy;

  int i=0;
  int count = 0;


  /* Estimate the total number of particles */
  /* walls */
  double x_low= xlow->x ;
  double y_low= xlow->y ;
  double z_low= xlow->z ;
  double x_high= xhigh->x;
  double y_high= xhigh->y;/*+ delx;*/
  double z_high= xhigh->z;/*+ delx;*/

  double x = x_low ;
  double y = y_low ;
  double z = z_low ;

  while(z<=z_high){
    while(y <= y_high){
      while(x <= x_high){
        if(x<=0.0+DELTA || y<=0.0+DELTA || z<=-0.1+DELTA ||
            x>=0.4-DELTA || z>=0.1-DELTA ){
          ++count;
        }
        x += dx;
      }
      x = x_low ;
      y += dy;
    }
    x = x_low ;
    y = y_low ;
    z += dz;
  }
  /*dam */
  x_low= 0.0;
  y_low= 0.0;
  z_low= -0.1 ;
  x_high= 0.1 ;
  y_high= 0.2;
  z_high= 0.1;
  x = x_low+ dx;
  y = y_low + dy;
  z = z_low + dz;
  while(z<z_high){
    while(y <= y_high){
      while(x <= x_high){
        ++count;
        x += dx;
      }
      x = x_low +dx;
      y += dy;
    }
    x = x_low +dx ;
    y = y_low +dx ;
    z += dz;
  }
  /*box*/
  x_low= x_high+ 20*dx;
  y_low= 0.0;
  z_low= -0.025 ;
  x_high= 0.25 ;
  y_high= 0.05;
  z_high= 0.025;
  x = x_low;
  y = y_low + dy;
  z = z_low ;
  while(z<=z_high){
    while(y <= y_high){
      while(x <= x_high){
        ++count;
        x += dx;
      }
      x = x_low;
      y += dy;
    }
    x = x_low ;
    y = y_low+dy ;
    z += dz;
  }
  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;


  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  initialize_with_zeros(pdata);
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf("Total Number of Cells = %d\n", cells.dim.x*
      cells.dim.y*cells.dim.z);

  /* Walls*/
  x_low= xlow->x ;
  y_low= xlow->y ;
  z_low= xlow->z ;
  x_high= xhigh->x;
  y_high= xhigh->y;/*+ delx;*/
  z_high= xhigh->z;/*+ delx;*/

  x = x_low ;
  y = y_low ;
  z = z_low ;

  while(z<=z_high){
    while(y <= y_high){
      while(x <= x_high){
        if(x<=0.0+DELTA || y<=0.0+DELTA || z<=-0.1+DELTA ||
            x>=0.4-DELTA || z>=0.1-DELTA ){
          pdata[i].pos.x = x;
          pdata[i].pos.y = y; 
          pdata[i].pos.z = z;
          pdata[i].vel.x = 0.;
          pdata[i].vel.y = 0.;
          pdata[i].vel.z = 0.;

          pdata[i].acc.x = 0.;
          pdata[i].acc.y = 0.;
          pdata[i].acc.z = 0.;
          pdata[i].phase = -1;
          pdata[i].incompressible = 0; 
          pdata[i].h = h_global*dx; 

          pdata[i].rho_wcsph = 0.0;
          pdata[i].rho = 1000.0;

          pdata[i].mass = pdata[i].rho*(dz*dx*dy);
          //pdata[i].sigma0 = p->rho/p->mass;
          pdata[i].pressure = 0.0;
          pdata[i].freesurface = false;
          pdata[i].eta = 0.001;
          //pdata[i].id = count; 
          ++count;
          ++i;

        }   
        x += dx;
      }
      x = x_low ;
      y += dy;
    }
    x = x_low ;
    y = y_low ;
    z += dz;
  }
  /*dam */
  x_low= 0.0 ;
  y_low= 0.0;
  z_low= -0.1;
  x_high= 0.1 ;
  y_high= 0.2;
  z_high= 0.1 ;
  x = x_low+ dx;
  y = y_low + dy;
  z = z_low + dz;
  while(z<z_high){
    while(y <= y_high){
      while(x <= x_high){
        pdata[i].pos.x = x;
        pdata[i].pos.y = y; 
        pdata[i].pos.z = z;
        pdata[i].vel.x = 0.;
        pdata[i].vel.y = 0.;
        pdata[i].vel.z = 0.;
        pdata[i].acc.x = 0.;
        pdata[i].acc.y = 0.;
        pdata[i].acc.z = 0.;
        pdata[i].phase = 1;
        pdata[i].incompressible = 0; 
        pdata[i].h = h_global*dx; 

        pdata[i].rho = 1000.0;

        pdata[i].mass = pdata[i].rho*(dz*dx*dy);
        pdata[i].pressure = 0.0;
        pdata[i].freesurface = true;
        pdata[i].eta = 0.001;
        //  pdata[i].id = count; 
        ++count;
        ++i;
        x += dx;
      }
      x = x_low + dx ;
      y += dy;
    }
    x = x_low + dx;
    y = y_low + dy;
    z += dz;
  }
  /*box*/
  x_low= x_high+20*dx ;
  y_low= 0.0;
  z_low= -0.025;
  x_high= 0.25 ;
  y_high= 0.05;
  z_high= 0.025 ;
  x = x_low;
  y = y_low + dy;
  z = z_low ;
  while(z<z_high){
    while(y <= y_high){
      while(x <= x_high){
        pdata[i].pos.x = x;
        pdata[i].pos.y = y; 
        pdata[i].pos.z = z;
        pdata[i].vel.x = 0.;
        pdata[i].vel.y = 0.;
        pdata[i].vel.z = 0.;
        pdata[i].acc.x = 0.;
        pdata[i].acc.y = 0.;
        pdata[i].acc.z = 0.;
        pdata[i].phase = 0;
        pdata[i].incompressible = 0; 
        pdata[i].h = h_global*dx; 

        pdata[i].rho = 1000.0;

        pdata[i].mass = pdata[i].rho*(dz*dx*dy);
        pdata[i].pressure = 0.0;
        pdata[i].freesurface = false;
        pdata[i].eta = 0.001;
        //  pdata[i].id = count; 
        ++count;
        ++i;
        x += dx;
      }
      x = x_low ;
      y += dy;
    }
    x = x_low ;
    y = y_low + dy;
    z += dz;
  }

  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;
}
static particleData * sphere_membrane(){

  double dy= particle_dx;
  double dx = dy;
  double dz = dy;

  int i=0,k;
  int count = 0;

  /* Estimate the total number of particles */
  /*sphere */
  int n_sphere=(int) 4.0*PI/(dx*dx);
//  vector * pts = points_in_sphere(0.0127,dx/1.2599,&n_sphere);
  vector * pts = points_on_sphere(n_sphere);
  count = count + n_sphere;

  printf ( "Total Number of Particles = %d\n", count);

  N = count;
//  vector translation;
//  translation.x = 0.0; translation.y=0.0; 

  count = 0;
  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  initialize_with_zeros(pdata);
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf("Total Number of Cells = %d\n", cells.dim.x*
      cells.dim.y*cells.dim.z);

  /*sphere */
  for(k=0;k<n_sphere;k++){
    pdata[i].pos.x = pts[k].x;
    pdata[i].pos.y = pts[k].y; 
    pdata[i].pos.z = pts[k].z;
    pdata[i].vel.x = 0.;
    pdata[i].vel.y = 0.;
    pdata[i].vel.z = 0.;
    pdata[i].acc.x = 0.;
    pdata[i].acc.y = 0.;
    pdata[i].acc.z = 0.;
    pdata[i].phase = 0;
    pdata[i].incompressible = 0; 
    pdata[i].h = h_global*dx; 

    pdata[i].rho = 1000.0;

    pdata[i].mass = pdata[i].rho*(dz*dx*dy);
    pdata[i].pressure = 0.0;
    pdata[i].freesurface = true;
    pdata[i].eta = 0.001;
    ++i;
  }
  // --------------------------------
  printf("%d particles built \n",count+n_sphere);
  return pdata;
}

static particleData * sphere_water_entry(){

  double dy= particle_dx;
  double dx = dy;
  double dz = dy;

  int i=0,k,c;
  int count = 0;

  /* Estimate the total number of particles */
  /* water column */
  double x_low= xlow->x ;
  double y_low= xlow->y ;
  double z_low= xlow->z ;
  double x_high= xhigh->x;
  double y_high= xhigh->y;/*+ delx;*/
  double z_high= 0.0;/*+ delx;*/

  double x = x_low+dx/2.0 ;
  double y = y_low+dy/2.0 ;
  double z = z_low+dz/2.0 ;

  while(z<=z_high){
    while(y <= y_high){
      while(x <= x_high){
        ++count;
        x += dx;
      }
      x = x_low +dx/2.0;
      y += dy;
    }
    x = x_low +dx/2.0;
    y = y_low +dy/2.0;
    z += dz;
  }
  /*sphere */
  int n_sphere;
  vector * pts = points_in_sphere(0.0127,dx/1.2599,&n_sphere);
  count = count + n_sphere;

  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  vector translation;
  translation.x = 0.0; translation.y=0.0; 
  //  translation.z=0.0277;
  translation.z=0.015;
  for(i=0;i<n_sphere;i++){
    for(c=0;c<dim;c++){
      (&pts[i].x)[c] += (&translation.x)[c];
    }
  }

  count = 0;
  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  initialize_with_zeros(pdata);
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf("Total Number of Cells = %d\n", cells.dim.x*
      cells.dim.y*cells.dim.z);

  /* Walls*/
  x_low= xlow->x ;
  y_low= xlow->y ;
  z_low= xlow->z ;
  x_high= xhigh->x;
  y_high= xhigh->y;/*+ delx;*/
  z_high= 0.0;/*+ delx;*/

  x = x_low +dx/2.0;
  y = y_low +dy/2.0;
  z = z_low +dz/2.0;
  i = 0;
  while(z<=z_high){
    while(y <= y_high){
      while(x <= x_high){
        pdata[i].pos.x = x;
        pdata[i].pos.y = y; 
        pdata[i].pos.z = z;
        pdata[i].vel.x = 0.;
        pdata[i].vel.y = 0.;
        pdata[i].vel.z = 0.0;

        pdata[i].acc.x = 0.;
        pdata[i].acc.y = 0.;
        pdata[i].acc.z = 0.;
        pdata[i].phase = 1;

        if(z<xlow->z+5.0*dx)
          pdata[i].phase = -1;

        pdata[i].incompressible = 0; 
        pdata[i].h = h_global*dx; 

        pdata[i].rho_wcsph = 0.0;
        pdata[i].rho = 1000.0;

        pdata[i].mass = pdata[i].rho*(dz*dx*dy);
        //pdata[i].sigma0 = p->rho/p->mass;
        pdata[i].pressure = 0.0;
        pdata[i].freesurface = true;
        pdata[i].eta = 0.001;
        //pdata[i].id = count; 
        ++count;
        ++i;
        x += dx;
      }
      x = x_low +dx/2.0;
      y += dy;
    }
    x = x_low +dx/2.0;
    y = y_low +dy/2.0;
    z += dz;
  }
  /*sphere */
  for(k=0;k<n_sphere;k++){
    pdata[i].pos.x = pts[k].x;
    pdata[i].pos.y = pts[k].y; 
    pdata[i].pos.z = pts[k].z;
    pdata[i].vel.x = 0.;
    pdata[i].vel.y = 0.;
    pdata[i].vel.z = -2.1011;
    pdata[i].acc.x = 0.;
    pdata[i].acc.y = 0.;
    pdata[i].acc.z = 0.;
    pdata[i].phase = 0;
    pdata[i].incompressible = 0; 
    pdata[i].h = h_global*dx; 

    pdata[i].rho = 1000.0;

    pdata[i].mass = pdata[i].rho*(dz*dx*dy)/2.0;
    pdata[i].pressure = 0.0;
    pdata[i].freesurface = true;
    pdata[i].eta = 0.001;
    ++i;
  }

  // --------------------------------

  printf("%d particles built \n",count+n_sphere);

  return pdata;
}

static particleData * parallel_plates(){

  double dy= particle_dx;
  double dx = dy;

  int i=0;
  int count = 0;

  /* Estimate the total number of particles */
  /* walls */
  double x_low= xlow->x ;
  double y_low= xlow->y ;
  double x_high= xhigh->x ;
  double y_high= xhigh->y;/*+ delx;*/

  double x = x_low+ dx/2.0;
  double y = y_low + dy/2.0;

  while(y <= y_high){
    while(x <= x_high){
      ++count;
      x += dx;
    }
    x = x_low + dx/2.0;
    y += dy;
  }

  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;


  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  initialize_with_zeros(pdata);
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf ( "Total Number of Cells = %d\n", cells.dim.x*cells.dim.y*cells.dim.z);

  /* left boundary*/
  x_low= xlow->x ;
  y_low= xlow->y ;
  x_high= xhigh->x ;
  y_high= xhigh->y;/*+ delx;*/

  x = x_low+ dx/2.0;
  y = y_low + dy/2.0;

  while(y <= y_high){
    while(x <= x_high){
      pdata[i].pos.x = x;
      pdata[i].pos.y = y; 
      pdata[i].pos.z = 0.;
      pdata[i].vel.x = 0.;
      pdata[i].vel.y = 0.;
      pdata[i].vel.z = 0.;

      //  pdata[i].kappa = 0.;

      pdata[i].acc.x = 0.;
      pdata[i].acc.y = 0.;
      pdata[i].acc.z = 0.;
      pdata[i].incompressible = 0; 
      pdata[i].h = h_global*dx; 

      pdata[i].rho_wcsph = 0.0;
      pdata[i].rho = 1000.0;

      pdata[i].mass = pdata[i].rho*(dx*dy);
      //pdata[i].sigma0 = p->rho/p->mass;
      pdata[i].pressure = 0.0;
      if(y>xhigh->y - 10.0*dy || y<xlow->y + 10.0*dy){
        pdata[i].phase = -1;
        pdata[i].freesurface = true;
      } else{
        pdata[i].phase = 1;
        pdata[i].freesurface = false;
      }
      if((x-0.2)*(x-0.2) + (y-0.1)*(y-0.1) <= 0.05*0.05)
        pdata[i].phase = 0;

      pdata[i].eta = 0.01;
      //pdata[i].id = count; 
      ++count;
      ++i;
      x += dx;
    }
    x = x_low + dx/2.0;
    y += dy;
  }

  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;
}
static particleData * spherical_drop(){

  int i = 0;
  int count  =0 ;
  double dy= particle_dx;
  double dx = dy;
  double dz = dy;

  /* Estimate the total number of particles */
  /* walls */
  double x_low= xlow->x ;
  double y_low= xlow->y ;
  double z_low= xlow->z ;
  double x_high= xhigh->x ;
  double y_high= xhigh->y;/*+ delx;*/
  double z_high= xhigh->z;/*+ delx;*/

  double x = x_low;
  double y = y_low;
  double z = z_low;
double radius =  1.0;
  while(z <= z_high){
  while(y <= y_high){
    while(x <= x_high){
      if(x*x/1.5242 + y*y/0.81 + z*z/0.81 <= radius*radius)
//      if(fabs(x)<= radius && fabs(y)<=radius)
        ++count;
      x += dx;
    }
    x = x_low ;
    y += dy;
  }
    x = x_low ;
    y = y_low ;
    z += dz;
  }
  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;


  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf ( "Total Number of Cells = %d\n", cells.dim.x*cells.dim.y*cells.dim.z);

  /* left boundary*/
  x_low= xlow->x ;
  y_low= xlow->y ;
  z_low= xlow->z ;
  x_high= xhigh->x ;
  y_high= xhigh->y;/*+ delx;*/
  z_high= xhigh->z;/*+ delx;*/

  x = x_low;
  y = y_low;
  z = z_low;

  while(z <= z_high){
  while(y <= y_high){
    while(x <= x_high){
      if(x*x/1.5242 + y*y/0.81 + z*z/0.81 <= radius*radius){
  //    if(fabs(x)<= radius && fabs(y)<=radius){
        pdata[i].pos.x = x;
        pdata[i].pos.y = y; 
        pdata[i].pos.z = z;
        pdata[i].vel.x = 0.;//-100. *x;
        pdata[i].vel.y = 0.;//100.  *y;
        pdata[i].vel.z = 0.;

        //  pdata[i].kappa = 0.;

        pdata[i].acc.x = 0.;
        pdata[i].acc.y = 0.;
        pdata[i].acc.z = 0.;
        pdata[i].incompressible = 0; 
        pdata[i].h = h_global*dx; 

        pdata[i].rho_wcsph = 0.0;
        pdata[i].rho = 10.0;

        pdata[i].mass = pdata[i].rho*(dx*dy*dz);
        //pdata[i].sigma0 = p->rho/p->mass;
        pdata[i].pressure = 0.0;
        pdata[i].phase = 1;
        pdata[i].freesurface = true;
        pdata[i].eta = 0.0001;
        //pdata[i].id = count; 
        ++count;
        ++i;
      }
      x += dx;
    }
    x = x_low;
    y += dy;
  }
    x = x_low;
    y = y_low;
    z += dz;
  }
  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;
}
static particleData * circular_drop(){

  int i = 0;
  int count  =0 ;
  double dy= particle_dx;
  double dx = dy;

  /* Estimate the total number of particles */
  /* walls */
  double x_low= xlow->x ;
  double y_low= xlow->y ;
  double x_high= xhigh->x ;
  double y_high= xhigh->y;/*+ delx;*/

  double x = x_low;
  double y = y_low;
double radius =  1.0;
  while(y <= y_high){
    while(x <= x_high){
      if(x*x/4.0 + y*y/(1.6*1.6) <= radius*radius)
//      if(fabs(x)<= radius && fabs(y)<=radius)
        ++count;
      x += dx;
    }
    x = x_low ;
    y += dy;
  }

  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;


  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  printf ( "Total Number of Cells = %d\n", cells.dim.x*cells.dim.y*cells.dim.z);

  /* left boundary*/
  x_low= xlow->x ;
  y_low= xlow->y ;
  x_high= xhigh->x ;
  y_high= xhigh->y;/*+ delx;*/

  x = x_low;
  y = y_low;

  while(y <= y_high){
    while(x <= x_high){
      if(x*x/4.0 + y*y/(1.6*1.6) <= radius*radius){
  //    if(fabs(x)<= radius && fabs(y)<=radius){
        pdata[i].pos.x = x;
        pdata[i].pos.y = y; 
        pdata[i].pos.z = 0.;
        pdata[i].vel.x = 0.;//-100. *x;
        pdata[i].vel.y = 0.;//100.  *y;
        pdata[i].vel.z = 0.;

        //  pdata[i].kappa = 0.;

        pdata[i].acc.x = 0.;
        pdata[i].acc.y = 0.;
        pdata[i].acc.z = 0.;
        pdata[i].incompressible = 0; 
        pdata[i].h = h_global*dx; 

        pdata[i].rho_wcsph = 0.0;
        pdata[i].rho = 1.0;

        pdata[i].mass = pdata[i].rho*(dx*dy);
        //pdata[i].sigma0 = p->rho/p->mass;
        pdata[i].pressure = 0.0;
        pdata[i].phase = 1;
        pdata[i].freesurface = true;
        pdata[i].eta = 0.0001;
        //pdata[i].id = count; 
        ++count;
        ++i;
      }
      x += dx;
    }
    x = x_low;
    y += dy;
  }

  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;
}



static particleData * lid_driven_cavity(){

  double dy= particle_dx;
  double dx = dy;

  int i=0;
  int count = 0;

  /* Estimate the total number of particles */
  double x_low= xlow->x ;
  double y_low= xlow->y ;
  double x_high=xhigh->x ;
  double y_high=xhigh->y;/*+ delx;*/

  double x = x_low +dx/2.0;
  double y = y_low +dx/2.0;

  while(y <= y_high +DELTA){
    while(x <= x_high +DELTA){
      ++count;
      x += dx;
    }
    x = x_low +dx/2.0;
    y += dy;
  }
  printf ( "Total Number of Particles = %d\n", count);

  N = count;
  count = 0;

  /* Creating particles */
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));
  initialize_with_zeros(pdata);
  /* Creating Cells */
  cells.dim.x = floor((xhigh->x - xlow->x)/(h_global*particle_dx*r_cutoff));
  cells.dim.y = floor((xhigh->y - xlow->y)/(h_global*particle_dx*r_cutoff));
  if(dim ==2){
    cells.dim.z = 1;
  } else if (dim ==3) {
    cells.dim.z = floor((xhigh->z - xlow->z)/(h_global*particle_dx*r_cutoff));
  }

  /*  cells.ncells = cells.dim.x * cells.dim.z * cells.dim.y;*/

  allocate_cells();

  x = x_low +dx/2.0;
  y = y_low +dx/2.0;

  while(y <= y_high +DELTA){
    while(x <= x_high +DELTA){
      pdata[i].pos.x = x;
      pdata[i].pos.y = y; 
      pdata[i].pos.z = 0.;
      pdata[i].vel.x = 0.;
      pdata[i].vel.y = 0.;
      pdata[i].vel.z = 0.;
      //  pdata[i].kappa = 0.;
      pdata[i].acc.x = 0.;
      pdata[i].acc.y = 0.;
      pdata[i].acc.z = 0.;
      pdata[i].phase = 1 ;
      if(x<-0.5+DELTA || x>0.5-DELTA || y<-0.5+DELTA || y >0.5-DELTA){
        pdata[i].phase = -1;
        if(y>=0.5-DELTA)
          pdata[i].vel.x = 1.0;
      }
      pdata[i].incompressible = 0; 
      pdata[i].freesurface = false; 
      pdata[i].h = h_global*dx; 

      pdata[i].rho_wcsph = 0.0;
      pdata[i].rho = 1.0;

      pdata[i].mass = pdata[i].rho*(dx*dy);
      //pdata[i].sigma0 = p->rho/p->mass;
      pdata[i].pressure = 0.0;
      //      pdata[i].eta = 0.0025;
      //    if(pdata[i].phase == -1)
      //    pdata[i].eta = 0.0025;
      //pdata[i].id = count; 
      ++count;
      ++i;
      x += dx;
    }
    x = x_low +dx/2.0;
    y += dy;
  }

  // --------------------------------

  printf("%d particles built \n",count);

  return pdata;
}


static void initialize_with_zeros(particleData * pdata)
{

}

void property_assign(particleData * pdata)
{

    density_assign(pdata);
  if(viscous)
    viscosity_assign(pdata);

  return ; 

}

static void density_assign(particleData * pdata)
{
  int i;

  rho[0] = rho[1];
  for(i=0;i<N;i++){
    if(pdata[i].phase >=0){
      pdata[i].rho = rho[pdata[i].phase]* pdata[i].rho;
      pdata[i].mass = rho[pdata[i].phase]* pdata[i].mass;
    }else{
      pdata[i].rho = rho[abs(pdata[i].phase)]* pdata[i].rho;
      pdata[i].mass = rho[abs(pdata[i].phase)]* pdata[i].mass;
      //  pdata[i].color = 1.0;
    }
  }
}
static void viscosity_assign(particleData * pdata)
{
  int i;

  for(i=0;i<N;i++){
    if(pdata[i].phase >0){
      pdata[i].eta = viscosity[pdata[i].phase];
    }else{
      pdata[i].eta = 5.0*viscosity[abs(pdata[i].phase)];
    }
  //  pdata[i].color = 1.0;
  }
}
static vector * points_in_sphere(double R_max, double dx, int * n_sphere)
{

  printf("Creating sphere of radius %lf and dx is %lf\n",R_max, dx);
  dx = dx/R_max;
  double da = dx*dx;                                                           
  double dr= dx;                                                               
  double * R;                                                                  
  int * n;                                                                     
  vector *allpts ;                                                             
  int n_r = ((int)(1.0/dr) +1);                                                
  int i,j,n_total,m,count_j;                                                   
  R = malloc(n_r*sizeof(double));                                              
  n = malloc(n_r*sizeof(int));                                                 
  n_total=0;                                                                   
  for(i=0;i<n_r;i++){                                                          
    R[i] = i*dr;                                                               
    n[i] = (int)(4.0*PI*R[i]*R[i]/da);                                         
    printf("N each level %d \n",n[i]);                                         
    if(n[i]==0) n[i] = 1;                                                      
    n_total += n[i];                                                           
  }
  *n_sphere = n_total;
  printf("Num of sphere points %d\n",n_total);                               
  allpts = malloc(n_total*sizeof(vector));                                     
  vector origin;                                                               
  int c;                                                                       
  for(c=0;c<3;c++)                                                             
    (&origin.x)[c] = 0.0;                                                      
  count_j=0;                                                                   
  for(i=0;i<n_r;i++){                                                          
    if(i==0)                                                                   
      allpts[i] = origin;                                                      
    else{                                                                      
      vector * pv  = points_on_sphere(n[i]);                                   
      for(m=0;m<n[i];m++){                                                     
        for(c=0;c<3;c++)                                                       
          (&pv[m].x)[c] = R[i]*(&pv[m].x)[c];                                  
      }                                                                        
      m = 0;                                                                   
      for(j=count_j;j<count_j+n[i];j++){                                       
        allpts[j] = pv[m];                                                     
        m++;                                                                   
      }                                                                        
    }                                                                          
    count_j=count_j+n[i];                                                      
  }                                                                            
  //scale sphere                                                               
  for(i=0;i<n_total;i++){                                                      
    for(c=0;c<3;c++)                                                           
      (&allpts[i].x)[c] = R_max*(&allpts[i].x)[c];                               
  }                        

  return allpts;
}
static vector *points_on_sphere(int n)                                                
{                                                                              
  double dlong = PI *(3.0 -sqrt(5.0));                                         
  double dz = 2.0/n;                                                           
  double longitude = 0.0;                                                      
  double z = 1.0 - dz/2.0;                                                     

  vector *pts;                                                                 
  pts = malloc(n*sizeof(vector));                                              
  int i;                                                                       
  for(i=0;i<n;i++){                                                            
    double r = sqrt(1.0 - z*z);                                                
    pts[i].x = cos(longitude)*r;                                               
    pts[i].y = sin(longitude)*r;                                               
    pts[i].z = z;                                                              
    z = z-dz;                                                                  
    longitude = longitude+dlong;                                               
  }                                                                            
  return pts;                                                                  
}           
static int variable_callback( void *user_data, const char *name, double *value ){         
                                                                                
  vector data = *((vector *) user_data);                                        
  // look up the variables by name                                              
  if( strcmp( name, "x" ) == 0 ){                                               
    // set return value, return true                                            
    *value = data.x;                                                            
    return PARSER_TRUE;                                                         
  } else if( strcmp( name, "y" ) == 0 ){                                        
    // set return value, return true                                            
    *value = data.y;                                                            
    return PARSER_TRUE;                                                         
  } else if( strcmp( name, "z" ) == 0 ){                                        
    // set return value, return true                                            
    *value = data.z;                                                            
    return PARSER_TRUE;                                                         
  }                                                                             
  // failed to find variable, return false                                      
  return PARSER_FALSE;                                                          
}                                                                               
static int function_callback( void *user_data, const char *name, const int num_args, const double *args, double *value ){
return PARSER_FALSE;                                                            
}

/* Functions for reading the STL file and finding points in geometry */
static triangle * read_geometry(FILE * solid_geometry, int * face_num)    
{                                                                               
//  triangle triarray[FACE_MAX];                                                  
  triangle * triarray;
  triarray = malloc(FACE_MAX * sizeof(triangle));//triarray[FACE_MAX];                                                  
  //   FILE * filein = solid_geometry;                                               
  int   count;                                                                  
  int   i;                                                                      
  int   icor3;                                                                  
  int   ivert;                                                                  
  char *next;                                                                   
  float r1;                                                                     
  float r2;                                                                     
  float r3;                                                                     
  float r4;     
  float temp[3];                                                                
  char  token[LINE_MAX_LEN];                                                    
  int   width;                                                                  
  int object_num;                                                               
  int text_num;                                                                 
  char input[LINE_MAX_LEN];                                                     
  //  Read the next line of the file into INPUT.                                  
  while ( fgets ( input, LINE_MAX_LEN, solid_geometry ) != NULL )                       
  {                                                                             
    text_num = text_num + 1;                                                    
    //  Advance to the first nonspace character in INPUT.                           
    for (next = input; *next != '\0' && ch_is_space ( *next ); next++)          
    {                                                                           
    }                                                                           
    //  Skip blank lines and comments.                                              
    if ( *next == '\0' || *next == '#' || *next == '!' || *next == '$' )        
    {                                                                           
      continue;                                                                 
    }                                                                           
    //  Extract the first word in this line.                                        
    sscanf ( next, "%s%n", token, &width );                                     
    //  Set NEXT to point to just after this token.                                 
    next = next + width;   
    //  FACET                                                                       
    if ( s_eqi ( token, "facet" ) )                                             
    {                                                                           
      //  Get the XYZ coordinates of the normal vector to the face.             
      sscanf ( next, "%*s %e %e %e", &r1, &r2, &r3 );                           
      fgets ( input, LINE_MAX_LEN, solid_geometry );                                    
      text_num = text_num + 1;                                                  
      ivert = 0;                                                                
      for ( ;; )                                                                
      {                                                                         
        fgets ( input, LINE_MAX_LEN, solid_geometry ) ; // to skip outerloop            
        text_num = text_num + 1;                                                
        count = sscanf ( input, "%*s %e %e %e", &r1, &r2, &r3 );                
        if ( count != 3 )                                                       
        {                                                                       
          break;                                                                
        }               
        temp[0] = r1;                                                           
        temp[1] = r2;                                                           
        temp[2] = r3;                                                           
        for(i = 0 ;i<3;i++)                                                     
          (&triarray[*face_num].r[ivert].x)[i] = temp[i];                       
        ivert = ivert + 1;                                                      
      }                                                                         
      fgets ( input, LINE_MAX_LEN, solid_geometry );                                    
      text_num = text_num + 1;                                                  

      *face_num = *face_num + 1;                                                

    }                                                                           
    // SOLID                                                                        
    else if ( s_eqi ( token, "solid" ) )                                        
    {                                                                           
      object_num = object_num + 1;                                              
    }                                                                           
    // ENDSOLID                                                                     
    else if ( s_eqi ( token, "endsolid" ) )                                     
    {                                                                           
    }                                                                           
    //  Unexpected or unrecognized.                                                 
    else                                  
    {                                                                           
      printf("\n Unrecognized first word on line \n");                          
      return 1;                                                                 
    }                                                                           
  }                                                                             
  return triarray;                                                              
}          
static boolean s_eqi ( char *s1, char *s2 )                                     
{                                                                               
  int i;                                                                        
  int nchar;                                                                    
  int nchar1;                                                                   
  int nchar2;                                                                   

  nchar1 = strlen ( s1 );                                                       
  nchar2 = strlen ( s2 );                                                       
  nchar = i4_min ( nchar1, nchar2 );                                            

  //                                                                              
  //  The strings are not equal if they differ over their common length.          
  //                                                                              
  for ( i = 0; i < nchar; i++ )                                                 
  {                                                                             

    if ( ch_cap ( s1[i] ) != ch_cap ( s2[i] ) )                                 
    {                                                                           
      return false;                                                             
    }                                                                           
  }                        
  //                                                                              
  //  The strings are not equal if the longer one includes nonblanks              
  //  in the tail.                                                                
  //                                                                              
  if ( nchar < nchar1 )                                                         
  {                                                                             
    for ( i = nchar; i < nchar1; i++ )                                          
    {                                                                           
      if ( s1[i] != ' ' )                                                       
      {                                                                         
        return false;                                                           
      }                                                                         
    }                                                                           
  }                                                                               
  else if ( nchar < nchar2 )                                                    
  {                                                                             
    for ( i = nchar; i < nchar2; i++ )                                          
    {                                                                           
      if ( s2[i] != ' ' )                                                       
      {                                                                         
        return false;                                                           
      }                                                                         
    }                                                                           
  }                     
  return true;                                                                  

}                                                                               
 //******** 
static boolean ch_is_space ( char c )                                           
{                                                                               
  if ( c == ' ' )                                                                 
  {                                                                             
    return true;                                                                
  }                                                                             
  else if ( c == '\f' )                                                         
  {                                                                             
    return true;                                                                
  }                                                                             
  else if ( c == '\n' )                                                         
  {                                                                             
    return true;                                                                
  }                                                                             
  else if ( c == '\r' )                                                         
  {                                                                             
    return true;                                                                
  }                                                                             
  else if ( c == '\t' )                                                         
  {                                                                             
    return true;                                                                
  }                                                                             
  else if ( c == '\v' )                                                         
  {                                                                             
    return true;                                                                
  }                                                                             
  else                                                                          
  {                                                                             
    return false;     
  }
}

static int i4_min ( int i1, int i2 )                                            
{                                                                               
  if ( i1 < i2 )                                                                
  {                                                                             
    return i1;                                                                  
  }                                                                             
  else                                                                          
  {                                                                             
    return i2;                                                                  
  }                                                                             


}                                                                               
                                                                                 
static char ch_cap ( char c )                                                   
{                                                                               
  if ( 97 <= c && c <= 122 )                                                    
  {                                                                             
    c = c - 32;                                                                 
  }                                                                             


  return c;                                                                     
}        
static boolean particle_in_geometry(vector current_pos, triangle * faces, int num_faces ){
  //  boolean truth=false;                                                        
  int i;                                                                        
  int intersection = 0;                                                         
  //  vector P_0 = *xlow;                                                         
  vector P_0;                                                                   
  P_0.x = xlow->x; P_0.y= current_pos.y; P_0.z= current_pos.z;                  
  vector P_1 = current_pos;                                                     
  vector V_0,V_1, V_2;                                                          
  for(i=0;i<num_faces;i++){                                                     
    V_0 = faces[i].r[0];                                                        
    V_1 = faces[i].r[1];                                                        
    V_2 = faces[i].r[2];                                                        
    // if all vertices are less or greater in y and z and if all verticess are greater than x, then no in     tersection.
    if( (V_0.y>P_1.y && V_1.y>P_1.y && V_2.y> P_1.y) ||                             
        (V_0.y<P_1.y && V_1.y<P_1.y && V_2.y< P_1.y) ||                             
        (V_0.z<P_1.z && V_1.z<P_1.z && V_2.z< P_1.z) ||                             
        (V_0.z>P_1.z && V_1.z>P_1.z && V_2.z> P_1.z) ||                             
        (V_0.x>P_1.x && V_1.x>P_1.x && V_2.x> P_1.x) )                              
      continue ;                                                                    

    //vector n = cross_product(difference(V_1,V_0), difference(V_2, V_0),dim);    
    vector n;
    cross_product(difference(V_1,V_0), difference(V_2, V_0),&n, dim);    
    double denom = dot_product(n, difference(P_1, P_0));                        
    double r;                                                                   
    if(denom == 0.0){                                                           
      // No intersection, plane is parallel to segment                          
      continue;                                                                 
    }else{                                                                      
      r = dot_product(n, difference(V_0 , P_0))/dot_product(n, difference(P_1, P_0));
    }                     
    if(r>1.0){                                                                  
      // No intersection                                                        
      continue;                                                                 
    }                                                                           
    int c;                                                                      
    vector P_I; //intersection point                                            
    for(c=0;c<3;c++)                                                            
      (&P_I.x)[c]  =  (&P_0.x)[c] + r*((&P_1.x)[c]- (&P_0.x)[c]);               
    vector u, v, w;                                                             
    u = difference(V_1,V_0);                                                    
    v = difference(V_2,V_0);                                                    
    w = difference(P_I,V_0);                                                    

    double  denom_2 = dot_product(u,v)*dot_product(u,v) - dot_product(u,u)*dot_product(v,v);

    double s_I = (dot_product(u,v)*dot_product(w,v) - dot_product(v,v)*dot_product(w,u))/denom_2;
    double t_I = (dot_product(u,v)*dot_product(w,u) - dot_product(u,u)*dot_product(w,v))/denom_2;
    if(s_I>=0.0 && t_I >= 0.0 && s_I+t_I <= 1.0)                                
      intersection ++; // There is intersection                                 
    else                                                                        
      continue ;                                                                
  }                                                                             
  if(intersection%2 == 0)                                                       
    return false;                                                               
  else                                                                          
    return true;                                                                
}             





