/**
 * @file run.c
 * @author Prapanch Nair
 * @author Gaurav Tomar
 * @date @f \today @f
 * @brief execution part of the code.
 * 
 * Function calls to calculate different quantities, calls to integrator, output writing routines.
 */
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "lis.h"
#include "sph.h"
#include "particles.h"
#include "integrator.h"
#include "run.h"
#include "write.h"
#include "cell.h"
#include "kernel.h"
#include "isph.h"

static void throw_details(int step, double sec);
static void compute_dt(particleData *pdata);
static double minimum_val(double *arr, int length);
static double calculate_rho_rms(particleData *pdata);
static double calculate_kappa_ab(double rho);

void run(particleData *pdata, int restart){

  integrator intgr = integrators(integrator_model);
  particle_in_cell(pdata);
  // important line to initiate the normalization coefficients in the kernel.
//  double ddd = kernel(1.0,1.0);
  //neighbor_build(pdata);

  if(restart == 0){
    step = 0;
    sim_time = 0.0;
    kappa = calculate_kappa_ab(pdata[0].rho);
  }
  double radius, kin_energy;
  double sec=0.0;
  //potential_params[0] = 0.00000086379;
  //potential_params[1] = 0.00000070951;
  evaporation_time =  1.1;
  int i;
  /* when restart file was not working, the issue was that mass 
   * and density was not being written into restart files. To fix that:
   for(i = 0 ; i <N;i++){
   pdata[i].rho = 1000.0;
   pdata[i].mass = pdata[i].rho*particle_dx*particle_dx;
   }
   */

  printf("Freesurface kappa =%2.8lf\n",kappa);
  printf("potential param  =%2.10lf %2.10lf \n",potential_params[0], potential_params[1]);
  /* 
     int i;
     for(i=0;i<N;i++)
     if(pdata[i].kab != 0.0)
     printf("\n kappa value ------- %lf %d %d \n", pdata[i].kab,pdata[i].cellid, cells.partincell[pdata[i].cellid]);
     */
  char filename[30];                                                             
  sprintf(filename, "time_data.dat");                                            
  FILE *fp = fopen(filename,"w");    
/*  if(GHOSTBC){
    create_ghost_particles(pdata);
  }*/
  //int div_input = divergence;
  //divergence = 3;
  double temperature_vel, total_ke;
  while(sim_time < end_time)
  {
    sec = omp_get_wtime();
    write(pdata, step);
    step++;

    /* if ghost particles, create the ghost particles array*/
    printf("Num of ghosts %d\n",g_N);
    intgr(pdata, dt);
    particle_in_cell(pdata);
    //neighbor_build(pdata);
    if(GHOSTBC){
      create_ghost_particles(pdata);
    }
    if(set_dt){
      compute_dt(pdata);
    }

    sim_time +=dt;
    throw_details(step,sec);
    radius= 0.0;
    //int idd = 9206;
    int idd ;
    double xmax=0.0, ymax=0.0, xmin = 0.0 , ymin=0.0, zmax=0.0, zmin=0.0;
    temperature_vel = 0.0;
    total_ke = 0.0;
    vector total_force[num_phases];
    // for the case of moving end plate.
    /*    if( sim_time > 1.50){
          for(i=0;i<N;i++){
          if( pdata[i].phase <0  && pdata[i].pos.x>0.0)
          pdata[i].vel.x = 0.004;

          }

          } */

    int c;
    int kk;
    for(kk=0;kk<num_phases;kk++){
      for(c=0;c<dim;c++)
        (&total_force[kk].x)[c] = 0.0;
    }
    for(i=0;i<N;i++){
      //temperature_vel += ((pdata[i].vel_xsph.x - pdata[i].vel.x)*(pdata[i].vel_xsph.x - pdata[i].vel.x) 
      //  + (pdata[i].vel_xsph.y - pdata[i].vel.y)*(pdata[i].vel_xsph.y - pdata[i].vel.y)) * pdata[i].mass;
      //  total_ke += ((pdata[i].vel.x)*( pdata[i].vel.x) + (pdata[i].vel.y)*(pdata[i].vel.y)) * pdata[i].mass;
      if(pdata[i].pos.x >xmax){
        xmax = pdata[i].pos.x;
      }
      if(pdata[i].pos.y >ymax){
        ymax = pdata[i].pos.y;
      }
      if(pdata[i].pos.z >zmax){
        zmax = pdata[i].pos.z;
      }
      if(pdata[i].pos.x < xmin){
        xmin = pdata[i].pos.x;
      }
      if(pdata[i].pos.y < ymin){
        ymin = pdata[i].pos.y;
      }
      if(pdata[i].pos.z < zmin){
        zmin = pdata[i].pos.z;
      }





      for(kk = 0; kk<num_phases ; kk++){
        if( pdata[i].body_id==kk ){
          for(c=0;c<dim;c++)
            (&total_force[kk].x)[c] += pdata[i].mass*((&pdata[i].accn.x)[c]);
        }
      }
    }
    //  double vel_onep = pdata[2318].vel.y;
    //radius= sqrt(pdata[idd].pos.x*pdata[idd].pos.x + pdata[idd].pos.y*pdata[idd].pos.y + pdata[idd].pos.z*pdata[idd].pos.z);
    //fprintf(fp, "%d %lf %lf %lf\n", step, sim_time,major_axis,pdata[1217].pressure);                       
    fprintf(fp, "%d %lf  %lf %lf %lf", step, sim_time,0.5*(xmax-xmin), 0.5*(ymax-ymin), 0.5*(zmax-zmin));                       
 //   for(kk=0; kk<num_phases; kk++){
 //     fprintf(fp, " %d %3.9lf %3.9lf %3.9lf ", kk, total_force[kk].x, total_force[kk].y, total_force[kk].z );                       
 //   }
    fprintf(fp, "\n");                       



    //fprintf(fp, "%d %lf %lf %lf  \n", step, sim_time, total_force.x, total_force.y ); 
    fflush(fp);
    //divergence = div_input;
  }

  free(VTKparticles->file_prefix); free(VTKparticles);
  free(timefile->file_prefix); free(timefile);
  return;
}

double calculate_kappa_ab(double rho)
{
  double kap= 0,a=0.0 ; 
  double vol;
  if (dim==2){vol =  particle_dx*particle_dx; a = 0.0;}
  else if(dim ==3){vol = pow(particle_dx,3.0); a=8.0;}
  double x, y, z;
  // int i,j,k;
  int c;
  double h = h_global * particle_dx;
  for(x= -8.0*particle_dx; x<=8.0*particle_dx; x+=particle_dx ){
    for(y= -8.0*particle_dx; y<=8.0*particle_dx; y+=particle_dx ){
      for(z= -a*particle_dx; z<=a*particle_dx; z+=particle_dx ){
        vector xr;
        double rdotr = 0.0, q = 0.0;
        vector neighb_pos;
        neighb_pos.x = x; neighb_pos.y = y; neighb_pos.z = z;
        for(c = 0;c<dim;c++){
          (&xr.x)[c] = 0.0 - (&neighb_pos.x)[c];
            rdotr += (&xr.x)[c]*(&xr.x)[c];
        }
        q = sqrt(rdotr)/h;
        if(q<=r_cutoff){
          double rdotdwdx = 0.0;
          vector dwdx;
          for(c=0;c<dim;c++){
            (&dwdx.x)[c] = (&xr.x)[c] * kernel_Fab(q,h);
            rdotdwdx += (&xr.x)[c]*(&dwdx.x)[c];
          }
// write expression here
          double Fab = rdotdwdx/(rdotr + 0.0001*h*h);
          kap += vol *(2.0/rho)*Fab;

        }

      }
    }
  }

  return kap;
}


static  void throw_details(int step, double sec)
{
  printf("Simulated t %lf\t Time elapsed %lf\n",sim_time,omp_get_wtime()-start_wall_time);
  printf("Timestep %d\t in %lf s , dt_0 %lf, dt %6.10f\n",step, omp_get_wtime()-sec, dt_0, dt);


return;
}

static void compute_dt(particleData *pdata)
{
  int i,c;
  double velmag,velmax,accmag, accmax;
  double dt_arr[4]; /* for initial, advection, viscous and acceleration based*/
  double local_h = h_global*particle_dx;
  velmax = 0.0; accmax=0.0;
  for(i=0;i<N;i++){
    velmag = 0.0;
    accmag = 0.0;
    for(c=0;c<dim;c++){
      velmag += (&pdata[i].vel.x)[c]*  (&pdata[i].vel.x)[c];
      accmag += ((&pdata[i].acc_p.x)[c] + (&pdata[i].acc_mu.x)[c] )*((&pdata[i].acc_p.x)[c] + (&pdata[i].acc_mu.x)[c] ) ;
    }
    velmag = sqrt(velmag);
    accmag = sqrt(accmag);

    if(velmag > velmax)
      velmax = velmag;
    if(accmag > accmax)
      accmax = accmag;

  }
  dt_arr[0] = dt_0;
  dt_arr[1] = 0.25*local_h/velmax;
  if(pdata[2].eta > 0.0)
    dt_arr[2] = 0.125* (local_h)*(local_h)/(pdata[2].eta/pdata[2].rho);
  else
    dt_arr[2] = dt_0;

  dt_arr[3] =  0.25 * sqrt(local_h/accmax);

  dt = minimum_val(dt_arr, 4);

  return ;
}

static double minimum_val(double *arr, int length) {
  // returns the minimum value of array
  int i;
  double min = arr[0];
  for (i = 1; i < length; ++i) {
    if(arr[i]<0){
      printf("Negative timestep \n");
      exit(1);
    }
    if (min > arr[i]) {
      min = arr[i];
    }
  }
  return min;

}

static double calculate_rho_rms(particleData *pdata){
  int i;
  double rms=0.0;
  for(i=0;i<N;i++){
    rms += pow((1.0-pdata[i].wsum),2.0);
  }
  return sqrt(rms/N);
}
