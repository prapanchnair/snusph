#ifndef ISPH_H_
#define ISPH_H_
 
#include "lis.h"

LIS_MATRIX        A;
LIS_VECTOR        b,x;
LIS_SOLVER        solver;                                                     
LIS_INT           nprocs,my_rank;                                             
LIS_INT           gn,is,ie,iter,nsol,err;                                     
LIS_REAL          resid;                                                      
LIS_PRECON          precon;                                                      
char              solvername[128]; 
int *row, *column;
double *value, *dirichlet;
int coo_id;

int L_solver,max_iter,laplacian_type,div_corr;
double bicg_eps;
int press_gradient, laplacian, divergence;

void solve_pressure(particleData * pdata, double dt);
void pressure_matrix_build(particleData *p_a, particleData * p_b, interactParams params/*, double mrho, double
hsquared*/,boolean ghost);
//void divergence_velocity(particles *p_me, particles *pneighbor, interact_params params);
/*void neuman(particles * plist);*/
#endif
