#ifndef kernel_id_ 

#define kernel_id_

#include "sph.h"
#include "particles.h"

int kernel_model;
double coeff[3];
double alpha_fab;
double r_cutoff;

double kernel(double q, double h);
double kernel_Fab(double q, double h);
void initialize_kernel_coefficients(double h);
//void calculate_wsum(particleData * pdata);
#endif
