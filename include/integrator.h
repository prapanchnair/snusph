#ifndef INTEGRATOR_H_
#define INTEGRATOR_H_
#include "sph.h"
#include "forces.h"

double dtby2;

typedef void (*integrator) (particleData *, double dt);

void * integrators(int choice);
void integrate_predictor_corrector(particleData *pdata, double dt);
void integrate_isph(particleData *pdata, double dt);
void integrate_isph_velocity_verlet(particleData *pdata, double dt);
void integrate_isph_cummins(particleData *pdata, double dt);

#endif
