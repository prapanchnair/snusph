#ifndef INPUT_H_
#define INPUT_H_

#define MAXLINE 2048
#define delta 4

char line[MAXLINE];
char copy[MAXLINE];
char * command;

particleData * read_input(FILE *);
particleData * read_restart(FILE *);
particleData * pd;

#endif
