#ifndef SPH_H_
#define SPH_H_

#define DELTA 1e-10
#define PI 2.0*acos(0.0)
//3.142

typedef struct _vector vector;
typedef struct _intvector intvector;
typedef struct _tensor tensor;
typedef struct _triangle triangle;                                              

typedef struct _simoutput simoutput;
/**
 * @brief 3 D vector*/
struct _vector{
  double x /** x coordinate*/, y/** y coordinate*/, z/** z coordinate*/;
};
/**
 * @brief 3 D vector with integer data members*/
struct _intvector{
  int x,y,z;
};

/**
 * @brief 2nd order tensor (vector of vectors)*/
struct _tensor{
  vector xx;  
  vector yy;
  vector zz;  
};
/**                                                                             
 * @brief Triangles forming a face of a geometry*/                              
struct _triangle {                                                              
  vector r[3];                                                                  
}; 

/*Global parameters*/
unsigned N /** Total number of particles*/,N_isph /** NUmber of ISPH particles*/, nnz /** Number of non zero
interactions, which are the number of elements in the linear system*/, g_N; 

vector * xlow /** lower end point of the domain*/ , * xhigh /** higher end point of the domain*/;

double rigidity; /**< rgidity modulus of the solid in the simulation*/

int dim; /**< Simulation dimension*/
double h_global; /**< smoothing length globally if it is constant */
int periodic[3]; /**< boolean to know periodicity in what directions */

double particle_dx; /**< Initial particle spacing*/
double vr_max;
double epsilon; 
double kappa; /**< Value of penalty for free surface*/
vector kappa_grad; /**< Value of penalty for free surface*/
double surf_sigma; /**< Surface tension coefficient*/
int rhs_method;

int num_phases;

/* for the rigid body in the flow*/
double m_i[10];
vector c_g[10];
//double m_t;
vector omega[10];
vector V_r[10];
vector wall_velocity;
vector omega_n[10];
vector V_r_n[10];
vector A_r;
vector force_r[10];
double m_r[10];


typedef enum {
false, 
true} boolean;
/** Purpose for interaction.
 * This enum provides flags to send to interaction function. 
 * Array countaining this enum is sent to achieve multiple set 
 * of interaction. 
 * */
typedef enum {
visc, /**< viscous force interaction */
wsum,/**< kernel summation */
pair_potential, /**< pairwise potential */
count_neighbors,
divV,
defGrad, /**< Deformation Gradient tensor computation */
pressForce,/**< Pressure force computation*/
pressSolve,/**< generate the linear system for pressure*/
surfNorm, /**< computer surface norm-CSF model*/                  
surfKappa,/**< computer surface curvature-CSF model*/
interfaceNeighbors} interact_type;

typedef enum {
internal,
i_o,
freesurf
} flow_t;

boolean SPH;
boolean rho_sum, h_sum;
boolean XSPH, CSPH, ISPH,FSI;
boolean density, energy, heat;
boolean solid;
boolean restart;
boolean surf_tens;
boolean rigid_fixed_velocity;
boolean rigid_body_sim;
boolean defgrad;
boolean GHOSTBC;
int domain_bc[6];

flow_t flow;



struct _simoutput{
  double dt;
  int gridx,gridy, gridz;
  int datatype; /* 0 for ascii and 1 for binary */
/*  boolean is_on;*/
  char * file_prefix;
  double c_time;
};

/* Kinematic functions*/
double magnitude(vector * a);
void cross_product(vector a, vector b, vector *c,int dim);
double  dot_product(vector a, vector  b);
vector  difference(vector a, vector  b);
vector transform(tensor basis, vector pos);
void normalize(vector * a);
double evaporation_time;
void print_logo();
#endif
