/**
 * @file particles.h
 * @brief declarations of data structure for particles and functions 
 * @author Prapanch Nair
 * @author Gaurav Tomar
 *
 * */
#ifndef particle_id_ 

#define particle_id_

#include "sph.h"

# define COR3_MAX 200      //
# define FACE_MAX 100000  //
# define LINE_MAX_LEN 256 //
# define LEVEL_MAX 10
# define LINES_MAX 1000000
# define ORDER_MAX 10     //


typedef struct _particleData particleData;
//typedef struct _neighborList neighborList;

/** \var typedef particleData
 * @brief The basic struct for a particle.
 *
 *
 */
struct _particleData{
  vector pos /** position at n*/ , vel/** velocity at n*/ , acc/** acceleration at n*/ ; 
  vector posn/** position at prev. time step */ , veln/** velocity at prev. time step*/ ,accn/** acceleration at prev. time step*/ ;
  vector pos_s; /**< temporary position for computing def. Gradient*/
  vector acc_p; /**< acc due to press. gradient*/
  vector acc_mu;/**< acc due to viscous and body forces*/
  vector n; /**< Any surface normal vector stored at the particle*/
  vector n_0; /**< Any surface normal vector stored at the particle*/
  vector gradW;
  vector uxx;
  vector vel_xsph;
vector * d1; /* for boundary proximity wall 1*/
vector * d2; /* for boundary proximity wall 2*/
vector * d3; /* for boundary proximity wall 3*/
  //  intvector cell;
  int cellid; /** < ID of the cell in which the particle belongs*/
  double mass; /**< Mass of the particle 
                 * Remains constant through the simulation*/
 // unsigned id;
  int id_left, id_right; 
  int num_neighbors; /**< Number of neighbors of the particle*/
  int num_interface_neighbors;
  int isph_id; /*cornet particles */
  int interface_tag; /**< TAg to demarcate the interface particles*/
  int real_id;
/*SPH*/
  boolean freesurface; /**< Tag to choose particles that can have likely free surfaces*/
  double h; /**< smoothing length of the kernel*/
  double rho /** density*/, pressure /** pressure*/;
  double eta; /**< Viscosity of the fluid */
double div_pos; /**< Divergence of position @f$\nabla \cdot \mathbf{r}@f$ */
  double u; /**< divergence of vel */
  double rho_wcsph; /**< Density as computed for WCSPH*/
//  double mu;/**< Rigidity Modulus */
double alpha;
  int incompressible; /**< Tag for particles that go to the linear solver*/
  /*vector n;*/
  double color;
  double modC;
/*  double kappa;
*/

  double s_kappa; /**< Curvature of surface, @f$ \kappa @f$ */
  int phase; /**< phase of the particle +ve for real phases, 0 for wall edge and -ve for wall particles*/
int body_id;
  tensor def_grad_n; /**< Deformation gradient tensor */
  double det_F; /** < Jacobian - determinant of Def. gradient */
  double pressuren;
/*  tensor stress;
  tensor stress_tot, stress_totn;
  tensor vorticity;
*/
  double kab;
  /*Some arbitrary functions*/
  double Ax;
  double wsum; /** < Summation of the kernel values contributed from neighbors and itself*/
  vector nearest;
  int nearest_id;
  int nearest_id2;
  int * near_id;
//  neighborList * nlist;
};

particleData * g_pdata;
typedef struct _Cells Cells;
/**
 * @brief Struct for the cells which contain particles 
 */
struct _Cells{
  int * partincell; /**< Array of particle ids which reveals particles in each 
 * cell successively */
  int * cellofpart; /**< Array of cell id of each particle */
  int ncells; /**< Total number of cells */
  vector *pos; /**< Position of CG of each cell */
  intvector *coord; /**< Integer coordinate of each cell- starts with (0,0,0)*/
  intvector dim; /**< gives the dimension of the interaction box in number of cells */
  vector *center; 
};

Cells  cells;

typedef struct _interactParams interactParams;
/**
 * @brief Struct that gets passed to functions that compute interactions between 
 * particles*/
struct _interactParams{
  double w; /**< Smoothing Kernel @f$ W(r,h) @f$ */
  double rdotr; /**< @f$ \mathbf{r}_{ab} \cdot \mathbf{r}_{ab} @f$ */
  double q; /**< @f$ r/h @f$*/
  double h; /**< Smoothing Distance h */
  vector dwdx; /**< Gradient of Kernel @f$\nabla W_{ab} @f$*/
  vector xr; /**< Relative postion between pair of particles @f$\mathbf{r}_{ab}@f$*/
  vector vr; /**< Relative velocity between pair of particles @f$ \mathbf{v}_{ab} @f$ */
  vector sr; /**< Relative velocity between pair of particles @f$ \mathbf{v}_{ab} @f$ */
};


/*
typedef struct _neighborList neighborList;
struct _neighborList{
particleData * pdata;
neighborList * next;
};
*/
intvector nboxes;


particleData * create_particles(int model);
particleData * create_particles_from_expression(int * expr_phase, char * expr_string[]);
particleData * create_particles_1D(int model);
void create_ghost_particles(particleData * pdata);
//void reset_ghost_particles(particleData * pdata, particleData * g_pdata);

void compute_normalization(particleData *pdata);
void property_assign(particleData *pdata);
void compute_pressure(particleData *pdata);
void compute_deform_grad(particleData *pdata, double dt);
void allocate_cells();

double viscosity[10];
double rho[10];

typedef struct _geomList geomList;

struct _geomList
{
  triangle * faces;
  int num_faces;
  geomList * next;
};

#endif
