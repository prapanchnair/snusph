#ifndef RUN_H_
#define RUN_H_

double dt, dt_0;
double sim_time;
double end_time;
double start_wall_time;
double wcsph_steps;
int nsteps, set_dt;
int step;
int check_bc;
double check_per;
double wrstart;
double twrite;
double tvtk, area_ab,major_axis;
int integrator_model;
void run(particleData *pdata,int );
#endif
