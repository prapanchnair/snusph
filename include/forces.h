#ifndef FORCES_H_
#define FORCES_H_

#include "sph.h"
#include "particles.h"
#include "lis.h"
void interact(particleData *pdata, interact_type * t_interact,int num_interact, double dt );
//void neighbor_build(particleData * pdata);
void interact_isph(particleData *pdata);
void interact_freesurface_detection(particleData *pdata);
void body_forces(particleData *pdata);
void viscous_forces(particleData * p_a, particleData * p_b, interactParams params, boolean symmetry);
void potential_forces(particleData * p_a, particleData * p_b, interactParams params, boolean symmetry);
void divergence_velocity(particleData *p_a, particleData * p_b, interactParams params, boolean symmetry);
void deform_grad(particleData *pdata, int i, int j, interactParams params, boolean symmetry);
void pressure_forces(particleData *p_a, particleData * p_b, interactParams params, boolean symmetry);
void surface_tension(particleData *pdata);
void compute_determinant_F(particleData *pdata, double dt);
void calculate_n_kab(particleData *pdata, int i, int j, interactParams params, boolean symmetry);
void calculate_normal(particleData *pdata, int i, int j, interactParams params, boolean symmetry);
void calculate_surf_kappa(particleData *pdata, int i, int j, interactParams params, boolean symmetry);
void interface_arrange(particleData * pdata);
void pad_surf_kappa(particleData * pdata);
void interface_detect(particleData * pdata);
void interface_neighbors(particleData * pdata, int i , int j, interactParams params, boolean symmetry);
void calculate_threepoint_kappa(particleData * pdata) ;                         
void calculate_MLS_kappa(particleData * pdata) ;                         
void calculate_lagrange_kappa(particleData * pdata) ;                         
void find_interface_neighbors(particleData * pdata, int a, int b, interactParams params, boolean symmetry);
void correct_pressure_foce(particleData * pdata);

boolean potential;
boolean pressure;
boolean body_force;
boolean viscous;
boolean art_visc;
boolean convective;
boolean surf_ten;
boolean art_stress;

vector bodyforce;

double press_params[3];
int press_model;
int viscous_model;
int art_visc_model;
double potential_params[4];
double epsilon;
double sigma;
/*
void deformation(particles *plist);
void frame_indiff(particles * plist); 
void compute_heat_flux(particles *plist);
*/
void acceleration(particleData *pdata, int tag);

void cg_rigid(particleData * pdata);
void rigid_body(particleData *pdata, double dt);
#endif
