#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "sph.h"
#include "particles.h"
#include "neighbor.h"
#include "integrator.h"
#include "forces.h"
#include "kernel.h"
#include "write.h"
#include "run.h"

particles * read_restart(FILE *fp){
/*  FILE *fp;
  fp = fopen("restart.dat","r");
  */
  int i,c;
  int read_version = 1;
  int file_version;
  fscanf(fp,"%d\n",&file_version);
  if(file_version != read_version){
    printf("Restart file does not correspond to reading function \n 
       Continuing with risk...\n");
  } 
  /*time info */
  fscanf(fp,"%d\n",&sim_time);
  fscanf(fp,"%d %lf %lf\n",&step, &sim_time, &end_time);

  /*Domain information */
  fscanf(fp,"%d\n",&dim);
  xlow = (vector *) malloc (sizeof(vector));
  xhigh = (vector *) malloc (sizeof(vector));
  for(c=0;c<dim;c++) fscanf(fp,"%lf\n",&(&xlow->x)[c]);
  for(c=0;c<dim;c++) fscanf(fp,"%lf\n",&(&xhigh->x)[c]);
  for(c=0;c<3;c++) fscanf(fp,"%d\n",&periodic[c]);
 
  /*Boundary */
  fscanf(fp,"%d\n",&GHOSTBC);
  for(c=0;c<2*dim;c++) fscanf(fp,"%d\n",&domain_bc[c]);
  fscanf(fp,"%lf\n",&wall_velocity);
  fscanf(fp,"%lf\n",&kappa);
  
  /*Pressure Solver info */
  fscanf(fp,"%d %d %d %lf %d\n",&L_solver, &laplacian_type, &max_iter, 
      &bicg_eps, &div_corr);

  /*Difference approximations*/
  fscanf(fp,"%d %d %d %d %d\n",&press_gradient, &divergence, &viscous, &viscous_model); 
  
  /*kernel and related*/
  fscanf(fp,"%d %lf %lf\n",&kernel_model, &r_cutoff, &h_global); 

  /*Constant parameters*/
  for(c=0;c<dim;c++) fscanf(fp,"%lf ",&(&bodyforce.x)[c]);
  fscanf("\n");
  fscanf(fp,"%lf %lf %lf %lf %lf\n",&viscosity[0], &viscosity[1], 
      &viscosity[2], &viscosity[3], &viscosity[4]);
  fscanf(fp,"%d\n",&rigid_body_sim);
  fscanf(fp,"%d\n",&integrator_model);

  /*Outputs*/                                                 
  timefile_is_on = true;
  timefile->file_prefix = (char *) malloc (sizeof(char)*20);       
  timefile= (simoutput *) malloc (sizeof(simoutput));  
  fscanf(fp,"%s\n",&timefile->file_prefix);
  fscanf(fp,"%lf\n",&timefile->dt);
  fscanf(fp,"%d\n",&timefile->datatype);
  timefile->c_time = 0.0;

  fscanf(fp,"%d\n",&VTKparticles_is_on);
  if(VTKparticles_is_on == 1){
    VTKparticles= (simoutput *) malloc (sizeof(simoutput));                 
    VTKparticles->file_prefix = (char *) malloc (sizeof(char)*20); 
    fscanf(fp,"%s\n",&VTKparticles->file_prefix);
    fscanf(fp,"%lf\n",&VTKparticles->dt);
    fscanf(fp,"%d\n",&VTKparticles->datatype);
    fscanf(fp,"%d\n",&VTKparticles->gridx);
    fscanf(fp,"%d\n",&VTKparticles->gridy);
    fscanf(fp,"%d\n",&VTKparticles->gridz);
    VTKparticles->c_time = 0.0;
  }
  fscanf(fp,"%d\n",&VTKgrid_is_on);
  if(VTKgrid_is_on == 1){
    VTKgrid= (simoutput *) malloc (sizeof(simoutput));                 
    VTKgrid->file_prefix = (char *) malloc (sizeof(char)*20); 
    fscanf(fp,"%s \n",&VTKgrid->file_prefix);
    fscanf(fp,"%lf\n",&VTKgrid->dt);
    fscanf(fp,"%d \n",&VTKgrid->datatype);
    fscanf(fp,"%d \n",&VTKgrid->gridx);
    fscanf(fp,"%d \n",&VTKgrid->gridy);
    fscanf(fp,"%d \n",&VTKgrid->gridz);
    VTKgrid->c_time = 0.0;
  }

  /*read_particles*/
  fscanf(fp,"%d \n",&N);
  particleData *pdata;
  pdata = malloc(N*sizeof(particleData ));                                      
  /* pos vel phase pressure rho mass h */
  for(i=0;i<N;i++){
    fscanf(fp, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",&pdata[i].pos.x,&pdata[i].pos.y,&pdata[i].pos.z,&pdata[i].vel.x,&pdata[i].vel.y,&pdata[i].vel.z, &pdata[i].phase, &pdata[i].pressure, &pdata[i].rho, &pdata[i].mass, &pdata[i].h);
  }
  
  
  printf("Read %d particles from the restart file at sim time %lf \n",N, sim_time);
  return pdata;
}



/*
void write_restart(particles *plist)
{

  FILE *fp;
  fp = fopen("restart.dat","w");
  
  fprintf(fp,"%d\n",dim);

  int i;
  for(i=0;i<3;i++) fprintf(fp,"%d\n",periodic[i]);
  for(i=0;i<dim;i++) fprintf(fp,"%3.14lf\n",(&xlow->x)[i]);
  for(i=0;i<dim;i++) fprintf(fp,"%3.14lf\n",(&xhigh->x)[i]);
 
  fprintf(fp,"%2.7lf\n",h_global);

  fprintf(fp,"%d %d %6.14lf %6.7lf %6.7lf\n",nsteps, set_dt, dt, sim_time, end_time);

  fprintf(fp,"%d %6.7lf %s\n",write_single,twrite,file_prefix);
  fprintf(fp, "%u %6.14lf\n",restart, wrstart);

  fprintf(fp,"%u %u %u %u %u\n",body_force, potential, pressure, viscous, art_visc);
  
  fprintf(fp,"%u %u %u %u %u %u\n",density, h_sum, rho_sum, energy, heat
	  , SPH, CSPH);

  fprintf(fp, "%u %d\n",neighbor, neighb_every);

  while(plist){
    particle *p = plist->p;
    fprintf(fp, "%d %d %3.14lf %3.14lf %3.14lf\n",p->id, p->phase, p->pos.x, p->pos.y, p->pos.z );
    fprintf(fp, "%3.14lf %3.14lf %3.14lf\n", p->vel.x, p->vel.y, p->vel.z);
    fprintf(fp,"%3.14lf %3.14lf %3.14lf %3.14lf %3.14lf\n",p->rho, p->mass, p->h, p->pressure, p->t);
    plist = plist->next;
  }

  fclose(fp);
}

*/
