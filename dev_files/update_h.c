#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "sph.h"
#include "particles.h"
#include "neighbor.h"
#include "kernel.h"
#include "boundary.h"

static void compute_hflux(particles *p_me, particles *pneighbor, interact_params params);
static void h_from_rho(particles *plist);
static void particle_loop(particles *plist);
static void reset_to_zero(particles *plist);

void advect_h(particles *plist)
{
  reset_to_zero(plist);
  /*h_sum = true => Compute h from density else use dhdt = - 1/dim*(h/rho)*DrhoDt*/
  if(h_sum){
    h_from_rho(plist);
    return;
  }

  if(neighbor==particle_based)
    particle_loop(plist);
}

static void compute_hflux(particles *p_me, particles *pneighbor, interact_params params)
{
  vector vr = params.vr;
  vector dwdx = params.dwdx;
  particle *pa = p_me->p;
  particle *pb = pneighbor->p;

  if(pa->phase < 0 || pb->phase < 0){
    /*Morris'97: Set boundary velocity field*/
    boundary_set_vr(p_me, pneighbor, &params);
    /*Monaghan: Boundary force*/
    /*boundary_density(p_me, pneighbor, params);*/
  }

  int i;
  for(i = 0; i < dim; i++){
    pa->h_flux += pa->h/dim*pb->mass/pb->rho*(&vr.x)[i]*(&dwdx.x)[i];
    pb->h_flux += pb->h/dim*pa->mass/pa->rho*(&vr.x)[i]*(&dwdx.x)[i];
  }

}

static void h_from_rho(particles *plist)
{
  double sigma = 2.;  
  while(plist){ 
    particle *p = plist->p;
    p->h = sigma*pow(p->mass/p->rho, 1./dim);
    plist = plist->next;
  }
}

static void reset_to_zero(particles *plist)
{  
  while(plist){ 
    particle *p = plist->p;
    if(h_sum)
      p->h = 0.;
    else
      p->h_flux = 0.;    
    plist = plist->next;
  }
}

static void particle_loop(particles *plist)
{
  while(plist){ 
    neighbor_list *temp_list = plist->neighbors;
    while(temp_list){
      interact_params params;
      params.w = temp_list->w;
      params.xr = temp_list->xr;
      params.vr = temp_list->vr;
      params.dwdx = temp_list->dwdx;
    
      compute_hflux(plist, temp_list->p, params);

      temp_list = temp_list->next;
    }
    plist = plist->next;
  }  
}
