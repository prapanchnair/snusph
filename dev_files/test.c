#include<stdio.h>

#include<math.h>
#include<memory.h>
#include<stdlib.h>

typedef struct _vector vector;

struct _vector{
  int x; int y; int z;
};

vector idtocoord(int);
int * allocate( int);


int dimx = 1;
int dimy = 1;
int dimz = 1;

vector idtocoord(int cellid)
{
  vector coord;
  int resz = (cellid)%(dimx * dimy);
  coord.z = (int)((cellid)/(dimx* dimy)) ;
  coord.y = (int)((resz)/dimx)  ;
  coord.x = (resz)%dimx ;  
  return coord;

}
int * allocate( int N)
{
int i;
int * a;
  a = malloc(N*sizeof(int));
for(i=0;i<N;i++)
  a[i] = i;

return a;
}


int main()
{
  vector coord;
  int i;
  int * b;
  int N = 5;
  b = allocate( N);
free(b);
  b = allocate(N+3);
  for(i=0;i<(N+3);i++){
    printf("%d is  \n" , b[i]);

  }


  return 0;

}
